<?php

use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Admin\PermissionController as AdminPermissionController;
use App\Http\Controllers\Admin\RoleController as AdminRoleController;
use App\Http\Controllers\Admin\UserController as AdminUserController;
use App\Http\Controllers\AddressController;
use App\Http\Controllers\Admin\RealEstateController as AdminRealEstateController;
use App\Http\Controllers\CategoryController as AppCategoryController;
use App\Http\Controllers\ProductController as AppProductController;
use App\Http\Controllers\CartController as AppCartController;


use App\Http\Controllers\ImageController;
use App\Http\Controllers\PayPalController;
use App\Http\Controllers\RealEstateController;
use App\Http\Controllers\User\ProfileController;
use App\Http\Controllers\User\RealEstateController as UserRealEstateController;
use App\Http\Controllers\User\TransactionController as UserTransactionController;
use App\Http\Controllers\VNPayController;
use App\Models\Image;
use App\Models\RealEstate;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
|--------------------------------------------------------------------------
| API Routes for login
|--------------------------------------------------------------------------
|
|
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('/', [AuthController::class, 'login']);

    Route::post('/register', [AuthController::class, 'register']);

    Route::get('/',  [AuthController::class, 'profile'])
        ->middleware('auth:sanctum');

    Route::post('/change-password',  [AuthController::class, 'changePassword'])
        ->middleware('auth:sanctum');

    Route::post('/logout',  [AuthController::class, 'logout'])
        ->middleware('auth:sanctum');

    Route::post('/password', [AuthController::class, 'resetPassword']);

    Route::put('/password', [AuthController::class, 'resetEmailCode']);
});
Route::post('/login', [AuthController::class, 'token']);

Route::post('/register', [AuthController::class, 'appRegister']);

Route::get('/profile', [AuthController::class, 'appProfile'])
    ->middleware('auth:sanctum');
/*
|--------------------------------------------------------------------------
| API Routes for admin
|--------------------------------------------------------------------------
|
|
|
*/

Route::group([
    'prefix' => 'admin',
    'middleware' => [
        'auth:sanctum',
    ]
], function () {
    /*
    |--------------------------------------------------------------------------
    | API Routes for user
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    Route::group([
        'prefix' => 'user',
        'middleware' => []
    ], function () {

        Route::group(['prefix' => '/'], function () {

            Route::get('/', [AdminUserController::class, 'list']);
        });

        Route::group(['prefix' => '/{id}'], function () {

            Route::get('/', [AdminUserController::class, 'detail'])
                ->where(['id' => '[0-9]+']);

            Route::put('/', [AdminUserController::class, 'update'])
                ->where(['id' => '[0-9]+']);

            Route::delete('/', [AdminUserController::class, 'delete'])
                ->where(['id' => '[0-9]+']);
        });
    });

    /*
    |--------------------------------------------------------------------------
    | API Routes for role
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    Route::group([
        'prefix' => 'role',
        'middleware' => [
            'sanctum.has_perms:manage_role',
        ]
    ], function () {

        Route::group(['prefix' => '/'], function () {

            Route::get('/', [AdminRoleController::class, 'list']);

            Route::post('/', [AdminRoleController::class, 'create']);
        });

        Route::group(['prefix' => '/{id}'], function () {

            Route::get('/', [AdminRoleController::class, 'detail'])
                ->where(['id' => '[0-9]+']);

            Route::put('/', [AdminRoleController::class, 'update'])
                ->where(['id' => '[0-9]+']);

            Route::delete('/', [AdminRoleController::class, 'delete'])
                ->where(['id' => '[0-9]+']);
        });
    });

    /*
    |--------------------------------------------------------------------------
    | API Routes for role
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    Route::group([
        'prefix' => 'permission',
        'middleware' => [
            'sanctum.has_perms:manage_permission',
        ]
    ], function () {

        Route::group(['prefix' => '/'], function () {

            Route::get('/', [AdminPermissionController::class, 'list']);

            Route::post('/', [AdminPermissionController::class, 'create']);
        });

        Route::group(['prefix' => '/{id}'], function () {

            Route::get('/', [AdminPermissionController::class, 'detail'])
                ->where(['id' => '[0-9]+']);

            Route::put('/', [AdminPermissionController::class, 'update'])
                ->where(['id' => '[0-9]+']);

            Route::delete('/', [AdminPermissionController::class, 'delete'])
                ->where(['id' => '[0-9]+']);
        });
    });

    /*
    |--------------------------------------------------------------------------
    | API Routes for role
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    Route::group([
        'prefix' => 'real-estate',
        'middleware' => [
            'sanctum.has_perms:manage_permission',
        ]
    ], function () {
        Route::get('/', [AdminRealEstateController::class, 'list']);
    });
});

Route::group([
    'prefix' => 'user',
    'middleware' => [
        'auth:sanctum',
    ]
], function () {
    Route::get('/', [PayPalController::class, 'profile']);
});

/*
|--------------------------------------------------------------------------
| API Routes for papal
|--------------------------------------------------------------------------
|
|
|
*/
Route::group([
    'prefix' => 'paypal'
], function () {
    Route::post('/', [PayPalController::class, 'create'])
        ->middleware('auth:sanctum');

    Route::put('/', [PayPalController::class, 'check']);
});


/*
|--------------------------------------------------------------------------
| API Routes for role
|--------------------------------------------------------------------------
|
|
|
*/
Route::group([
    'prefix' => 'vnpay'
], function () {
    Route::post('/', [VNPayController::class, 'create'])
        ->middleware('auth:sanctum');

    Route::put('/', [VNPayController::class, 'check']);

    Route::get('/', [VNPayController::class, 'ipn']);
});
Route::post('/image', [ImageController::class, 'create'])
    ->middleware('auth:sanctum');


Route::group([
    'prefix' => '/address'
], function () {
    Route::get('/province', [AddressController::class, 'provinces']);

    Route::get('/province/{id}', [AddressController::class, 'province'])
        ->where(['id' => '[0-9]+']);

    Route::get('/district', [AddressController::class, 'districts']);

    Route::get('/district/{id}', [AddressController::class, 'district'])
        ->where(['id' => '[0-9]+']);

    Route::get('/ward', [AddressController::class, 'wards']);

    Route::get('/ward/{id}', [AddressController::class, 'ward'])
        ->where(['id' => '[0-9]+']);
});


Route::group([
    'prefix' => '/user',
    'middleware' => [
        'auth:sanctum'
    ]
], function () {
    // api for user profile 
    Route::group([
        'prefix' => '/',
    ], function () {
        Route::put('/', [ProfileController::class, 'update']);
    });

    Route::group([
        'prefix' => '/real-estate',
    ], function () {
        Route::get('/', [UserRealEstateController::class, 'list']);

        Route::post('/', [UserRealEstateController::class, 'create'])
            ->where(['id' => '[0-9]+']);

        Route::get('/{id}', [UserRealEstateController::class, 'detail'])
            ->where(['id' => '[0-9]+']);

        Route::put('/{id}', [UserRealEstateController::class, 'update'])
            ->where(['id' => '[0-9]+']);
    });

    Route::group([
        'prefix' => '/transaction',
    ], function () {
        Route::get('/', [UserTransactionController::class, 'list']);
    });
});


Route::group([
    'prefix' => '/real-estate',
], function () {
    Route::get('/', [RealEstateController::class, 'list']);

    Route::get('/{id}', [RealEstateController::class, 'detail'])
        ->where(['id' => '[0-9]+']);

    Route::post('/{id}/comment', [RealEstateController::class, 'comment'])
        ->middleware(['auth:sanctum'])
        ->where(['id' => '[0-9]+']);
});

Route::group([
    'prefix' => '/category',
], function () {
    Route::get('/', [AppCategoryController::class, 'list']);

    Route::post('/', [AppCategoryController::class, 'create']);
});

Route::group([
    'prefix' => '/product',
], function () {
    Route::get('/', [AppProductController::class, 'list']);

    Route::post('/', [AppProductController::class, 'create']);

    Route::get('/{id}', [AppProductController::class, 'detail'])
        ->where(['id' => '[0-9]+']);
});


Route::group([
    'prefix' => '/cart',
    'middleware' => [
        'auth:sanctum',
    ]
], function () {
    Route::get('/', [AppCartController::class, 'index']);

    Route::post('/{id}', [AppCartController::class, 'add'])
        ->where(['id' => '[0-9]+']);

    Route::post('/payment', [AppCartController::class, 'payment']);

    Route::get('/{id}', [AppCartController::class, 'detail'])
        ->where(['id' => '[0-9]+']);
});

// Route::group([
//     'prefix' => '/test',
// ], function () {
//     Route::get('/', function () {
//         $images = User::where('avatar','like','%gondarpham.myddns.rocks%')
//             ->get();
//         foreach ($images as $img) {
//             $img->avatar =  str_replace('gondarpham.myddns.rocks', 'gondarpham.freeddns.org', $img->avatar);
//             $img->save();
//         }
//     });
// });
