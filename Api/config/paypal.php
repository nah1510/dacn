<?php


return [
    'mode'    => env('PAYPAL_MODE', 'sandbox'),
    'sandbox' => [
        'client_id'         => env('PAYPAL_SANDBOX_CLIENT_ID', 'AUWwdhna59DfFDAPAFJXXcMTAjq-pL-iz03u7KX9z-KXZ0fYEZw0tvZhcrXw8Hg3qq4-npB-OAZ4sGOE'),
        'client_secret'     => env('PAYPAL_SANDBOX_CLIENT_SECRET', 'EJwV1tUi4wNXiKuRjrUxBEve33527qK7KF-IKXuEOWm1_K_66ei9-TM0WjywHjW2v5CNFM4q73qpqe4t'),
        'app_id'            => 'APP-80W284485P519543T',
    ],
    'live' => [
        'client_id'         => env('PAYPAL_LIVE_CLIENT_ID', 'AUWwdhna59DfFDAPAFJXXcMTAjq-pL-iz03u7KX9z-KXZ0fYEZw0tvZhcrXw8Hg3qq4-npB-OAZ4sGOE'),
        'client_secret'     => env('PAYPAL_LIVE_CLIENT_SECRET', 'EJwV1tUi4wNXiKuRjrUxBEve33527qK7KF-IKXuEOWm1_K_66ei9-TM0WjywHjW2v5CNFM4q73qpqe4t'),
        'app_id'            => 'APP-80W284485P519543T',
    ],

    'payment_action' => env('PAYPAL_PAYMENT_ACTION', 'Order'),
    'currency'       => env('PAYPAL_CURRENCY', 'USD'),
    'notify_url'     => env('PAYPAL_NOTIFY_URL', ''),
    'locale'         => env('PAYPAL_LOCALE', 'en_US'),
    'validate_ssl'   => env('PAYPAL_VALIDATE_SSL', true),
    'usd_rate'       => env('USD_RATE', 20000),
];
