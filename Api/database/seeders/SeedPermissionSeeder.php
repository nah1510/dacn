<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use DB;

class SeedPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // User 
        Permission::updateOrCreate(['code' => 'manage_user'], ['name' => 'Manage User']);

        // Role 
        Permission::updateOrCreate(['code' => 'manage_role'], ['name' => 'Manage Role']);
        
        // Permission 
        Permission::updateOrCreate(['code' => 'manage_permission'], ['name' => 'Manage Permission']);
        
    }
}
