<?php

namespace Database\Seeders;

use App\Models\AddressDistrict;
use App\Models\AddressProvince;
use App\Models\AddressWard;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use DB;

class SeedAddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('address_provinces')->count()) {
            echo("Table provinces had migrated.\n");
            return;
        }
        
        DB::table('address_provinces')->delete();
        DB::table('address_districts')->delete();
        DB::table('address_wards')->delete();

        $this->_importProvinces();
        $this->_importDistricts();
        $this->_importWards();

        echo "FINISHED!";
    }

    private function _importProvinces()
    {
        $provinces = file_get_contents($this->_path('provinces'));
        $provincesDecode = json_decode($provinces);

        foreach ($provincesDecode as $key => $province) {
            $data = AddressProvince::create([
                'id' => $province->id,
                'state' => $province->state,
                'name' => $province->name,
                'name_en' => $province->name_en,
                ]);

                echo "{$data}\n";
        }
        echo "\n\n\nDONE Provinces!\n\n\n";
    }

    private function _importDistricts()
    {
        $districts = file_get_contents($this->_path('districts'));
        $districtsDecode = json_decode($districts);

        foreach ($districtsDecode as $key => $district) {
            $data = AddressDistrict::create([
                'id' => $district->id,
                'name' => $district->name,
                'name_en' => $district->name_en,
                'province_id' => $district->province_id
            ]);

            echo "{$data}\n";
        }
        echo "\n\n\nDONE Districts!\n\n\n";
    }

    private function _importWards()
    {
        $wards = file_get_contents($this->_path('wards'));
        $wardsDecode = json_decode($wards);
        foreach ($wardsDecode as $key => $ward) {
            $data = AddressWard::create([
                'id' => (int)$ward->id,
                'name' => $ward->name,
                'name_en' => $ward->name_en,
                'district_id' => $ward->district_id
            ]);

            echo "{$data}\n";
        }
        echo "\n\n\nDONE Wards!\n\n\n";
    }

    private function _path($path)
    {
        return __DIR__ . "/../data/{$path}.json";
    }
}
