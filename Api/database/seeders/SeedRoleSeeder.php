<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use DB;

class SeedRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        /***************
         *General Roles*
         ***************/

        // super admin
        $superAdminRole = Role::updateOrCreate(['code' => 'super-admin'], ['name' => 'Super Admin']);
        $superAdminPermissions = Permission::all()->pluck('id');
        $superAdminRole->permissions()->syncWithoutDetaching($superAdminPermissions);

        // Admin
        $adminRole = Role::updateOrCreate(['code' => 'admin'], ['name' => 'Admin']);
        $adminPermissions = Permission::whereIn('code', [
            'manage_role',
            'manage_permission',
        ])
            ->pluck('id');
        $adminRole->permissions()->syncWithoutDetaching($adminPermissions);

    }
}
