<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRealEstatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('real_estates', function (Blueprint $table) {
            $table->id();
            $table->string('image');
            $table->string('tittle');
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('province_id')->index();
            $table->unsignedInteger('district_id')->index(); 
            $table->unsignedInteger('ward_id')->index();
            $table->string('street')->nullable();
            $table->string('address')->nullable();
            $table->unsignedBigInteger('price')->default(0);
            $table->unsignedInteger('year_built')->nullable();
            $table->unsignedInteger('acreage');
            $table->unsignedInteger('land_acreage');
            $table->unsignedInteger('length');
            $table->unsignedInteger('width');
            $table->unsignedInteger('floor');
            $table->unsignedSmallInteger('type');
            $table->unsignedSmallInteger('post_type');
            $table->text('info')->nullable();
            $table->text('description')->nullable();
            $table->unsignedInteger('views')->default(0);
            $table->unsignedSmallInteger('status')->default(0);
            $table->unsignedSmallInteger('bathroom')->default(0);
            $table->unsignedSmallInteger('bedroom')->default(0);
            $table->timestamp('expired_time')->nullable();
            $table->unsignedSmallInteger('direction');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('real_estates');
    }
}
