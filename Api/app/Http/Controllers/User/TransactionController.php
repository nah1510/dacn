<?php

namespace App\Http\Controllers\User;

use App\Helpers\Traits\PhoneNumberTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\Profile\UpdateRequest;


class TransactionController extends Controller
{
    use PhoneNumberTrait;

    public function list(Request $request)
    {
        $user = $request->user();

        $transactions = $user->ewallet->transactions;

        return $this->responseApiSuccess($transactions);
    }
}
