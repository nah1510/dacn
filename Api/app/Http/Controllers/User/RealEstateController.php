<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\RealEstate\CreateRequest;
use App\Http\Requests\User\RealEstate\UpdateRequest;
use App\Models\AddressWard;
use App\Models\RealEstate;
use App\Models\Transaction;
use DB;

class RealEstateController extends Controller
{
    public function list(Request $request)
    {
        $limit = $request->query('limit', 10);
        $orderBy = $request->query('order_by', 'id');
        $orderType = $request->query('order_type', 'desc');
        $keyword = $request->query('keyword', null);
        $type =  $request->query('type', null);
        $postType =  $request->query('post_type', null);

        $query = RealEstate::with([
            'province',
            'district',
            'ward',
        ])
            ->orderBy($orderBy, $orderType)
            ->where('user_id', $request->user()->id);

        if ($keyword) {
            $query = $query->where('tittle', 'like', "%{$keyword}%");
        }
        if (isset($postType)) {
            $query->where('post_type', $postType);
        }
        if (isset($type)) {
            $query->where('type', $type);
        }
        $result = $query->paginate($limit);

        return $this->responseApiSuccess($result);
    }

    public function create(CreateRequest $request)
    {
        $user = $request->user();
        $data = $request->validated();
        if ($user->ewallet->account_balance < $data['amount']) {
            return $this->responseApiError('Không đủ tiền trong tài khoảng');
        }
        $data['expired_time'] = now()->addDays($data['days_display']);
        $data['user_id'] = $user->id;
        $ward = AddressWard::find($data['ward_id']);
        $district = $ward->district;
        $data['district_id'] = $district->id;
        $data['province_id'] = $district->province_id;
        $data['status'] = RealEstate::STATUS_SHOW;
        try {
            DB::begintransaction();
            $realEstate = RealEstate::create($data);
            $realEstate->images()->createMany($data['images']);
            $ewallet = $user->ewallet;
            $txn = new Transaction;
            $txn->old_balance = $ewallet->account_balance;
            $txn->new_balance = $ewallet->account_balance - $data['amount'];
            $txn->amount = $data['amount'];
            $txn->status = Transaction::STATUS_COMPLETED;
            $txn->menthod = Transaction::MENTHOD_POST;
            $txn->ewallet_id = $ewallet->id;
            $ewallet->decrement('account_balance', $data['amount']);
            $txn->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return $this->responseApiError($e->getMessage());
        }
        return $this->responseApiSuccess($realEstate);
    }

    public function update(UpdateRequest $request)
    {
        $user = $request->user();

        $realEstate = RealEstate::where('user_id', $user->id)
            ->where('id', $request->id)
            ->first();

        if (!$realEstate) {
            return $this->responseApiError('Không tìm thấy thông tin');
        }
        $data = $request->validated();

        if (isset($data['ward_id'])) {
            $ward = AddressWard::find($data['ward_id']);
            $district = $ward->district;
            $data['district_id'] = $district->id;
            $data['province_id'] = $district->province_id;
        }
        try {
            DB::begintransaction();
            $realEstate->update($data);
            if (isset($data['images'])) {
                $realEstate->images()->delete();
                $realEstate->images()->createMany($data['images']);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return $this->responseApiError($e->getMessage());
        }
        return $this->responseApiSuccess($realEstate);
    }

    public function detail(Request $request)
    {
        $realEstate = RealEstate::with([
            'province',
            'district',
            'ward',
            'images',
            'comments.user',
        ])
            ->where('user_id', $request->user()->id)
            ->where('id', $request->id)
            ->first();

        if (!$realEstate) {
            return $this->responseApiError('Không tìm thấy thông tin');
        }

        return $this->responseApiSuccess($realEstate);
    }
}
