<?php

namespace App\Http\Controllers\User;

use App\Helpers\Traits\PhoneNumberTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\Profile\UpdateRequest;
use App\Models\AddressDistrict;
use App\Models\AddressProvince;
use App\Models\AddressWard;

class ProfileController extends Controller
{
    use PhoneNumberTrait;

    public function update(UpdateRequest $request)
    {
        $user = $request->user();
        $data = $request->validated();

        if (isset($data['phone_number'])) {
            $data['phone_number'] = $this->_normalizePhoneNumber($data['phone_number']);
        }

        $user->update($data);

        return $this->responseApiSuccess($user);
    }
}
