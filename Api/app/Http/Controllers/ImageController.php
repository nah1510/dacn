<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\RegisterRequest;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class ImageController extends Controller
{
    public function create(Request $request)
    {
        if ($request->hasFile('image')) {
            $file = $request->file('image');

            $file_name = Str::uuid(4) . "." . $file->getClientOriginalExtension();
    
            $file->move('upload', $file_name);
        }
        return $this->responseApiSuccess(asset('upload/'.$file_name));
    }
}
