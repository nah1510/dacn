<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AddressDistrict;
use App\Models\AddressProvince;
use App\Models\AddressWard;
use Illuminate\Support\Str;

class AddressController extends Controller
{
    public function provinces(Request $request)
    {

        $limit = $request->query('limit', 10);
        $orderBy = $request->query('order_by', 'id');
        $orderType = $request->query('order_type', 'asc');
        $keyword = $request->query('keyword', null);

        $query = AddressProvince::orderBy($orderBy, $orderType);

        if ($keyword) {
            $query = $query->where('name', 'like', "%{$keyword}%");
        }

        $result = $query->get();

        return $this->responseApiSuccess($result);
    }

    public function province(Request $request)
    {
        $result = AddressProvince::find($request->id);

        if(!$result) {
            return $this->responseApiError('Không tìm thấy thông tin');
        }

        return $this->responseApiSuccess($result);
    }

    public function districts(Request $request)
    {

        $limit = $request->query('limit', 10);
        $orderBy = $request->query('order_by', 'id');
        $orderType = $request->query('order_type', 'asc');
        $keyword = $request->query('keyword', null);
        $provinceId = $request->query('province_id', null);

        $query = AddressDistrict::orderBy($orderBy, $orderType);

        if ($provinceId) {
            $query = $query->where('province_id', $provinceId);
        }
        
        if ($keyword) {
            $query = $query->where('name', 'like', "%{$keyword}%");
        }
        $result = $query->get();

        return $this->responseApiSuccess($result);
    }

    public function district(Request $request)
    {
        $result = AddressDistrict::find($request->id);

        if(!$result) {
            return $this->responseApiError('Không tìm thấy thông tin');
        }

        return $this->responseApiSuccess($result);
    }

    public function wards(Request $request)
    {

        $limit = $request->query('limit', 10);
        $orderBy = $request->query('order_by', 'id');
        $orderType = $request->query('order_type', 'asc');
        $keyword = $request->query('keyword', null);
        $districtId = $request->query('district_id', null);

        $query = AddressWard::orderBy($orderBy, $orderType);

        if ($keyword) {
            $query = $query->where('name', 'like', "%{$keyword}%");
        }

        if ($districtId) {
            $query = $query->where('district_id', $districtId);
        }

        $result = $query->get();

        return $this->responseApiSuccess($result);
    }

    public function ward(Request $request)
    {
        $result = AddressWard::find($request->id);

        if(!$result) {
            return $this->responseApiError('Không tìm thấy thông tin');
        }

        return $this->responseApiSuccess($result);
    }
}
