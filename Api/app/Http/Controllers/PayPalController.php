<?php

/**
 * PAYPAL API SERVICE TEST
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Paypal\CheckRequest;
use App\Http\Requests\Paypal\CreateRequest;
use App\Models\Transaction;
use App\Services\PayPal;
use Illuminate\Http\Request;
use Str;
use DB;

class PayPalController extends Controller
{

    protected $paypalService;

    public function __construct(PayPal $paypalService)
    {

        $this->paypalService = $paypalService;
    }

    public function create(CreateRequest $request)
    {
        $user = $request->user();
        $data = $request->validated();
        $dataBody = [
            "intent" => "CAPTURE",
            "purchase_units" =>  [
                [
                    "amount" => [
                        "currency_code" => "USD",
                        "value" => $data['price']
                    ]
                ]
            ],
            "application_context" =>  [
                "return_url" => $data['return_url']
            ]
        ];
        $response = $this->paypalService->sendRequest(
            'POST',
            'https://api.sandbox.paypal.com/v2/checkout/orders',
            $dataBody,
            201
        );
        if (!$response) {
            return $this->responseApiError('Không thể thực hiện giao dịch trong lúc này.');
        }

        Transaction::create([
            'amount' => config('paypal.usd_rate') * $data['price'],
            'payment_id' => $response['id'],
            'ewallet_id' => $user->ewallet->id
        ]);
        foreach ($response['links'] as $value) {
            if ($value['rel'] == 'approve') {
                return $this->responseApiSuccess($value['href']);
            }
        }
        return $this->responseApiSuccess($response);
    }

    public function check(CheckRequest $request)
    {
        $data = $request->validated();
        $txn = Transaction::where('payment_id', $data['payment_id'])
            ->first();
        if (!$txn) {
            return $this->responseApiError('Không tồn tại giao dịch', null, 400);
        }

        if ($txn->status != Transaction::STATUS_PENDING) {
            return $this->responseApiError('Trạng thái giao dịch không hợp lệ', null, 400);
        }

        $response = $this->paypalService->sendRequest(
            'GET',
            'https://api.sandbox.paypal.com/v2/checkout/orders/' . $txn->payment_id,
        );
        if (!$response) {
            return $this->responseApiError('Không thể kiểm tra giao dịch trong lúc này.');
        }

        if ($response['status'] != 'APPROVED') {
            return $this->responseApiError('Đơn hàng chưa thanh toán.');
        }
        try {
            DB::begintransaction();
            $ewallet = $txn->ewallet;
            $txn->old_balance = $ewallet->account_balance;
            $txn->new_balance = $ewallet->account_balance + $txn->amount;
            $txn->status = Transaction::STATUS_COMPLETED;
            $ewallet->increment('account_balance', $txn->amount);
            $txn->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return $this->responseApiError('Không thể cộng tiền vào ví');
        }

        return $this->responseApiSuccess($txn->load('ewallet'));
    }
}
