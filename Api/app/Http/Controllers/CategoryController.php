<?php

namespace App\Http\Controllers;

use App\Helpers\Traits\AppApiResponseTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    use AppApiResponseTrait;
    public function list(Request $request)
    {
        $limit = $request->query('limit', 10);

        $result = Category::paginate($limit);

        return $this->responseSuccess($result);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'image' => 'required|string',
        ]);
    
        if ($validator->fails()) {
            return $this->responseError($validator->errors()->first());
        }
        $data = $request->all();
        $result = Category::create(
            $data
        );

        return $this->responseSuccess($result);
    }
}
