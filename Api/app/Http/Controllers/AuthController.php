<?php

namespace App\Http\Controllers;

use App\Helpers\Traits\AppApiResponseTrait;
use App\Http\Requests\Auth\ChangePasswordRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Jobs\ResetPassword;
use App\Jobs\SendVerifyMail;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Str;

class AuthController extends Controller
{
    use AppApiResponseTrait;
    public function login(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $token = $user->createToken('user-token')->plainTextToken;
        } else {
            return $this->responseApiError('Sai tài khoản hoặc mật khẩu.', null, 404);
        }

        if ($user->email_verified_at == null) {
            if ($request->input('email_verify_code', null)  == $user->email_verify_code) {
                $user->email_verified_at = now();
                $user->save();
            }
        }

        $response = [
            'user' => $user,
            'token' => 'Bearer ' . $token
        ];
        return $this->responseApiSuccess($response, 'Đăng nhập thành công');
    }


    public function token(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $token = $user->createToken('user-token')->plainTextToken;
        } else {
            return $this->responseError('Sai tài khoản hoặc mật khẩu');
        }

        if ($user->email_verified_at == null) {
            if ($request->input('email_verify_code', null)  == $user->email_verify_code) {
                $user->email_verified_at = now();
                $user->save();
            }
        }
        return $this->responseSuccess('Bearer ' . $token);
    }

    public function register(RegisterRequest $request)
    {
        $data = $request->validated();
        $data['password'] = Hash::make($request->password);
        $user = User::create(
            $data
        );

        $user->ewallet()->create();
        SendVerifyMail::dispatch($user);
        return $this->responseApiSuccess($user);
    }
    public function appRegister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|string|min:8',
        ]);
    
        if ($validator->fails()) {
            return $this->responseError($validator->errors()->first());
        }
        $data = $request->all();
        $data['password'] = Hash::make($request->password);
        $user = User::create(
            $data
        );

        $user->ewallet()->create();
        return $this->responseSuccess($user->createToken('user-token')->plainTextToken);
    }

    public function resetPassword(ChangePasswordRequest $request)
    {
        $user = User::where('email_verify_code', $request->input('email_verify_code'))
            ->first();

        if (!$user) {
            return $this->responseApiError('Code bạn nhập không chính xác');
        }

        $data = $request->validated();
        if ($data['new_password'] != $data['recheck_password']) {
            return $this->responseApiError('Mật khẩu bạn nhập không khớp');
        }

        $user->password = Hash::make($data['new_password']);
        $user->email_verify_code = Str::uuid();
        $user->save();

        return $this->responseApiSuccess($user, 'Đổi mật khẩu thành công');
    }

    public function resetEmailCode(Request $request)
    {
        $user = User::where('email', $request->input('email'))
            ->first();

        if (!$user) {
            return $this->responseApiError('Email không tồn tại');
        }

        if ($user->email_verified_at) {
            ResetPassword::dispatch($user);
            return $this->responseApiSuccess(null, 'Đã gửi email');
        } else {
            return $this->responseApiError('Bạn chưa xác nhận email');
        }
    }

    public function profile(Request $request)
    {
        $user = $request->user()->load([
            'roles.permissions',
            'ewallet'
        ]);
        return $this->responseApiSuccess($user);
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $user = $request->user();
        $data = $request->validated();

        if ($data['new_password'] != $data['recheck_password']) {
            return $this->responseApiError('Mật khẩu bạn nhập không khớp');
        }

        if (Hash::check($data['new_password'], $user->password)) {
            return $this->responseApiError('Mật khẩu mới phải khác mật khẩu cũ');
        }

        $user->password = Hash::make($data['new_password']);
        $user->save();

        return $this->responseApiSuccess($user, 'Đổi mật khẩu thành công');
    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();

        return $this->responseApiSuccess(null, 'Đã đăng xuất');
    }

    public function appProfile(Request $request) {
        return $this->responseSuccess($request->user());
    }
}
