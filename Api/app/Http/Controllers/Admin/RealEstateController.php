<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\RealEstate;
use App\Models\RealEstateComment;

class RealEstateController extends Controller
{
    public function list(Request $request)
    {
        $limit = $request->query('limit', 10);
        $provinceId =  $request->query('province_id', null);
        $districtId =  $request->query('district_id', null);
        $wardId =  $request->query('ward_id', null);
        $type =  $request->query('type', null);
        $postType =  $request->query('post_type', null);
        $orderBy = $request->query('order_by', 'price');
        $orderType = $request->query('order_type', 'asc');
        $keyword = $request->query('keyword', null);
        $mt = $request->query('mt', null);
        $ml = $request->query('ml', null);
        $direction = $request->query('direction', null);


        $query = RealEstate::with([
            'images',
            'user:id,name,phone_number',
            'province',
            'district',
            'ward',
        ])
            ->where('status', RealEstate::STATUS_SHOW)
            ->orderBy($orderBy, $orderType);
        if (isset($postType)) {
            $query->where('post_type', $postType);
        }
        if (isset($type)) {
            $query->where('type', $type);
        }
        if ($mt) {
            $query->where('price', '>=', $mt);
        }
        if ($ml) {
            $query->where('price', '<=', $ml);
        }
        if ($keyword) {
            $query->where('tittle', 'like', "%{$keyword}%");
        }
        if ($provinceId) {
            $query->where('province_id', $provinceId);
        }

        if ($districtId) {
            $query->where('district_id', $districtId);
        }
        if ($wardId) {
            $query->where('ward_id', $districtId);
        }
        if (isset($direction)) {
            $query->where('direction', $direction);
        }
        if ($limit) {
            $result = $query->paginate($limit);
        } else {
            $result = $query->get();
        }

        return $this->responseApiSuccess($result);
    }
}
