<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\UpdateRequest;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function list(Request $request)
    {
        $limit = $request->query('limit', 10);
        $status = $request->query('status', null);
        $orderBy = $request->query('order_by', 'id');
        $orderType = $request->query('order_type', 'desc');

        $query = User::with([
            'roles.permissions',
        ])
            ->orderBy($orderBy, $orderType);

        if (isset($status)) {
            $query->where('status', $status);
        }

        $result = $query->paginate($limit);

        return $this->responseApiSuccess($result);
    }

    public function detail(Request $request)
    {
        $user = User::with([
            'roles.permissions',
        ])
            ->where('id', $request->id)
            ->first();

        if (!$user) {
            return $this->responseApiError('Không tìm thấy thông tin');
        }

        return $this->responseApiSuccess($user);
    }

    public function update(UpdateRequest $request)
    {
        $user = User::find($request->id);

        if (!$user) {
            return $this->responseApiError('Không tìm thấy thông tin');
        }
        $data = $request->validated();

        $user->update($data);

        $user->roles()->sync($data['roles']);

        return $this->responseApiSuccess($user);
    }

    public function delete(Request $request)
    {
        $user = User::find($request->id);

        if (!$user) {
            return $this->responseApiError('Không tìm thấy thông tin');
        }
        $user->delete();

        return $this->responseApiSuccess($user);
    }
}
