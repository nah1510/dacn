<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Permission\CreateRequest;
use App\Http\Requests\Admin\Permission\UpdateRequest;
use App\Models\Permission;


class PermissionController extends Controller
{
    public function list(Request $request)
    {
        $limit = $request->query('limit', 10);
        $status = $request->query('status', null);
        $orderBy = $request->query('order_by', 'id');
        $orderType = $request->query('order_type', 'desc');

        $query = Permission::orderBy($orderBy, $orderType);

        if (isset($status)) {
            $query->where('status', $status);
        }
        $result = $query->paginate($limit);

        return $this->responseApiSuccess($result);
    }

    public function detail(Request $request)
    {
        $permission = Permission::find($request->id);

        if (!$permission) {
            return $this->responseApiError('Không tìm thấy thông tin', null, 400);
        }

        return $this->responseApiSuccess($permission);
    }

    public function create(CreateRequest $request)
    {
        $data = $request->validated();

        $permission = Permission::create($data);

        return $this->responseApiSuccess($permission);
    }

    public function update(UpdateRequest $request)
    {
        $permission = Permission::find($request->id);

        if (!$permission) {
            return $this->responseApiError('Không tìm thấy thông tin', null, 400);
        }

        $data = $request->validated();
        $permission->update($data);

        return $this->responseApiSuccess($permission);
    }

    public function delete(Request $request)
    {
        $permission = Permission::find($request->id);

        if (!$permission) {
            return $this->responseApiError('Không tìm thấy thông tin', null, 400);
        }

        $permission->delete();

        return $this->responseApiSuccess($permission);
    }
}
