<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Role\CreateRequest;
use App\Http\Requests\Admin\Role\UpdateRequest;
use App\Models\Role;


class RoleController extends Controller
{
    public function list(Request $request)
    {
        $limit = $request->query('limit', 10);
        $status = $request->query('status', null);
        $orderBy = $request->query('order_by', 'id');
        $orderType = $request->query('order_type', 'desc');

        $query = Role::orderBy($orderBy, $orderType);

        if (isset($status)) {
            $query->where('status', $status);
        }
        $result = $query->paginate($limit);

        return $this->responseApiSuccess($result);
    }

    public function detail(Request $request)
    {
        $role = Role::with('permissions')->where('id', $request->id)->first();

        if (!$role) {
            return $this->responseApiError('Không tìm thấy thông tin', null, 400);
        }

        return $this->responseApiSuccess($role);
    }

    public function create(CreateRequest $request)
    {
        $data = $request->validated();

        $role = Role::create($data);
        $role->permissions()->sync($data['permissions']);
        $role->load('permissions');

        return $this->responseApiSuccess($role);
    }

    public function update(UpdateRequest $request)
    {
        $role = Role::where('id', $request->id)->first();
        if (!$role) {
            return $this->responseApiError('Không tìm thấy thông tin', null, 400);
        }

        $data = $request->validated();

        $role->update($data);
        $role->permissions()->sync($data['permissions']);

        $role->load('permissions');

        return $this->responseApiSuccess($role);
    }

    public function delete(Request $request)
    {
        $role = Role::where('id', $request->id)->first();
        if (!$role) {
            return $this->responseApiError('Không tìm thấy thông tin', null, 400);
        }

        $role->permissions()->delete();
        $role->delete();

        return $this->responseApiSuccess($role);
    }
}
