<?php

namespace App\Http\Controllers;

use App\Helpers\Traits\AppApiResponseTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\CartDetail;
use App\Models\Product;
use DB;

class CartController extends Controller
{
    use AppApiResponseTrait;

    public function index(Request $request)
    {
        $cart = Cart::firstOrCreate([
            'user_id' => $request->user()->id
        ], [
            'user_id' => $request->user()->id
        ]);

        return $this->responseSuccess($cart->load('details.product'));
    }

    public function add(Request $request)
    {
        $product = Product::find($request->id);

        if(!$product) {
            return $this->responseError('Product not found');
        }
 
        $cart = Cart::firstOrCreate([
            'user_id' => $request->user()->id
        ], [
            'user_id' => $request->user()->id
        ]);


        $detail = CartDetail::firstOrCreate(
            [
                'cart_id' => $cart->id,
                'product_id' => $request->id,
            ],[
                'cart_id' => $cart->id,
                'product_id' => $request->id,
                'quantity' => 0,
                'unit_price' => $product->price
            ]
        );
        $detail->increment('quantity');
        $cart ->update([
            'total_price' => DB::raw('GREATEST(total_price + ' . $product->price . ',0)')
        ]);
        return $this->responseSuccess($product);
    }

    public function payment(Request $request)
    {
        $user = $request->user();
        $cart = Cart::firstOrCreate([
            'user_id' => $request->user()->id
        ], [
            'user_id' => $request->user()->id
        ]);
        if($cart->total_price > $user->ewallet->account_balance) {
            return $this->responseError('Unavailable balance');
        }
        $user->ewallet->account_balance -= $cart->total_price;
        $user->ewallet->save();
        $cart->details()->delete();

        $cart->delete();
        return $this->responseSuccess($cart);
    }
}
