<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\RealEstate;
use App\Models\RealEstateComment;

class RealEstateController extends Controller
{
    public function list(Request $request)
    {
        $limit = $request->query('limit', 10);
        $provinceId =  $request->query('province_id', null);
        $districtId =  $request->query('district_id', null);
        $wardId =  $request->query('ward_id', null);
        $type =  $request->query('type', null);
        $postType =  $request->query('post_type', null);
        $orderBy = $request->query('order_by', 'price');
        $orderType = $request->query('order_type', 'asc');
        $keyword = $request->query('keyword', null);
        $mt = $request->query('mt', null);
        $ml = $request->query('ml', null);
        $direction = $request->query('direction', null);


        $query = RealEstate::with([
            'images',
            'user:id,name,phone_number',
            'province',
            'district',
            'ward',
        ])
            ->where('status', RealEstate::STATUS_SHOW)
            ->orderBy($orderBy, $orderType);
        if (isset($postType)) {
            $query->where('post_type', $postType);
        }
        if (isset($type)) {
            $query->where('type', $type);
        }
        if ($mt) {
            $query->where('price', '>=', $mt);
        }
        if ($ml) {
            $query->where('price', '<=', $ml);
        }
        if ($keyword) {
            $query->where('tittle', 'like', "%{$keyword}%");
        }
        if ($provinceId) {
            $query->where('province_id', $provinceId);
        }

        if ($districtId) {
            $query->where('district_id', $districtId);
        }
        if ($wardId) {
            $query->where('ward_id', $districtId);
        }
        if (isset($direction)) {
            $query->where('direction', $direction);
        }
        $result = $query->paginate($limit);

        return $this->responseApiSuccess($result);
    }

    public function detail(Request $request)
    {
        $realEstate = RealEstate::with([
            'images',
            'user',
            'comments.user',
            'province',
            'district',
            'ward',
        ])
            ->where('status', RealEstate::STATUS_SHOW)
            ->where('id', $request->id)
            ->first();

        if (!$realEstate) {
            return $this->responseApiError('Không tìm thấy thông tin');
        }
        $realEstate->increment('views');
        return $this->responseApiSuccess($realEstate);
    }

    public function comment(Request $request)
    {
        $realEstate = RealEstate::where('status', RealEstate::STATUS_SHOW)
            ->where('id', $request->id)
            ->first();

        if (!$realEstate) {
            return $this->responseApiError('Không tìm thấy thông tin');
        }

        $comment = RealEstateComment::create([
            'user_id' => $request->user()->id,
            'real_estate_id' => $realEstate->id,
            'comment' => $request->input('comment', '')
        ]);
        return $this->responseApiSuccess($comment);
    }
}
