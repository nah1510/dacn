<?php

namespace App\Http\Controllers;

use App\Helpers\Traits\AppApiResponseTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    use AppApiResponseTrait;
    public function list(Request $request)
    {
        $limit = $request->query('limit', 10);

        $result = Product::paginate($limit);

        return $this->responseSuccess($result);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tittle' => 'required|string',
            'category_id' => 'required|exists:categories,id',
            'images' => 'required|array',
            'images.*.url' => 'required|url',
            'price' => 'required|integer',
            'rating' => 'required|numeric',
            'description' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->responseError($validator->errors()->first());
        }
        $data = $request->all();
        $product = Product::create(
            $data
        );
        $product->images()->createMany($request->images);

        return $this->responseSuccess($product->load('images'));
    }

    public function detail(Request $request)
    {
        $product = Product::with('images')
            ->where('id', $request->id)
            ->first();
        if(!$product) {
            return $this->responseError("Product not found");
        }
        return $this->responseSuccess($product);
    }
}
