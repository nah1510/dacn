<?php

namespace App\Http\Requests\User\RealEstate;

use App\Http\Controllers\Controller;
use App\Http\Requests\FormRequest;
use App\Models\RealEstate;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tittle' => 'string',
            'ward_id' => 'exists:address_wards,id',
            'street' => 'required',
            'address' => 'required',
            'price' => 'integer|min:0',
            'year_built' => 'date_format:Y',
            'acreage' => 'integer|min:1',
            'land_acreage' => 'integer|min:1',
            'length' => 'integer|min:1',
            'width' => 'integer|min:1',
            'floor' => 'integer|min:0',
            'type' => 'in:' . RealEstate::TYPE_APARTMENT .
                ',' . RealEstate::TYPE_HOUSE .
                ',' . RealEstate::TYPE_LAND,
            'post_type' => 'in:' . RealEstate::POST_TYPE_RENT .
                ',' . RealEstate::POST_TYPE_SALE,
            'info' => 'string',
            'description' => 'string',
            'image' => 'url',
            'images' => 'array',
            'images.*.url' => 'url',
            'days_display' => 'integer|min:1',
            'bathroom' => 'integer|min:0',
            'bedroom' => 'integer|min:0',
            'direction' => 'integer|min:0|max:7',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->responseApiError($validator->errors()->first(), null, 400));
    }
}
