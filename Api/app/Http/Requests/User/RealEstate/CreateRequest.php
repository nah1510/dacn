<?php

namespace App\Http\Requests\User\RealEstate;

use App\Http\Controllers\Controller;
use App\Http\Requests\FormRequest;
use App\Models\RealEstate;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tittle' => 'required|string',
            'ward_id' => 'required|exists:address_wards,id',
            'street' => 'required',
            'address' => 'required',
            'price' => 'required|integer|min:0',
            'year_built' => 'required|date_format:Y',
            'acreage' => 'required|integer|min:1',
            'land_acreage' => 'required|integer|min:1',
            'length' => 'required|integer|min:1',
            'width' => 'required|integer|min:1',
            'floor' => 'required|integer|min:0',
            'type' => 'required|in:' . RealEstate::TYPE_APARTMENT .
                ',' . RealEstate::TYPE_HOUSE .
                ',' . RealEstate::TYPE_LAND,
            'post_type' => 'required|in:' . RealEstate::POST_TYPE_RENT .
                ',' . RealEstate::POST_TYPE_SALE,
            'info' => 'required|string',
            'description' => 'required|string',
            'image' => 'required|url',
            'images' => 'required|array',
            'images.*.url' => 'required|url',
            'days_display' => 'required|integer|min:1',
            'bathroom' => 'required|integer|min:0',
            'bedroom' => 'required|integer|min:0',
            'amount' => 'required|integer|min:1000',
            'direction' => 'required|integer|min:0|max:7',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->responseApiError($validator->errors()->first(), null, 400));
    }
}
