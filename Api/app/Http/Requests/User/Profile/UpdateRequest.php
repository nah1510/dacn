<?php

namespace App\Http\Requests\User\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */ 
    public function rules()
    {
        return [
            'avatar' => 'url',
            'phone_number' => array('regex:/\+?(0|84)(\d{9}){1}\b/'),
            'gender' => 'in:0,1,2',
            'birthday' => 'date_format:Y-m-d',
            'name' => 'string '
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->responseApiError($validator->errors()->first(), null, 400));
    }
}
