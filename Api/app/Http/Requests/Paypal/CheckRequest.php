<?php

namespace App\Http\Requests\Paypal;

use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CheckRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'payment_id' => 'required|string',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->responseApiError($validator->errors()->first(), null, 400));
    }
}
