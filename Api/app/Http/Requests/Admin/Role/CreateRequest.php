<?php

namespace App\Http\Requests\Admin\Role;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */ 
    public function rules()
    {
        return [
            'code' => 'required|unique:auth_roles,code|max:100',
            'name' => 'required|max:100',
            'permissions' => 'array',
            'permissions.*' => 'exists:auth_permissions,id',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->responseApiError($validator->errors()->first(), null, 400));
    }
}
