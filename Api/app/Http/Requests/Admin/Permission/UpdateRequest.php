<?php

namespace App\Http\Requests\Admin\Permission;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'unique:auth_permissions,code,' . request()->id . ',id|max:100',
            'name' => 'max:100',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->responseApiError($validator->errors()->first(), null, 400));
    }
}
