<?php

namespace App\Http\Requests\Auth;

use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'new_password' => 'required|string|min:8',
            'recheck_password' => 'required|string|min:8'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->responseApiError($validator->errors()->first(), null, 400));
    }
}
