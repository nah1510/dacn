<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use App\Helpers\Traits\ApiControllerTrait;
use Closure;
use Exception;

class HasAnyPermission extends Middleware
{
    use ApiControllerTrait;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next, ...$guards)
    {
        try {
            $user = $request->user();
            if(!$user) {
                return $this->responseApiError('Không tìm thấy người dùng', null, 404);
            }
            if (!$user->hasAnyPermission($guards)) {
                return $this->responseApiError('Không có quyền', null, 403);
            }

            return $next($request);
        }
        catch (Exception $e) {
            return $this->responseApiError('Có lỗi khi kiểm tra quyền', null,  500);
        }
    }
}
