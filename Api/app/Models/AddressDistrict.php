<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddressDistrict extends Model
{
    protected $table = 'address_districts';
    
    protected $fillable = [
        'id',
        'name',
        'name_en',
        'province_id',
    ];

    public function province()
    {
        return $this->belongsTo(AddressProvince::class, 'province_id');
    }

    public function wards()
    {
        return $this->hasMany(AddressWard::class, 'district_id');
    }
}
