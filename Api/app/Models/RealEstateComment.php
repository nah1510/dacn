<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RealEstateComment extends Model
{
    protected $table = 'real_estate_comments';

    protected $fillable = [
        'id',
        'real_estate_id',
        'user_id',
        'comment',

    ];

    public function realEstate()
    {
        return $this->belongsTo(RealEstate::class, 'real_estate_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
