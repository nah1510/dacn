<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    const STATUS_PENDING = 0;
    const STATUS_COMPLETED = 1;
    const STATUS_CANCEL = 2;

    const MENTHOD_PAYPAL = 'paypal';
    const MENTHOD_POST = 'post';

    protected $table = 'transactions';

    protected $fillable = [
        'id',
        'ewallet_id',
        'menthod',
        'amount',
        'old_balance',
        'new_balance',
        'payment_id',
        'status'
    ];

    public function ewallet()
    {
        return $this->belongsTo(Ewallet::class, 'ewallet_id');
    }
}
