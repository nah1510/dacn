<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'auth_permissions';

    protected $fillable = [
        'id',
        'name', 
        'code',
    ];

    protected $hidden = [];
}
