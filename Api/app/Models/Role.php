<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'auth_roles';

    protected $fillable = [
        'id',
        'name', 
        'code',
    ];

    protected $hidden = [];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'auth_role_permissions', 'role_id', 'permission_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'auth_user_roles', 'role_id', 'user_id');
    }
}
