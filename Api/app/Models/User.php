<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'email_verify_code',
        'avatar',
        'phone_number',
        'gender',
        'birthday',
        'device_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'birthday'    => 'date:Y-m-d\TH:i:s.sss\Z',
    ];
    /**
     * Relationship
     *
     * 
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'auth_user_roles', 'user_id', 'role_id');
    }

    public function ewallet()
    {
        return $this->hasOne(Ewallet::class,  'user_id');
    }


    public function toArray() {
        $data = parent::toArray();

        if(isset($this->ewallet)) {
            $data['amount'] = $this->ewallet->account_balance;
        }

        return $data;
    }

    public function hasAnyPermission($codes)
    {
        $codes = is_array($codes) ? $codes : explode('|', $codes);

        $permissions = $this->getPermissions();
        $lackedPermissions = array_diff($codes, $permissions);

        if (empty($lackedPermissions)) {
            return true;
        }

        return false;
    }

    public function getPermissions()
    {
        $permissions = [];

        $roles = $this->roles()->with('permissions')->get();
        foreach ($roles as $role) {
            foreach ($role->permissions as $permission) {
                array_push($permissions, $permission->code);
            }
        }

        return $permissions;
    }

    public function hasPermission($code)
    {
        $permissions = $this->getPermissions();
        return in_array($code, $permissions);
    }
}
