<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ewallet extends Model
{
    protected $table = 'ewallets';

    protected $fillable = [
        'id',
        'user_id',
        'account_balance',
        'process_id'
    ];

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'ewallet_id');
    }
}
