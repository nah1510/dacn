<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartDetail extends Model
{
    protected $table = 'cart_details';

    protected $fillable = [
        'id',
        'cart_id',
        'product_id',
        'quantity',
        'unit_price'
    ];

    protected $hidden = [];

    public function cart() 
    {
        return $this->belongsTo(Cart::class, 'cart_id');
    }

    public function product() 
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
