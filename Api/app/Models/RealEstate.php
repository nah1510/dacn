<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RealEstate extends Model
{
    const STATUS_HIDE = 0;
    const STATUS_SHOW = 1;

    const TYPE_HOUSE = 0;
    const TYPE_APARTMENT = 1;
    const TYPE_LAND = 2;

    const POST_TYPE_SALE = 0;
    const POST_TYPE_RENT = 1;

    const  DIRECTION_NORTH = 0;
    const  DIRECTION_NORTH_WEST = 1;
    const  DIRECTION_NORTH_EAST = 2;
    const  DIRECTION_SOUTH = 3;
    const  DIRECTION_SOUTH_WEST = 4;
    const  DIRECTION_SOUTH_EAST = 5;
    const  DIRECTION_EAST = 6;
    const  DIRECTION_WEST = 7;

    protected $table = 'real_estates';

    protected $fillable = [
        'id',
        'tittle',
        'user_id',
        'province_id',
        'district_id',
        'ward_id',
        'street',
        'address',
        'price',
        'year_built',
        'acreage',
        'length',
        'width',
        'type',
        'floor',
        'info',
        'description',
        'views',
        'status',
        'expired_time',
        'image',
        'post_type',
        'land_acreage',
        'direction',
        'bathroom',
        'bedroom'
    ];

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function comments()
    {
        return $this->hasMany(RealEstateComment::class, 'real_estate_id');
    }

    public function province()
    {
        return $this->belongsTo(AddressProvince::class, 'province_id');
    }

    public function district()
    {
        return $this->belongsTo(AddressDistrict::class, 'district_id');
    }

    public function ward()
    {
        return $this->belongsTo(AddressWard::class, 'ward_id');
    }
}
