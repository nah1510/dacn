<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddressProvince extends Model
{
    protected $table = 'address_provinces';
    
    protected $fillable = [
        'id',
        'state',
        'name',
        'name_en',
    ];

    const STATE_NORTHERN = 1;
    const STATE_CENTRAL = 2;
    const STATE_SOUTH = 3;

    public static function getStateChoices()
    {
        return [
            self::STATE_NORTHERN => 'Miền bắc',
            self::STATE_CENTRAL => 'Miền trung',
            self::STATE_SOUTH => 'Miền nam',
        ];
    }

    

    public function toArray() {
        $data = parent::toArray();

        if (isset($data['state'])) {
            $data['state_text'] = $this->getStateText($data['state']); 
        }

        return $data; 
    }

    public function getStateText($state)
    {
        $stateList = self::getStateChoices();
        return isset($stateList[$state]) ? $stateList[$state] : '-empty-';
    }

    public function districts()
    {
        return $this->hasMany(AddressDistrict::class, 'province_id');
    }

}
