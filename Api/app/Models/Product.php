<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'id',
        'tittle',
        'category_id', 
        'price',
        'rating', 
        'description',
    ];

    protected $hidden = [];

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function toArray() {
        $data = parent::toArray();

        if(isset($this->images)) {
            $data['image'] = $this->images()->first()->url;
        }

        return $data;
    }
}
