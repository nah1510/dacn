<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddressWard extends Model
{
    protected $table = 'address_wards';
    
    protected $fillable = [
        'id',
        'name',
        'name_en',
        'district_id',
    ];


    public function district()
    {
        return $this->belongsTo(AddressDistrict::class, 'district_id');
    }
}
