<?php

namespace App\Helpers\Traits;

use Illuminate\Support\Str;

trait PhoneNumberTrait
{
    private function _generatePhoneNumberUID($phoneNumber)
    {
        $phoneNumber = $this->_normalizePhoneNumber($phoneNumber);
        return substr(md5($phoneNumber), 0, 8);
    }

    private function _normalizePhoneNumber($phoneNumber)
    {
        if (Str::startsWith($phoneNumber, '0')) {
            $phoneNumber = Str::replaceFirst('0', '+84', $phoneNumber);
        }

        if (Str::startsWith($phoneNumber, '84')) {
            $phoneNumber = Str::replaceFirst('84', '+84', $phoneNumber);
        }

        return $phoneNumber;
    }

    private function _replaceStarForPhoneNumber($phoneNumber)
    {
        $length = strlen($phoneNumber);
        return substr($phoneNumber, 0, 3) . '******' . substr($phoneNumber, $length-3, $length);
    }
}
