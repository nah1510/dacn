<?php

namespace App\Helpers\Traits;

trait AppApiResponseTrait
{
  public  function responseSuccess($data, $code = 200)
  {
    return response()->json($data, $code);
  }

  public function responseError($message, $code = 400)
  {
    return response()->json($message, $code);
  }

  public function validateFail($message)
  {
    return [
      'valid' => false,
      'message' => $message
    ];
  }

  public function validateSuccess($data)
  {
    return [
      'valid' => true,
      'data' => $data
    ];
  }
}
