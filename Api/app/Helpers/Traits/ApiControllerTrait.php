<?php

namespace App\Helpers\Traits;

trait ApiControllerTrait
{
  public function responseApi($status, $message, $data, $error, $code)
  {
    $result = [];
    $result['success'] = $status;
    $result['data'] = $data;
    $result['msg'] = $message;
    $result['errors'] = $error;
    $code = $code ? $code : ($status ? 200 : 500);
    $result['code'] = $code;

    return response($result, 200);
  }

  public function responseApiSuccess($data, $msg = '', $code = 200)
  {
    return $this->responseApi(true, $msg, $data, null, null, $code);
  }

  public function responseApiError($msg, $errors = null, $code = null)
  {
    return $this->responseApi(false, $msg, null, $errors, $code);
  }
}
