<?php

namespace App\Services;

use GuzzleHttp\Client;

class PayPal
{
    protected $clientId;
    protected $clientSecret;
    protected $token;
    protected $appId;

    public function __construct()
    {
        if (config('paypal.mode') == 'sandbox') {
            $config = config('paypal.sandbox');
        } else {
            $config = config('paypal.live');
        }
        $this->clientId = $config['client_id'];
        $this->clientSecret = $config['client_secret'];
    }

    public function setToken()
    {
        try {
            $credentials = base64_encode(
                $this->clientId . ':' . $this->clientSecret
            );
            $client = new Client();
            $response = $client
                ->request(
                    'POST',
                    'https://api-m.sandbox.paypal.com/v1/oauth2/token',
                    [
                        'form_params' => [
                            'grant_type' => 'client_credentials'
                        ],
                        'headers' => [
                            'Accept' => 'application/json',
                            'Accept-Language' => 'en_US',
                            'Authorization' => 'Basic ' . $credentials
                        ]
                    ]
                );
        } catch (Exception $e) {
            Log::error("Có lỗi khi gửi request " . $e->getMessage());
        }
        $httpStatus = $response->getStatusCode();
        $content = json_decode($response->getBody()->getContents(), true);
        if ($httpStatus != 200) {
            return 0;
        } else {
            if ($content) {
                $this->token = $content['token_type'] . ' ' . $content['access_token'];
            }
        }
    }

    public function sendRequest($method, $uri, $data = [], $httpResponseStatus = 200)
    {
        $this->setToken();
        try {
            $client = new Client();
            $response = $client
                ->request(
                    $method,
                    $uri,
                    [
                        'json' => $data,
                        'headers' => [
                            'Accept' => 'application/json',
                            'Authorization' => $this->token
                        ]
                    ]
                );
        } catch (Exception $e) {
            Log::error("Có lỗi khi gửi request " . $e->getMessage());
        }
        $httpStatus = $response->getStatusCode();

        $content = json_decode($response->getBody()->getContents(), true);
        if ($httpStatus != $httpResponseStatus) {
            return 0;
        } else {
            return ($content);
        }
    }
}
