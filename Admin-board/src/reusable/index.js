import DocsLink from './DocsLink'

function formatDate(dateinfo){
  const date = new Date(dateinfo);
  if(date.getDate() < 10) {
      return `0` + date.getDate() + "/" + date.getMonth()+1 + "/" + date.getFullYear();
  }
  return date.getDate() + "/" + date.getMonth()+1 + "/" + date.getFullYear();
}

export function formatPrice ( price )
{
    const bil = 1000000000;
    const mil = 1000000;
    if ( price >= bil )
    {
        return `${ Math.ceil( price / bil ) } tỷ`;
    }
    if ( price >= mil )
    {
        return `${ Math.ceil( price / mil ) } triệu`;
    }
    return price;
}

export {
  DocsLink,
  formatDate
}
