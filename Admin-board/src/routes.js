import React from 'react';
const Users = React.lazy( () => import( './views/users/Users' ) );
const User = React.lazy( () => import( './views/users/User' ) );

const BuySection = React.lazy( () => import( './views/realestate/buy' ) );
const BuyId = React.lazy( () => import( './views/realestate/buyid' ) );
const RentId = React.lazy( () => import( './views/realestate/rentid' ) );
const RentSection = React.lazy( () => import( './views/realestate/rent' ) );
const BuyStatistic = React.lazy( () => import( './views/statistics/buystatistic' ) );
const RentStatistic = React.lazy( () => import( './views/statistics/rentstatistic' ) );
const UserStatistic = React.lazy( () => import( './views/statistics/userstatistic' ) );

const routes = [
  { path: '/', exact: true, name: 'Trang chủ' },
  { path: '/users', exact: true, name: 'Users', component: Users },
  { path: '/users/:id', exact: true, name: 'User Details', component: User },
  { path: '/realestate/buy', exact: true, name: 'Loại hình - bán', component: BuySection },
  { path: '/realestate/buy/:id', exact: true, name: 'Thông tin chi tiết', component: BuyId },
  { path: '/realestate/rent', exact: true, name: 'Loại hình - thuê', component: RentSection },
  { path: '/realestate/rent/:id', exact: true, name: 'Thông tin chi tiết', component: RentId },
  { path: '/statistics/buystatistic', exact: true, name: 'Thống kê bất động sản - bán', component: BuyStatistic },
  { path: '/statistics/rentstatistic', exact: true, name: 'Thống kê bất động sản - cho thuê', component: RentStatistic },
  { path: '/statistics/userstatistic', exact: true, name: 'Thống kê người dùng', component: UserStatistic },
];

export default routes;
