import axios from 'axios';


const dev_url = "https://gondarpham.xyz/";

let instance = axios.create( {
    proxy: {
        protocol: 'http',
        host: '192.168.1.8',
        port: 8000,
        auth: {
            username: 'mikeymike',
            password: 'rapunz3l'
        },
    },
    baseURL: '192.168.1.8',
} );

export function apiGetUserList ( token, cb )
{
    axios( {
        method: 'get',
        url: 'api/admin/user?limit=999',
        baseURL: dev_url,
        headers: { "Content-type": "application/json", 'Authorization': token }
    } ).then( function ( response )
    {
        const data = response.data.data;
        cb( null, data );
    } ).catch( function ( error )
    {
        cb( error, null );
    } );
}

export function apiGetUser ( token, id, cb )
{
    axios( {
        method: 'get',
        url: `api/admin/user/${ id }`,
        baseURL: dev_url,
        headers: { "Content-type": "application/json", 'Authorization': token }
    } ).then( function ( response )
    {
        const data = response.data.data;
        cb( null, data );
    } ).catch( function ( error )
    {
        cb( error, null );
    } );
}

export function updateUser ( token, id, data, cb )
{
    axios( {
        method: 'put',
        url: `api/admin/user/${ id }`,
        baseURL: dev_url,
        data: data,
        headers: { "Content-type": "application/json", 'Authorization': token }
    } ).then( function ( response )
    {
        const data = response.data;
        cb( null, data );
    } ).catch( function ( error )
    {
        cb( error, null );
    } );
}

export function deleteUser ( token, id, cb )
{
    axios( {
        method: 'delete',
        url: `api/admin/user/${ id }`,
        baseURL: dev_url,
        headers: { "Content-type": "application/json", 'Authorization': token }
    } ).then( function ( response )
    {
        const data = response.data.data;
        cb( null, data );
    } ).catch( function ( error )
    {
        cb( error, null );
    } );
}
