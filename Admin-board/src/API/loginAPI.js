import axios from 'axios';

const dev_url = "https://gondarpham.xyz/";
// const dev_url = "http://gondarpham.xyz";
const base_url = "http://gondarpham.xyz";

let instance = axios.create( {
    proxy: {
        protocol: 'http',
        host: '192.168.1.8',
        port: 8000,
        auth: {
            username: 'mikeymike',
            password: 'rapunz3l'
        },
    },
    baseURL: '192.168.1.8',
} );

export function apiPostImage ( token, data, cb )
{
    axios( {
        method: 'post',
        url: 'api/image',
        baseURL: dev_url,
        data: data,
        headers: { "Content-type": "application/json", 'Authorization': token }
    } )
        .then( function ( response )
        {
            const data = response.data;
            cb( null, data );
        } )
        .catch( function ( error )
        {
            cb( error, null );
        } );
}

export function apiRegister ( data, cb )
{
    axios( {
        method: 'post',
        url: 'api/auth/register',
        baseURL: dev_url,
        data: data,
        headers: { "Content-type": "application/json" }
    } )
        .then( function ( response )
        {
            const data = response.data;
            cb( null, data );
        } )
        .catch( function ( error )
        {
            cb( error, null );
        } );
}

export function apiLogin ( data, cb )
{
    axios( {
        method: 'post',
        url: 'api/auth',
        baseURL: dev_url,
        data: data,
        headers: { "Content-type": "application/json" }
    } )
        .then( function ( response )
        {
            const data = response.data;
            cb( null, data );
        } )
        .catch( function ( error )
        {
            cb( error, null );
        } );
}

export function apiLogout ( token, cb )
{
    axios( {
        method: 'post',
        url: 'api/auth/logout',
        baseURL: dev_url,
        headers: { "Content-type": "application/json", "Authorization": token }
    } )
        .then( function ( response )
        {
            const data = response.data;
            cb( null, data );
        } )
        .catch( function ( error )
        {
            cb( error, null );
        } );
}

export function apiCheckLogin ( token, cb )
{
    axios( {
        method: 'get',
        url: 'api/auth',
        baseURL: dev_url,
        headers: { "Content-type": "application/json", "Authorization": token }
    } )
        .then( function ( response )
        {
            const data = response.data;
            cb( null, data );
        } )
        .catch( function ( error )
        {
            cb( error, null );
        } );
}
