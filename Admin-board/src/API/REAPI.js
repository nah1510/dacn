import axios from 'axios';


const dev_url = "https://gondarpham.xyz/";

export function apiUserPostRE ( token, data, cb )
{
    axios( {
        method: 'post',
        url: 'api/user/real-estate',
        baseURL: dev_url,
        data: data,
        headers: { "Content-type": "application/json", 'Authorization': token }
    } )
        .then( function ( response )
        {
            const data = response.data;
            cb( null, data );
        } )
        .catch( function ( error )
        {
            cb( error, null );
        } );
}

export function apiGetREAdmin ( token, query = '', cb )
{
    axios( {
        method: 'get',
        url: 'api/admin/real-estate?limit=0' + `${ query != '' ? "&" + query : '' }`,
        baseURL: dev_url,
        headers: { "Content-type": "application/json", 'Authorization': token }
    } ).then( function ( response )
    {
        const data = response.data;
        cb( null, data );
    } ).catch( function ( error )
    {
        cb( error, null );
    } );
}

export function apiGetDetailRE ( id, cb )
{
    axios( {
        method: 'get',
        url: 'api/real-estate/' + id,
        baseURL: dev_url,
        headers: { "Content-type": "application/json" }
    } ).then( function ( response )
    {
        const data = response.data.data;
        cb( null, data );
    } ).catch( function ( error )
    {
        cb( error, null );
    } );
}

export function apiSearchRealEstate ( query = '', cb )
{
    axios( {
        method: 'get',
        url: 'api/real-estate' + `${ query != '' ? "?" + query : '' }`,
        baseURL: dev_url,
        headers: { "Content-type": "application/json" }
    } ).then( function ( response )
    {
        const data = response.data.data;
        cb( null, data );
    } ).catch( function ( error )
    {
        cb( error, null );
    } );
}

export function apiGetRE ( id, cb )
{
    axios( {
        method: 'get',
        url: `api/real-estate/${ id }`,
        baseURL: dev_url,
        headers: { "Content-type": "application/json" }
    } ).then( function ( response )
    {
        const data = response.data.data;
        cb( null, data );
    } ).catch( function ( error )
    {
        cb( error, null );
    } );
}

// user API
export function apiGetUserListRE ( token, query, cb )
{
    axios( {
        method: 'get',
        url: 'api/user/real-estate?' + query,
        baseURL: dev_url,
        headers: { "Content-type": "application/json", 'Authorization': token }
    } ).then( function ( response )
    {
        const data = response.data.data;
        cb( null, data );
    } ).catch( function ( error )
    {
        cb( error, null );
    } );
}
export function apiGetUserDetailRE ( token, id, cb )
{
    axios( {
        method: 'get',
        url: `api/user/real-estate/${ id }`,
        baseURL: dev_url,
        headers: { "Content-type": "application/json", 'Authorization': token }
    } ).then( function ( response )
    {
        const data = response.data.data;
        cb( null, data );
    } ).catch( function ( error )
    {
        cb( error, null );
    } );
}

export function apiUserCommentPostRE ( token, id, data, cb )
{
    axios( {
        method: 'post',
        url: 'api/real-estate/' + id + '/comment',
        baseURL: dev_url,
        data: data,
        headers: { "Content-type": "application/json", 'Authorization': token }
    } )
        .then( function ( response )
        {
            const data = response.data;
            cb( null, data );
        } )
        .catch( function ( error )
        {
            cb( error, null );
        } );
}

