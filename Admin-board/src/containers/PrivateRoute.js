import React, { useEffect, useState } from "react";
import { Route, Redirect } from "react-router-dom";
import { apiCheckLogin } from '../API';
import Cookies from 'js-cookie';

const PrivateRoute = ( props ) =>
{

  const [ isLogin, setIsLogin ] = useState( false );
  const token = Cookies.get( 'user' );
  useEffect( () =>
  {
    apiCheckLogin( token, ( err, res ) =>
    {
      if ( err ) return;
      else
      {
        console.log( 'res Private route', res );
        if ( res.success )
        {
          setIsLogin( true );
        }
      }

    } );
    return;
  }, [] );
  // const condition = performValidationHere();

  return isLogin ? ( <Route path={ props.path } exact={ props.exact } render={ props.render } /> ) :
    ( <Redirect to="/login" /> );
};
export default PrivateRoute;
