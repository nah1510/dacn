import React, { useState, useEffect } from 'react';
import { Router, useHistory, useLocation } from 'react-router-dom';
import
{
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CPagination,
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
} from '@coreui/react';
import { apiGetUserList, deleteUser, apiCheckLogin } from '../../API';
import usersData_fake from './UsersData';
import { formatDate } from '../../reusable';
import Cookies from 'js-cookie';

const getBadge = status =>
{
  switch ( status )
  {
    case 'Active': return 'success';
    case 'Inactive': return 'secondary';
    case 'Pending': return 'warning';
    case 'Banned': return 'danger';
    default: return 'primary';
  }
};
const fields = [ { key: 'name', label: 'Họ và Tên', _classes: 'font-weight-bold', _style: { width: '20%' } }, { key: 'email', label: 'email', _style: { width: '20%' } },
{ key: 'phone_number', label: 'Điện thoại', _style: { width: '10%' } },
{ key: 'created_at', label: 'Ngày tạo', _style: { width: '10%' } }, { key: 'action', label: '', sorter: false, filter: false, _style: { width: '20%' } } ];
const Users = () =>
{
  const history = useHistory();
  const queryPage = useLocation().search.match( /page=([0-9]+)/, '' );
  const currentPage = Number( queryPage && queryPage[ 1 ] ? queryPage[ 1 ] : 1 );
  const [ page, setPage ] = useState( currentPage );
  const [ itemsPerPage, setItemPerpage ] = useState( 5 );
  const [ usersData, setUsersData ] = useState( usersData_fake );
  const [ modal, setModal ] = useState( false );
  const [ selectedUser, setSelectedUser ] = useState( null );
  const token = Cookies.get( 'user' );

  const pageChange = newPage =>
  {
    currentPage !== newPage && history.push( `/users?page=${ newPage }` );
  };
  // console.log('user items: ', usersData.length);
  useEffect( () =>
  {
    apiCheckLogin( token, ( err, res ) =>
    {
      if ( err ) history.push( '/login' );
      else
      {
        if ( !res.success )
        {
          history.push( '/login' );
        }
      }
    } );
    currentPage !== page && setPage( currentPage );
    apiGetUserList( token, ( err, res ) =>
    {
      if ( err ) return;
      setUsersData( res.data );
      console.log( 'list user: ', res );
    } );
  }, [ currentPage, page ] );

  return (
    <CRow>
      <CCol xl={ 12 }>
        <CCard>
          <CCardHeader>
            Users
          </CCardHeader>
          <CCardBody>
            <CDataTable
              items={ usersData }
              fields={ fields }
              hover
              columnFilter
              tableFilter
              itemsPerPageSelect
              sorter={ true }
              striped
              itemsPerPage={ itemsPerPage }
              activePage={ page }
              clickableRows
              onPaginationChange={ ( e ) =>
              {
                setItemPerpage( e );
              } }
              scopedSlots={ {
                // 'status':
                //   (item)=>(
                //     <td>
                //       <CBadge color={getBadge(item.status)}>
                //         {item.status}
                //       </CBadge>
                //     </td>
                //   ),
                'created_at': ( item ) => (
                  <td>
                    {formatDate( item.created_at ) }
                  </td>
                ),
                'phone_number': ( item ) => (
                  <td>
                    { item.phone_number ? item.phone_number : "Chưa đăng ký số điện thoại" }
                  </td>
                ),
                'action':
                  ( item ) =>
                  {
                    return (
                      <td className="flex-center trow">
                        <CButton
                          color="primary"
                          variant="outline"
                          shape="square"
                          size="sm"
                          onClick={ ( e ) =>
                          {
                            e.preventDefault();
                            history.push( `/users/${ item.id }` );
                          } }
                        >
                          Chi tiết
                      </CButton>
                        <CButton
                          color="primary"
                          variant="outline"
                          shape="square"
                          size="sm"
                          onClick={ () =>
                          {
                            setSelectedUser( item );
                            setModal( !modal );
                          } }
                        >
                          Xóa
                      </CButton>
                      </td>
                    );
                  },
                'create_at':
                  ( item ) =>
                  {

                  }
              } }
            />
            <CPagination
              activePage={ page }
              onActivePageChange={ pageChange }
              pages={ `${ Math.ceil( usersData ? usersData.length / itemsPerPage : 5 ) }` }
              doubleArrows={ false }
              align="center"
            />
          </CCardBody>
        </CCard>
      </CCol>
      <CModal
        show={ modal }
        onClose={ setModal }
      >
        <CModalHeader closeButton>
          <CModalTitle>Xóa Người dùng</CModalTitle>
        </CModalHeader>
        <CModalBody>
          Bạn có chắc muốn xóa user { selectedUser ? selectedUser.name : '' } ?
        </CModalBody>
        <CModalFooter>
          <CButton
            color="secondary"
            onClick={ () => setModal( false ) }
          >Cancel</CButton>
          <CButton
            color="primary"
            onClick={ () => deleteUser( token, selectedUser.id, ( err, res ) =>
            {
              if ( err ) return;
              if ( res )
              {
                apiGetUserList( token, ( err, res ) =>
                {
                  if ( err ) return;
                  setUsersData( res.data );
                  setModal( !modal );
                  console.log( 'list user: ', res );
                } );
              }
              return;
            } ) }
          >Đồng ý</CButton>
        </CModalFooter>
      </CModal>
    </CRow>
  );
};

export default Users;
