import React, { useEffect, useState } from 'react';
import
{
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CSelect,
} from '@coreui/react';
import CIcon from '@coreui/icons-react';
import { apiGetUser, updateUser } from '../../API';
import { formatDate } from '../../reusable';
import Cookies from 'js-cookie';
import DatePicker from "react-datepicker";
import usersData from './UsersData';

const User = ( { match } ) =>
{
  const token = Cookies.get( 'user' );

  const [ modal, setModal ] = useState( false );
  // console.log('match pareams id: ', match.params.id);
  const [ user, setUser ] = useState( null );
  const [ userEdit, setUserEdit ] = useState( null );
  const user1 = usersData.find( user => user.id.toString() === match.params.id );

  // variable edit info
  const [ name, setName ] = useState( null );
  const [ gender, setGender ] = useState( null );
  const [ phoneNumber, setPhoneNumber ] = useState( null );
  // date picker: 
  const [ birthDay, setBirthDay ] = useState( new Date() );

  console.log( user1 );
  const userDetails = user ? Object.entries( user ) :
    [ [ 'id', ( <span><CIcon className="text-muted" name="cui-icon-ban" /> Not found</span> ) ] ];

  const handleModal = ( e ) =>
  {
    e.preventDefault();
    setModal( !modal );
  };
  const formDatas = () =>
  {
    let object = {};
    if ( phoneNumber != null ) object[ "phone_number" ] = phoneNumber;
    if ( name != null ) object[ "name" ] = name;
    if ( birthDay != null ) object[ "birthday" ] = birthDay;
    if ( gender != null ) object[ "gender" ] = gender;
    object[ "roles" ] = [ 1 ];
    return object;
  };
  const handleSavingInfo = e =>
  {
    e.preventDefault();
    console.log( 'data object user: ', formDatas() );
    updateUser( token, match.params.id, formDatas(), ( err, res ) =>
    {
      if ( err ) return;
      if ( res )
      {
        console.log( res );
        apiGetUser( token, match.params.id, ( err, res ) =>
        {
          if ( err ) console.log( err );
          console.log( 'res:', res );
          setUser( res );
        } );
        setModal( !modal );

      }
    } );
  };
  useEffect( () =>
  {
    if ( user ) console.log( 'user: ', user );
    apiGetUser( token, match.params.id, ( err, res ) =>
    {
      if ( err ) console.log( err );
      console.log( 'res:', res );
      setUser( res );
    } );
  }, [] );



  return (
    <>
      <CRow>
        <CCol lg={ 6 }>
          <CCard>
            <CCardHeader>
              User id: { match.params.id }
            </CCardHeader>
            <CCardBody>
              <table className="table table-striped table-hover">
                <tbody>
                  {
                    userDetails ?
                      userDetails.map( ( [ key, value ], index ) =>
                      {
                        if ( key == 'pivot' || key == 'permissions' || key == 'avatar' || key == 'email_verify_code' || key == 'email_verified_at' || key == 'device_token' ) return null;
                        if ( key == 'roles' )
                        {
                          // if(value == Array) {
                          //   if (value != Array(0)) value = value[0].code;
                          //   else value = 'member';
                          // }
                          if ( value.length == 0 )
                          {
                            value = 'member';
                          } else
                          {
                            value = 'supper-admin';
                          }
                        }
                        if ( key == 'created_at' || key == 'updated_at' )
                        {
                          value = formatDate( value );
                        }
                        return (
                          <tr key={ index.toString() }>
                            <td>{ `${ key }:` }</td>
                            <td><strong>{ value }</strong></td>
                          </tr>
                        );
                      } ) : null
                  }
                </tbody>
              </table>
              <CButton
                color="primary"
                variant="outline"
                shape="square"
                size="sm"
                onClick={ handleModal }
              >
                Chỉnh sửa
                </CButton>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
      <CModal
        show={ modal }
        onClose={ setModal }
      >
        <CModalHeader closeButton>
          <CModalTitle>Chỉnh sửa thông tin</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CInputGroup className="mb-3">
            <CInputGroupPrepend>
              <CInputGroupText>
                <CIcon name="cil-user" />
              </CInputGroupText>
            </CInputGroupPrepend>
            <CInput type="text" placeholder="Họ tên" autoComplete="username" defaultValue={ user ? user.name : '' } onChange={ ( e ) => setName( e.target.value ) } />
          </CInputGroup>
          <CInputGroup className="mb-3">
            <CInputGroupPrepend>
              <CInputGroupText>
                <CIcon name="cil-user" />
              </CInputGroupText>
            </CInputGroupPrepend>
            <DatePicker selected={ birthDay } onChange={ date => setBirthDay( date ) } className="form-control" />
          </CInputGroup>
          <CInputGroup className="mb-3">
            <CInputGroupPrepend>
              <CInputGroupText>
                <CIcon name="cil-user" />
              </CInputGroupText>
            </CInputGroupPrepend>
            <CSelect placeholder="Chọn giới tính" onChange={ e => { setGender( e.target.value ); } }>
              <option value="0">Nam</option>
              <option value="1">Nữ</option>
              <option value="2">Khác</option>
            </CSelect>
          </CInputGroup>
          <CInputGroup className="mb-3">
            <CInputGroupPrepend>
              <CInputGroupText>
                <CIcon name="cil-user" />
              </CInputGroupText>
            </CInputGroupPrepend>
            <CInput type="number" placeholder="Số điện thoại" onChange={ e => { setPhoneNumber( e.target.value ); } } />
          </CInputGroup>
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={ handleSavingInfo }>Lưu lại</CButton>{ ' ' }
          <CButton
            color="secondary"
            onClick={ () => setModal( false ) }
          >Thoát</CButton>
        </CModalFooter>
      </CModal>
    </>
  );
};

export default User;
