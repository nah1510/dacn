import React, { useState, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { useCookies } from "react-cookie";
import { Link } from 'react-router-dom';
import
{
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow
} from '@coreui/react';
import CIcon from '@coreui/icons-react';
import { apiLogin } from '../../../API';

const Login = () =>
{
  const [ mailValue, setMailValue ] = useState( '' );
  const [ passwordValue, setPasswordValue ] = useState( '' );
  const [ cookie, setCookie ] = useCookies( [ "user" ] );
  const history = useHistory();

  const handleOnClick = ( e ) =>
  {
    e.preventDefault();
    const formData = new FormData();
    formData.append( "email", mailValue );
    formData.append( "password", passwordValue );
    apiLogin( formData, ( err, res ) =>
    {
      if ( err ) console.log( 'err: ', err );
      else
      {
        console.log( 'res: ', res );
        if ( res.success == true )
        {
          // console.log('login res:', res);
            setCookie( "user", res.data.token, {
              path: "/",
              maxAge: 3600, // Expires after 1hr
              sameSite: true,
            } );
            history.push( '/' );
        }
      }
    } );
  };

  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="8">
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm>
                    <h1>Đăng nhập</h1>
                    <p className="text-muted">Xin vui lòng đăng nhập để tiếp tục truy cập</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        value={ mailValue }
                        onChange={ ( e ) => { setMailValue( e.target.value ); } }
                        type="text"
                        placeholder="Nhập tên đăng nhập"
                        autoComplete="username"
                      />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        type="password"
                        placeholder="Nhập mật khẩu"
                        autoComplete="current-password"
                        value={ passwordValue }
                        onChange={ ( e ) => { setPasswordValue( e.target.value ); } }
                      />
                    </CInputGroup>
                    <CRow>
                      <CCol xs="6">
                        <CButton color="primary" className="px-4" onClick={ handleOnClick }>Login</CButton>
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
              <CCard className="text-white bg-primary py-5 d-md-down-none" style={ { width: '44%' } }>
                <CCardBody className="text-center">
                  <div>
                    <h2>Đăng nhập admin</h2>
                  </div>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default Login;
