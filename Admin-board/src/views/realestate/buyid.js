import React, { useEffect, useState } from 'react';
import
{
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CSelect,
} from '@coreui/react';
import CIcon from '@coreui/icons-react';
import { apiGetUser, updateUser, apiGetREAdmin, apiGetDetailRE } from '../../API';
import { formatDate, formatPrice } from '../../reusable';
import Cookies from 'js-cookie';
import DatePicker from "react-datepicker";
import builtData from './buydata';

const direction_Array = [ 'Bắc', 'T.Bắc', 'Đ.Bắc', 'Nam',
  'T.Nam', 'Đ.Nam', 'Bắc', 'Tây' ];

const type_Array = [ 'Nhà riêng', 'Chung cư / căn hộ', 'Đất nền' ];

const posttype_Array = [ 'Bán', 'Cho thuê' ];
const status = [ 'Không hoạt động', 'Đang hoạt động' ];

const subTitle = { image: 'Ảnh đại diện', tittle: 'Chung cư', street: 'Tên đường', price: 'Giá tiền', adress: 'Địa chỉ', year_built: 'Năm xây dựng', acreage: 'Diện tích', land_acreage: 'Diện tích đất nềnnền', length: 'Chiều dài', width: 'Chiều rộng', type: 'Loại hình giao dịch', post_type: 'Loại bất động sản', description: 'Mô tả', bathroom: 'Số Phòng tắm', bedroom: 'Số phòng ngủ', expired_time: 'Ngày hết hạn bài viết', direction: 'Hướng', created_at: 'Ngày tạo bài viết', updated_at: 'Ngày cập nhật bài viết', user: 'Tên Người sở hữu', floor: 'Số tầng', status: 'Trạng thái' };

const BuyId = ( { match } ) =>
{
  const token = Cookies.get( 'user' );

  const [ buyData, setbuyData ] = useState( null );

  // console.log( user1 );
  const buyDetails = buyData ? Object.entries( buyData ) :
    [ [ 'id', ( <span><CIcon className="text-muted" name="cui-icon-ban" /> Not found</span> ) ] ];
  useEffect( () =>
  {
    apiGetDetailRE( match.params.id, ( err, res ) =>
    {
      if ( err ) return;
      setbuyData( res );
    } );
  }, [] );



  return (
    <>
      <CRow>
        <CCol lg={ 6 }>
          <CCard>
            <CCardHeader>
              id của bất động sản: { match.params.id }
            </CCardHeader>
            <CCardBody>
              <table className="table table-striped table-hover">
                <tbody>
                  {
                    buyDetails ?
                      buyDetails.map( ( [ key, value ], index ) =>
                      {
                        if ( key == 'comments' || key == 'district' || key == 'images' || key == 'province' || key == 'ward' || key == 'info' || key == 'province_id' || key == 'district_id' || key == 'views' || key == 'ward_id' || key == 'user_id' || key == 'id' ) return null;
                        if ( key == 'roles' )
                        {
                          // if(value == Array) {
                          //   if (value != Array(0)) value = value[0].code;
                          //   else value = 'member';
                          // }
                          if ( value.length == 0 )
                          {
                            value = 'member';
                          } else
                          {
                            value = 'supper-admin';
                          }
                        }
                        if ( key == 'created_at' || key == 'updated_at' )
                        {
                          value = formatDate( value );
                        }
                        if ( key == 'user' )
                        {
                          value = value.name;
                        }
                        if ( key == 'price' )
                        {
                          value = formatPrice( value );
                        }
                        if ( key == 'type' )
                        {
                          value = type_Array[ value ];
                        }
                        if ( key == 'status' )
                        {
                          value = status[ value ];
                        }
                        if ( key == 'post_type' )
                        {
                          value = posttype_Array[ value ];
                        }
                        if ( key == 'direction' )
                        {
                          value = direction_Array[ value ];
                        }
                        return (
                          <tr key={ index.toString() }>
                            <td>{ `${ subTitle[ key ] }:` }</td>
                            <td><strong>{ value }</strong></td>
                          </tr>
                        );
                      } ) : null
                  }
                </tbody>
              </table>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};
export default BuyId;
