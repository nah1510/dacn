import React, { useState, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import
{
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CPagination,
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
} from '@coreui/react';
import { apiGetUserList, deleteUser, apiGetREAdmin } from '../../API';
import rentData from './rentdata';
import { formatDate } from '../../reusable';
import Cookies from 'js-cookie';

const getBadge = status =>
{
  switch ( status )
  {
    case 'Active': return 'success';
    case 'Inactive': return 'secondary';
    case 'Pending': return 'warning';
    case 'Banned': return 'danger';
    default: return 'primary';
  }
};
const fields = [ { key: 'tittle', label: 'Tiêu đề', _classes: 'font-weight-bold', _style: { width: '40%' } }, { key: 'user', label: 'Người sở hữu', _style: { width: '15%' } },
{ key: 'expired_time', label: 'Ngày hết hạn', _style: { width: '15%' } },
{ key: 'created_at', label: 'Ngày tạo', _style: { width: '15%' } }, { key: 'action', label: '', sorter: false, filter: false, _style: { width: '15%' } } ];
const fields2 = [
  { key: 'name', _classes: 'font-weight-bold' },
  'registered', 'role', 'status'
];

const RentSection = () =>
{
  const history = useHistory();
  const queryPage = useLocation().search.match( /page=([0-9]+)/, '' );
  const currentPage = Number( queryPage && queryPage[ 1 ] ? queryPage[ 1 ] : 1 );
  const [ page, setPage ] = useState( currentPage );
  const [ itemsPerPage, setItemPerpage ] = useState( 5 );
  const [ rentData, setrentData ] = useState( rentData );
  const token = Cookies.get( 'user' );

  const pageChange = newPage =>
  {
    currentPage !== newPage && history.push( `/realestate/rent?page=${ newPage }` );
  };
  useEffect( () =>
  {
    currentPage !== page && setPage( currentPage );
    apiGetREAdmin( token, 'post_type=1', ( err, res ) =>
    {
      if ( err ) return;
      setrentData( res.data );
    } );
  }, [ currentPage, page ] );

  return (
    <CRow>
      <CCol xl={ 12 }>
        <CCard>
          <CCardHeader>
            Thống kê bất động sản bán
          </CCardHeader>
          <CCardBody>
            <CDataTable
              items={ rentData }
              fields={ fields }
              hover
              columnFilter
              tableFilter
              itemsPerPageSelect
              sorter={ true }
              striped
              itemsPerPage={ itemsPerPage }
              activePage={ page }
              clickableRows
              responsive
              onPaginationChange={ ( e ) =>
              {
                setItemPerpage( e );
              } }
              scopedSlots={ {
                // 'status':
                //   (item)=>(
                //     <td>
                //       <CBadge color={getBadge(item.status)}>
                //         {item.status}
                //       </CBadge>
                //     </td>
                //   ),
                'user': ( item ) =>
                {
                  if ( item.user )
                  {
                    return (
                      <td>
                        { item.user.name }
                      </td>
                    );
                  }
                  else
                  {
                    return (
                      <td>
                      </td>
                    );
                  }
                },
                'created_at': ( item ) => (
                  <td>
                    { formatDate( item.created_at ) }
                  </td>
                ),
                'action':
                  ( item ) =>
                  {
                    return (
                      <td className="flex-center trow">
                        <CButton
                          color="primary"
                          variant="outline"
                          shape="square"
                          size="sm"
                          onClick={ ( e ) =>
                          {
                            e.preventDefault();
                            history.push( `/realestate/rent/${ item.id }` );
                          } }
                        >
                          Chi tiết
                      </CButton>
                      </td>
                    );
                  },
                'create_at':
                  ( item ) =>
                  {

                  }
              } }
            />
            <CPagination
              activePage={ page }
              onActivePageChange={ pageChange }
              pages={ `${ Math.floor( rentData ? rentData.length / itemsPerPage : 5 ) }` }
              doubleArrows={ false }
              align="center"
            />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  );
};

export default RentSection;
