import React, { useState, useEffect } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CPagination,
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
} from '@coreui/react';
import { apiGetUserList } from '../../API/users';
import { formatDate } from '../../reusable'
import Cookies from 'js-cookie';
import { DocsLink } from 'src/reusable'
import usersStatisticData_fake from '../users/UsersData';
import {
  CChartBar,
  CChartLine,
  CChartDoughnut,
  CChartRadar,
  CChartPie,
  CChartPolarArea
} from '@coreui/react-chartjs'
// import usersStatisticData from './userstatisticdata'

const getBadge = status => {
  switch (status) {
    case 'Active': return 'success'
    case 'Inactive': return 'secondary'
    case 'Pending': return 'warning'
    case 'Banned': return 'danger'
    default: return 'primary'
  }
}

const fields = [{key: 'name', label: 'Họ và Tên', _classes: 'font-weight-bold'}, {key: 'email', label: 'email'},
  {key: 'phone_number', label: 'Điện thoại'}, 
  {key: 'created_at', label: 'Ngày tạo'}]

const TotalStatistic = () => {
  const history = useHistory()
  const queryPage = useLocation().search.match(/page=([0-9]+)/, '')
  const currentPage = Number(queryPage && queryPage[1] ? queryPage[1] : 1)
  const [page, setPage] = useState(currentPage)
  const [itemsPerPage, setItemPerpage] = useState(5);
  const [usersData, setUsersData] = useState(usersStatisticData_fake);
  // const [modal, setModal] = useState(false)
  const token = Cookies.get('user');

  const pageChange = newPage => {
    currentPage !== newPage && history.push(`/statistics/userstatistic?page=${newPage}`)
  }

  useEffect(() => {
    currentPage !== page && setPage(currentPage);
    apiGetUserList(token, (err, res)=> {
      if(err) return;
      setUsersData(res);
      console.log('list user: ', res);
    })
  }, [currentPage, page])

  return (
    <>
      <CRow>
        <CCol xl={6}>
          <CCard>
            <CCardHeader>
              Users
            </CCardHeader>
            <CCardBody>
            <CDataTable
              items={usersData}
              fields={fields}
              hover
              columnFilter
              tableFilter
              itemsPerPageSelect
              sorter={true}
              striped
              itemsPerPage={itemsPerPage}
              activePage={page}
              clickableRows
              onPaginationChange={(e) => {
                setItemPerpage(e);
              }}
              scopedSlots = {{
                'created_at': (item) =>(
                  <td>
                    {formatDate(item.created_at)}
                  </td>
                ),
              }}
            />
            <CPagination
              activePage={page}
              onActivePageChange={pageChange}
              pages={`${Math.ceil(usersData ? usersData.length / itemsPerPage : 5)}`}
              doubleArrows={false} 
              align="center"
            />
            </CCardBody>
          </CCard>
        </CCol>
        <CCol xl={6}>
          <CCard>
            <CCardHeader>
              Bar Chart
            </CCardHeader>
            <CCardBody>
              <CChartBar
                datasets={[
                  {
                    label: 'GitHub Commits',
                    backgroundColor: '#f87979',
                    data: [40, 20, 12, 39, 10, 40, 39, 80, 40, 20, 12, 11]
                  }
                ]}
                labels="months"
                options={{
                  tooltips: {
                    enabled: true
                  }
                }}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
  </>
  )
}

export default TotalStatistic;
