import React, { useState, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import
{
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CPagination,
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
} from '@coreui/react';
import { apiGetUserList } from '../../API/users';
import { formatDate } from '../../reusable';
import Cookies from 'js-cookie';
import { DocsLink } from 'src/reusable';
import usersStatisticData_fake from '../users/UsersData';
import
{
  CChartBar,
  CChartLine,
  CChartDoughnut,
  CChartRadar,
  CChartPie,
  CChartPolarArea
} from '@coreui/react-chartjs';
// import usersStatisticData from './userstatisticdata'

const getBadge = status =>
{
  switch ( status )
  {
    case 'Active': return 'success';
    case 'Inactive': return 'secondary';
    case 'Pending': return 'warning';
    case 'Banned': return 'danger';
    default: return 'primary';
  }
};

const fields = [ { key: 'name', label: 'Họ và Tên', _classes: 'font-weight-bold' }, { key: 'email', label: 'email' },
{ key: 'phone_number', label: 'Điện thoại' },
{ key: 'created_at', label: 'Ngày tạo' } ];

const UsersStatistic = () =>
{
  const history = useHistory();
  const queryPage = useLocation().search.match( /page=([0-9]+)/, '' );
  const currentPage = Number( queryPage && queryPage[ 1 ] ? queryPage[ 1 ] : 1 );
  const [ page, setPage ] = useState( currentPage );
  const [ itemsPerPage, setItemPerpage ] = useState( 5 );
  const [ usersData, setUsersData ] = useState( usersStatisticData_fake );
  const [ totalUsers, setTotalUsers ] = useState( usersStatisticData_fake );
  // const [modal, setModal] = useState(false)
  const token = Cookies.get( 'user' );

  const pageChange = newPage =>
  {
    currentPage !== newPage && history.push( `/statistics/userstatistic?page=${ newPage }` );
  };

  const countDataByMonth = ( usersData ) =>
  {
    let m1 = 0, m2 = 0, m3 = 0, m4 = 0, m5 = 0, m6 = 0, m7 = 0, m8 = 0, m9 = 0, m10 = 0, m11 = 0, m12 = 0;
    if ( usersData )
    {
      usersData.map( ( e, index ) =>
      {
        const date = new Date( e.created_at );
        switch ( date.getMonth() )
        {
          case 0:
            m1 = m1 + 1;
            break;
          case 1:
            m2 = m2 + 1;
            break;
          case 2:
            m3 = m3 + 1;
            break;
          case 3:
            m4 = m4 + 1;
            break;
          case 4:
            m5 = m5 + 1;
            break;
          case 5:
            m6 = m6 + 1;
            break;
          case 6:
            m7 = m7 + 1;
            break;
          case 7:
            m8 = m8 + 1;
            break;
          case 8:
            m9 = m9 + 1;
            break;
          case 9:
            m10 = m10 + 1;
            break;
          case 10:
            m11 = m11 + 1;
            break;
          case 11:
            m12 = m12 + 1;
            break;
          default:
            console.log( 'invalid data' );
        }
      } );
    }
    console.log( 'userData: ', usersData );
    return [ m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12 ];
  };

  useEffect( () =>
  {
    currentPage !== page && setPage( currentPage );
    apiGetUserList( token, ( err, res ) =>
    {
      if ( err ) return;
      setUsersData( res.data );
      console.log( 'list user: ', res );
    } );
  }, [ currentPage, page ] );

  return (
    <>
      <CRow>
        <CCol xl={ 6 }>
          <CCard>
            <CCardHeader>
              Bảng thống kê người dùng
            </CCardHeader>
            <CCardBody>
              <CDataTable
                items={ usersData }
                fields={ fields }
                hover
                columnFilter
                tableFilter
                itemsPerPageSelect
                sorter={ true }
                striped
                itemsPerPage={ itemsPerPage }
                activePage={ page }
                clickableRows
                onPaginationChange={ ( e ) =>
                {
                  setItemPerpage( e );
                } }
                scopedSlots={ {
                  'created_at': ( item ) => (
                    <td>
                      {formatDate( item.created_at ) }
                    </td>
                  ),
                } }
              />
              <CPagination
                activePage={ page }
                onActivePageChange={ pageChange }
                pages={ `${ Math.floor( usersData ? usersData.length / itemsPerPage : 5 ) }` }
                doubleArrows={ false }
                align="center"
              />
            </CCardBody>
          </CCard>
        </CCol>
        <CCol xl={ 6 }>
          <CCard>
            <CCardHeader>
              Biểu đồ thống kê người dùng
            </CCardHeader>
            <CCardBody>
              <CChartBar
                datasets={ [
                  {
                    label: 'Lượng User đăng ký trong 1 tháng',
                    backgroundColor: '#f87979',
                    data: countDataByMonth( usersData )
                  }
                ] }
                labels="months"
                options={ {
                  tooltips: {
                    enabled: true
                  }
                } }
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default UsersStatistic;
