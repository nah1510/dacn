import React, {useEffect, useState} from 'react';
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CSelect,
} from '@coreui/react';
import CIcon from '@coreui/icons-react';
import { apiGetUser } from '../../API';
import { formatDate } from '../../reusable'
import Cookies from 'js-cookie';
import DatePicker from "react-datepicker";
import usersData from './UsersData'

const User = ({match}) => {
  const token = Cookies.get('user');
  
  const [ modal, setModal ] = useState(false)
  // console.log('match pareams id: ', match.params.id);
  const [user, setUser] = useState(null);
  const user1 = usersData.find( user => user.id.toString() === match.params.id);

  // variable edit info
  const [name, setName] = useState('');
  const [genter, setGender ] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  // date picker: 
  const [birthDay, setBirthDay] = useState(new Date());

  console.log(user1);
  const userDetails = user ? Object.entries(user) : 
    [['id', (<span><CIcon className="text-muted" name="cui-icon-ban" /> Not found</span>)]]

  const handleModal = (e) => {
    e.preventDefault();
    setModal(!modal);
  }
  const handleSavingInfo = e => {
    e.preventDefault();
    console.log('saved');
  }
  useEffect(()=> {
    apiGetUser(token,match.params.id, (err, res) => {
      if(err) console.log(err);
      console.log('res:', res);
      setUser(res);
    })
  },[])

  

  return (
    <>
      <CRow>
        <CCol lg={6}>
          <CCard>
            <CCardHeader>
              User id: {match.params.id}
            </CCardHeader>
            <CCardBody>
                <table className="table table-striped table-hover">
                  <tbody>
                    {
                      userDetails ? 
                      userDetails.map(([key, value], index) => {
                        if(key == 'pivot' || key == 'permissions' || key == 'avatar' || key == 'email_verify_code' || key == 'email_verified_at' || key == 'device_token') return null;
                        if(key == 'roles'){
                          // if(value == Array) {
                          //   if (value != Array(0)) value = value[0].code;
                          //   else value = 'member';
                          // }
                          if(value.length == 0){
                            value = 'member';
                          } else {
                            value = 'supper-admin';       
                          }
                        }
                        if(key == 'created_at' || key == 'updated_at') {
                          value = formatDate(value);
                        }
                        return (
                          <tr key={index.toString()}>
                              <td>{`${key}:`}</td>
                              <td><strong>{value}</strong></td>
                          </tr>
                        )
                      }) : null
                    }
                  </tbody>
                </table>
                <CButton
                  color="primary"
                  variant="outline"
                  shape="square"
                  size="sm"
                  onClick={handleModal}
                >
                  Chỉnh sửa
                </CButton>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
      <CModal 
        show={modal} 
        onClose={setModal}
      >
        <CModalHeader closeButton>
          <CModalTitle>Chỉnh sửa thông tin</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CInputGroup className="mb-3">
            <CInputGroupPrepend>
              <CInputGroupText>
                <CIcon name="cil-user" />
              </CInputGroupText>
            </CInputGroupPrepend>
            <CInput type="text" placeholder="Họ và tên" autoComplete="username" />
          </CInputGroup>
          <CInputGroup className="mb-3">
            <CInputGroupPrepend>
              <CInputGroupText>
                <CIcon name="cil-user" />
              </CInputGroupText>
            </CInputGroupPrepend>
            <DatePicker selected={birthDay} onChange={date => setBirthDay(date)} className="form-control"/>
          </CInputGroup>
          <CInputGroup className="mb-3">
            <CInputGroupPrepend>
              <CInputGroupText>
                <CIcon name="cil-user" />
              </CInputGroupText>
            </CInputGroupPrepend>
            <CSelect placeholder="Chọn giới tính" >
              <option>Nam</option>
              <option>Nữ</option>
              <option>Khác</option>
            </CSelect>
          </CInputGroup>
          <CInputGroup className="mb-3">
            <CInputGroupPrepend>
              <CInputGroupText>
                <CIcon name="cil-user" />
              </CInputGroupText>
            </CInputGroupPrepend>
            <CInput type="text" placeholder="Họ tên" autoComplete="username" value={user ? user.name : ''}/>
          </CInputGroup>
          <CInputGroup className="mb-3">
            <CInputGroupPrepend>
              <CInputGroupText>
                <CIcon name="cil-user" />
              </CInputGroupText>
            </CInputGroupPrepend>
            <CInput type="number" placeholder="Số điện thoại"/>
          </CInputGroup>
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={handleSavingInfo}>Lưu lại</CButton>{' '}
          <CButton 
            color="secondary" 
            onClick={() => setModal(false)}
          >Thoát</CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default User
