require( 'dotenv' ).config();
module.exports = {
  env: {
    customKey: 'my-value',
    URL: process.env.HOSTNAME,
    API_URL: process.env.APIHOSTNAME,
  },
  webpack ( config )
  {
    config.module.rules.push( {
      test: /\.svg$/,
      use: [ "@svgr/webpack" ]
    } );

    return config;
  }
};
