import { useState, useEffect, useRef } from 'react';
import axios from 'axios';

export function useInterval ( callback, delay )
{
    const savedCallback = useRef( () => { } );
    useEffect( () =>
    {
        savedCallback.current = callback;
    }, [ callback ] );
    useEffect( () =>
    {
        function tick ()
        {
            savedCallback.current();
        }
        if ( delay !== null )
        {
            let id = setInterval( tick, delay );
            return () => clearInterval( id );
        }
    }, [ delay ] );
}
export function useFakeProgress ()
{
    const [ fakeProgress, setFakeProgress ] = useState( 0 );
    const [ isActive, setIsActive ] = useState( false );
    function stopFakeProgress ()
    {
        setIsActive( false );
        setFakeProgress( 0 );
    }
    function startFakeProgress ()
    {
        setIsActive( true );
    }
    useInterval(
        () =>
        {
            if ( fakeProgress >= 100 )
            {
                stopFakeProgress();
            } else
            {
                setFakeProgress( fakeProgress + 10 );
            }
        },
        isActive ? 500 : null,
    );
    return [ fakeProgress, startFakeProgress, stopFakeProgress ];
}


export function onFileUpload ()
{
    const formData = new FormData();
    formData.append(
        "myFile",
        this.state.selectedFile,
        this.state.selectedFile.name
    );
    console.log( formData );
};

export function formatDate ( dateinfo )
{
    const date = new Date( dateinfo );
    if ( date.getDate() < 10 )
    {
        return `0` + date.getDate() + "/" + date.getMonth() + 1 + "/" + date.getFullYear();
    }
    return date.getDate() + "/" + date.getMonth() + 1 + "/" + date.getFullYear();
}

export function formatDate1 ( dateinfo )
{
    const date = new Date( dateinfo );
    const month = date.getMonth() + 1;
    return `${ date.getFullYear() }-${ date.getMonth() < 10 ? `0` + month : month }-${ date.getDate() < 10 ? `0` + date.getDate() : date.getDate() }`;
}

export function getParameterByName ( name, url = '/' )
{
    name = name.replace( /[\[\]]/g, '\\$&' );
    var regex = new RegExp( '[?&]' + name + '(=([^&#]*)|&|#|$)' ),
        results = regex.exec( url );
    if ( !results ) return null;
    if ( !results[ 2 ] ) return '';
    return decodeURIComponent( results[ 2 ].replace( /\+/g, ' ' ) );
}

export const ERROR_MODAL = {
    main: `#DB4437`,
    hover: `#c23124`,
};
export const SUCCESS_MODAL = {
    main: `#08AD36`,
    hover: `#067a27`,
};
export const PRIMARY_MODAL = {
    main: `#FFAC12`,
    hover: `#ed9a00`,
};
export const WARNING_MODAL = {
    main: `#ffff00`,
    hover: `#cccc00`,
};
export const BLUE_MODAL = {
    main: `#1565D8`,
    hover: `#1053B5`,
};

export function deleteArrayItemByIndex ( arr, index )
{
    let slices;
    for ( var i = 0; i < arr.length; i++ )
    {
        if ( arr[ i ] === index )
        {

            slices = arr.splice( i, 1 );
        }
    }
    console.log( 'slices', slices );
    return slices;
}


export function formatDateString ( datecreate )
{
    const date1 = new Date( datecreate );
    const date2 = new Date();
    let date = new Date( date2 - date1 );
    console.log( 'date: ', date );
    if ( date.getFullYear() > 0 )
    {
        return `${ date.getFullYear() } năm trước`;
    }
    if ( date.getMonth() > 0 )
    {
        return `${ date.getMonth() } tháng trước`;
    }
    if ( date.getDate() > 0 )
    {
        return `${ date.getDate() } ngày trước`;
    }
    if ( date.getHours() > 0 )
    {
        return `${ date.getHours() } giờ trước`;
    }
    if ( date.getMinutes() > 0 )
    {
        return `${ date.getMinutes() } phút trước`;
    }
    return 'vừa xong';
    // console.log( 'format: ', dateFormat.getDate(), '  ', dateFormat.getMonth() );
}

export function formatPrice ( price )
{
    const bil = 1000000000;
    const mil = 1000000;
    if ( price >= bil )
    {
        return `${ Math.ceil( price / bil ) } tỷ`;
    }
    if ( price >= mil )
    {
        return `${ Math.ceil( price / mil ) } triệu`;
    }
    return price;
}


export function formatAddress ( province, district, ward, street, address )
{
    return `${ address } ${ street }, ${ ward }, ${ district }, ${ province }`;
}
export function formatAddressShort ( province, district, ward )
{
    return `${ ward }, ${ district } ${ province }`;
}
