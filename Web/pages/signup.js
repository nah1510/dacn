import Signup from "../components/signup";
import { useState, useEffect } from 'react';
import Cookies from 'js-cookie';
import { apiCheckLogin } from '../API';
import Head from 'next/head';
import { useRouter } from 'next/router';
const SignupPage = ( props ) =>
{
    const token = Cookies.get( 'user' );
    const router = useRouter();
    useEffect( () =>
    {
        apiCheckLogin( token, ( err, res ) =>
        {
            if ( err ) console.log( err );
            else
            {
                if ( res.success )
                {
                    router.push( '/' );
                }
            }
        } );
    }, [] );
    return (
        <>
            <Head>
                <link rel="icon" href="/favicon.ico" />
                <meta
                    name="description"
                    content="Learn how to build a personal website using Next.js"
                />
                <meta
                    property="og:image"
                    content={ `https://og-image.now.sh/${ encodeURI(
                        'Đăng ký'
                    ) }.png?theme=light&md=0&fontSize=75px&images=https%3A%2F%2Fassets.vercel.com%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg` }
                />
                <meta name="og:title" content={ 'Đăng ký' } />
                <meta name="twitter:card" content="summary_large_image" />
                <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests" />
            </Head>
            <div className="section">
                <Signup />
            </div>
        </>
    );
};

export default SignupPage;
