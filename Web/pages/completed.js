import React, { Component, useState, useEffect } from 'react';
import Link from 'next/link';
import Checked from '../public/svgicon/checked.svg';
import { useRouter } from 'next/router';
import Head from 'next/head';

function CompletedPage ( props )
{
    const { textCongrate, subText } = props;
    const [ seccond, setSecond ] = useState( 5 );
    const router = useRouter();

    useEffect( () =>
    {
        setTimeout( () =>
        {
            router.push( '/login' );
        }, 3000 );
    }, [] );
    return (
        <>
            <Head>
                <link rel="icon" href="/favicon.ico" />
                <meta
                    name="description"
                    content="Learn how to build a personal website using Next.js"
                />
                <meta
                    property="og:image"
                    content={ `https://og-image.now.sh/${ encodeURI(
                        'Đăng ký thành công'
                    ) }.png?theme=light&md=0&fontSize=75px&images=https%3A%2F%2Fassets.vercel.com%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg` }
                />
                <meta name="og:title" content={ 'Đăng ký thành công' } />
                <meta name="twitter:card" content="summary_large_image" />
                <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests" />
            </Head>
            <section className="congrate">
                <Link href="/">
                    <a className="login-logo">
                        <img src="/images/Logo.png" />
                    </a>
                </Link>
                <div className="congrate-box">
                    <h3 className="congrate-heading">
                        Chúc mừng bạn đã đăng ký thành công !
                </h3>
                    <p className="congrate-subText">
                        chào mừng bạn đã tham gia với cộng đồng chúng tôi
                </p>
                    <p className="congrate-redirect">
                        Bạn sẽ được chuyển hướng về trang chủ
                </p>
                </div>
            </section>
        </>
    );
};

export default CompletedPage;
