import React, { Component, useState, useEffect } from 'react';
import { FileUploader } from 'baseui/file-uploader';
import { apiPostImage } from '../API';
import { FormControl } from 'baseui/form-control';
import { Input } from 'baseui/input';
import { Checkbox } from 'baseui/checkbox';
import { Button } from "baseui/button";
import { Select } from 'baseui/select';
import { Radio, RadioGroup } from 'baseui/radio';
import { useFakeProgress } from "../reusefunc/reuse";
import Layout from '../layout/layout';
import Scrollbar from 'smooth-scrollbar';
import { Textarea } from 'baseui/textarea';
import Cookies from 'js-cookie';
import { DatePicker } from "baseui/datepicker";
import { apiCheckLogin } from '../API';
import { useRouter } from 'next/router';
import Notification from '../components/modal';
import { apiGetListProvinces, apiGetListDistricts, apiGetListWards, apiUserPostRE } from '../API';
import { deleteArrayItemByIndex, ERROR_MODAL, SUCCESS_MODAL, WARNING_MODAL, PRIMARY_MODAL, BLUE_MODAL } from '../reusefunc/reuse';

const images_array = [ '/images/background1.jpg', '/images/background1.jpg', '/images/background1.jpg', '/images/background1.jpg', '/images/background1.jpg', '/images/background1.jpg', '/images/background1.jpg', '/images/background1.jpg' ];

const direction_Array = [ { value: 0, name: 'Bắc' }, { value: 1, name: 'T.Bắc' }, { value: 2, name: 'Đ.Bắc' }, { value: 3, name: 'Nam' },
{ value: 4, name: 'T.Nam' }, { value: 5, name: 'Đ.Nam' }, { value: 6, name: 'Bắc' }, { value: 0, name: 'Tây' } ];
function PostPage ( props )
{
    const router = useRouter();
    const [
        progressAmount,
        startFakeProgress,
        stopFakeProgress,
    ] = useFakeProgress();
    const token = Cookies.get( 'user' );
    // const [fileUpload, setFileUploader] = useState(null);
    // post product 
    const [ provinces, setProvices ] = useState();

    const [ images, setImages ] = useState( [] );
    const handleDeleteImage = index =>
    {
        // console.log( 'index: ', index );
        // let arr = images_array;
        // console.log( 'arr before: ', arr );
        // arr = deleteArrayItemByIndex( images_array, index );
        // console.log( 'arr after: ', arr );
        setImages( images.filter( ( value, indexarray ) =>
        {
            return indexarray != index;
        } ) );
    };
    useEffect( () =>
    {
        apiCheckLogin( token, ( err, res ) =>
        {
            if ( err ) console.log( err );
            else
            {
                if ( !res.success )
                {
                    router.push( '/login' );
                }
            }
        } );
        apiGetListProvinces( token, ( err, res ) =>
        {
            if ( err ) return;
            else
            {
                setCities( res );
            }
        } );
        Scrollbar.init( document.getElementById( 'my-scrollbar' ),
            {
                damping: 0.05,
                thumbMinSize: 20,
                renderByPixels: true,
                alwaysShowTracks: false,
                continuousScrolling: true
            } );
        return () =>
        {
            Scrollbar.destroyAll();
        };
    }, [] );
    // swiper card-item;


    // address: set address
    const [ city, setCity ] = useState( null );
    const [ district, setDistrict ] = useState( null );
    const [ ward, setWard ] = useState( null );

    const [ cities, setCities ] = useState( [] );
    const [ districts, setDistricts ] = useState( [] );
    const [ wards, setWards ] = useState( [] );

    const [ pickday, setPickDay ] = useState( new Date() );
    const [ dayNumber, setDayNumber ] = useState( 0 );
    const [ totalMoney, setTotalMoney ] = useState( 0 );

    const [ bathroomValue, setbathroomValue ] = useState( null );
    const [ bedroomValue, setbedroomValue ] = useState( null );
    const [ description, setDescription ] = useState( null );
    const [ addressValue, setAddressValue ] = useState( null );
    const [ priceValue, setPriceValue ] = useState( null );
    const [ phoneNumber, setPhoneNumber ] = useState( null );
    const [ country, setCountry ] = useState( undefined );
    const [ typeRE, setTypeRE ] = useState( '1' );
    const [ typeExchange, setTypeExchange ] = useState( '1' );
    const [ typePosts, setTypePosts ] = useState( '1' );
    const [ text, setText ] = useState( "" );

    const [ title, setTitle ] = useState( null );
    const [ floor, setFloor ] = useState( 0 );
    const [ streetValue, setStreetValue ] = useState( null );
    const [ checkbox1, setCheckbox1 ] = useState( null );
    const [ checkbox2, setCheckbox2 ] = useState( null );
    const [ checkbox3, setCheckbox3 ] = useState( null );
    const [ checkbox4, setCheckbox4 ] = useState( null );
    const [ acreageValue, setacreageValue ] = useState( null );
    const [ land_acreageValue, setlandAcreageValue ] = useState( null );
    const [ lengthValue, setLengthValue ] = useState( null );
    const [ widthValue, setWidthValue ] = useState( null );
    const [ yearBuiltValue, setYearBuilt ] = useState( null );
    const [ directionValue, setdirectionValue ] = useState( null );

    // handle modal event
    const [ contentModal, setContentModal ] = useState( <p>đây là nội dung 2</p> );
    const [ headingModal, setHeadingModal ] = useState( "đây là tiêu đề" );
    const [ stylesModal, setStylesModal ] = useState( ERROR_MODAL );
    const [ isOpenModal, setIsOpenModal ] = useState( false );

    const handlePostProduct = ( e ) =>
    {
        let data = {};
        let array_images = [];
        for ( let i = 0; i < images.length; i++ )
        {
            array_images.push( { url: images[ i ] } );
        }

        if ( title ) data[ 'tittle' ] = title;
        if ( ward ) data[ 'ward_id' ] = ward[ 0 ].id;
        if ( city ) data[ 'province_id' ] = city[ 0 ].id;
        if ( district ) data[ 'district_id' ] = district[ 0 ].id;
        if ( streetValue ) data[ 'street' ] = streetValue;
        if ( addressValue ) data[ 'adress' ] = addressValue;
        if ( priceValue ) data[ 'price' ] = priceValue;
        if ( yearBuiltValue ) data[ 'year_built' ] = yearBuiltValue;
        if ( land_acreageValue ) data[ 'land_acreage' ] = land_acreageValue;
        if ( acreageValue ) data[ 'acreage' ] = acreageValue;
        if ( lengthValue ) data[ 'length' ] = lengthValue;
        if ( widthValue ) data[ 'width' ] = widthValue;
        if ( floor ) data[ 'floor' ] = floor;
        if ( dayNumber ) data[ 'days_display' ] = dayNumber;
        if ( bathroomValue ) data[ 'bathroom' ] = bathroomValue;
        if ( bedroomValue ) data[ 'bedroom' ] = bedroomValue;
        if ( totalMoney ) data[ 'amount' ] = totalMoney;
        if ( directionValue ) data[ 'direction' ] = directionValue[ 0 ].value;
        if ( images ) data[ 'image' ] = images[ 0 ];
        if ( description ) data[ 'description' ] = description;
        if ( array_images ) data[ 'images' ] = array_images;
        if ( typeRE ) data[ 'type' ] = typeRE;
        if ( typeExchange ) data[ 'post_type' ] = typeExchange;
        data[ 'info' ] = "info";
        apiUserPostRE( token, data, ( err, res ) =>
        {
            if ( err ) return;
            else
            {
                console.log( 'res: ', res );
                if ( res.success == true )
                {
                    setHeadingModal( 'Thông báo' );
                    setStylesModal( SUCCESS_MODAL );
                    setIsOpenModal( true );
                    setContentModal( <p className="text-success">
                        Bạn đã đăng bài thành công
                    </p> );
                } else
                {
                    setHeadingModal( 'Thông báo' );
                    setStylesModal( ERROR_MODAL );
                    setIsOpenModal( true );
                    setContentModal( <p className="danger-text">
                        { res.msg }
                    </p> );
                }
            }

        } );
    };
    return (
        <Layout >
            <div className="box-container">
                <h3>
                    <span className="list-heading">
                        Thông tin BĐS
            </span>
                </h3>
                <div className="postnews__wrapper marginTop-l">
                    <div className="postnews-left">
                        <div className="postnews-block">
                            <h5 className="textAlign-center postnews-heading">Thông tin chi tiết</h5>
                            <FormControl
                                label="Số phòng tắm*"
                            >
                                <Input
                                    onChange={ e => setbathroomValue( e.target.value ) }
                                    value={ bathroomValue }
                                    id={ props.id }
                                    placeholder={ "Nhập số phòng tắm" }
                                    type="number"
                                />
                            </FormControl>
                            <FormControl
                                label="Số phòng ngủ"
                            >
                                <Input
                                    onChange={ e => setbedroomValue( e.target.value ) }
                                    value={ bedroomValue }
                                    id={ props.id }
                                    placeholder={ "Nhập Số phòng ngủ" }
                                />
                            </FormControl>
                            <FormControl
                                label="Số tầng"
                            >
                                <Input
                                    onChange={ e => setFloor( e.target.value ) }
                                    value={ floor }
                                    id={ props.id }
                                    placeholder={ "Nhập số tầng" }
                                    type="number"
                                />
                            </FormControl>
                            <FormControl
                                label="diện tích đất nền"
                            >
                                <Input
                                    onChange={ e => setlandAcreageValue( e.target.value ) }
                                    value={ land_acreageValue }
                                    id={ props.id }
                                    placeholder={ "Nhập diện tích đất nền" }
                                    type="number"
                                />
                            </FormControl>
                            <FormControl
                                label="diện tích"
                            >
                                <Input
                                    onChange={ e => setacreageValue( e.target.value ) }
                                    value={ acreageValue }
                                    id={ props.id }
                                    placeholder={ "Nhập diện tích" }
                                    type="number"
                                />
                            </FormControl>
                            <FormControl
                                label="chiều dài"
                            >
                                <Input
                                    onChange={ e => setLengthValue( e.target.value ) }
                                    value={ lengthValue }
                                    id={ props.id }
                                    placeholder={ "Nhập chiều dài" }
                                    type="number"
                                />
                            </FormControl>
                            <FormControl
                                label="chiều rộng"
                            >
                                <Input
                                    onChange={ e => setWidthValue( e.target.value ) }
                                    value={ widthValue }
                                    id={ props.id }
                                    placeholder={ "Nhập chiều rộng" }
                                    type="number"
                                />
                            </FormControl>
                            <FormControl
                                label="năm xây"
                            >
                                <Input
                                    onChange={ e => setYearBuilt( e.target.value ) }
                                    value={ yearBuiltValue }
                                    id={ props.id }
                                    placeholder={ "Nhập năm xây" }
                                    type="number"
                                />
                            </FormControl>
                            <FormControl
                                label="Chọn hướng"
                            >
                                <Select
                                    id="select-id"
                                    value={ directionValue }
                                    onChange={ ( { value } ) => setdirectionValue( value ) }
                                    options={ direction_Array }
                                    labelKey="name"
                                    valueKey="id"
                                    placeholder="Chọn hướng"
                                />
                            </FormControl>

                        </div>
                        <div className="postnews-block">
                            <h5 className="textAlign-center postnews-heading">Ngày đăng tin</h5>
                            <FormControl
                                label="Chọn số ngày đăng*"
                            >
                                <Input
                                    onChange={ e =>
                                    {
                                        setDayNumber( e.target.value );
                                        setTotalMoney( e.target.value * 5000 );
                                    } }
                                    value={ dayNumber }
                                    id={ props.id }
                                    placeholder={ "Nhập số ngày đăng" }
                                    type="number"
                                />
                            </FormControl>
                            <FormControl
                                label="Thành tiền"
                                caption="đơn giá mỗi tin là: 5000 vnđ"
                            >
                                <Input
                                    value={ totalMoney }
                                    disabled
                                    type="number"
                                />
                            </FormControl>
                        </div>
                    </div>
                    <div className="postnews-right">
                        <div className="marginBottom-m">
                            <h5 className="textAlign-center postnews-heading">Thông tin bất động sản</h5>
                            <h5 className="postnews-form-heading">Loại hình giao dịch <span className="danger-text">*</span></h5>
                            <RadioGroup
                                name="basic usage"
                                onChange={ e =>
                                {
                                    console.log( e.target.value );
                                    setTypeExchange( e.target.value );
                                } }
                                value={ typeExchange }
                                align="horizontal"
                            >
                                <Radio value="0">Bán</Radio>
                                <Radio
                                    value="1"
                                >
                                    Cho thuê
                                </Radio>
                            </RadioGroup>
                        </div>
                        <div className="marginBottom-m">
                            <h5 className="postnews-form-heading">giá đề nghị <span className="danger-text">*</span></h5>
                            <FormControl >
                                <Input
                                    onChange={ e => setPriceValue( e.target.value ) }
                                    value={ priceValue }
                                    id={ props.id }
                                    placeholder={ "Nhập giá đề nghị" }
                                    type="number"
                                />
                            </FormControl>
                        </div>
                        <div className="marginBottom-m">
                            <h5 className="postnews-form-heading">Nhập tiêu đề<span className="danger-text">*</span></h5>
                            <FormControl >
                                <Input
                                    onChange={ e =>
                                    {
                                        if ( e.target.value.length < 100 ) setTitle( e.target.value );
                                    }
                                    }
                                    value={ title }
                                    id={ props.id }
                                    placeholder={ "Nhập tiêu đề (tối đa 100 ký tự)" }
                                />
                            </FormControl>
                        </div>
                        <div className="marginBottom-m">
                            <h5 className="postnews-form-heading">Loại bất động sản <span className="danger-text">*</span></h5>
                            <RadioGroup
                                name="basic usage"
                                onChange={ e => setTypeRE( e.target.value ) }
                                value={ typeRE }
                                align="horizontal"
                            >

                                <Radio value="0">Nhà Riêng</Radio>
                                <Radio value="1">Chung cư / Căn hộ</Radio>
                                <Radio
                                    value="2"
                                >
                                    Đất nền
                        </Radio>
                            </RadioGroup>
                        </div>
                        <h5 className="postnews-form-heading">Địa chỉ bất động sản <span className="danger-text">*</span></h5>
                        <div className="flex-2">
                            <div className="flex-item">
                                <FormControl
                                    label="Tỉnh / thành phố"
                                >
                                    <Select
                                        id="select-city"
                                        value={ city }
                                        onChange={ ( { value } ) =>
                                        {
                                            setCity( value );
                                            apiGetListDistricts( token, value[ 0 ].id, ( err, res ) =>
                                            {
                                                if ( err ) return;
                                                else
                                                {
                                                    setDistricts( res );
                                                }
                                            } );
                                        } }
                                        options={ cities }
                                        labelKey="name"
                                        valueKey="id"
                                        placeholder="Chọn tỉnh/thành phố"
                                    />
                                </FormControl>
                            </div>
                            <div className="flex-item">
                                <FormControl
                                    label="Quận / Huyện"
                                >
                                    <Select
                                        id="select-district"
                                        value={ district }
                                        onChange={ ( { value } ) =>
                                        {
                                            console.log( 'selected district: ', value );
                                            setDistrict( value );
                                            apiGetListWards( token, value[ 0 ].id, ( err, res ) =>
                                            {
                                                if ( err ) return;
                                                else
                                                {
                                                    setWards( res );
                                                }
                                            } );
                                        } }
                                        options={ districts }
                                        labelKey="name"
                                        valueKey="id"
                                        placeholder="Chọn Quận / huyện"
                                    />
                                </FormControl>
                            </div>
                        </div>
                        <div className="flex-2">
                            <div className="flex-item">
                                <FormControl
                                    label="Phường Xã"
                                >
                                    <Select
                                        id="select-ward"
                                        value={ ward }
                                        onChange={ ( { value } ) => setWard( value ) }
                                        options={ wards }
                                        labelKey="name"
                                        valueKey="id"
                                        placeholder="Chọn phường / xã"
                                    />
                                </FormControl>
                            </div>
                            <div className="flex-item">
                                <FormControl
                                    label="Tên đường"
                                >
                                    <Input
                                        onChange={ e => setStreetValue( e.target.value ) }
                                        value={ streetValue }
                                        id={ props.id }
                                        placeholder={ "Nhập tên đường" }
                                    />
                                </FormControl>
                            </div>
                        </div>

                        <FormControl
                            label="Số nhà"
                        >
                            <Input
                                onChange={ e => setAddressValue( e.target.value ) }
                                value={ addressValue }
                                id={ props.id }
                                placeholder={ "Nhập số nhà" }
                            />
                        </FormControl>

                        <div className="flex-2">
                            <div className="flex-item">
                                <h5 className="postnews-form-heading">Hình ảnh nhà</h5>
                                <div className="flex-3 image-grids">
                                    {
                                        images ?
                                            images.map( ( item, index ) =>
                                            {
                                                return (
                                                    <div className="flex-item image-item">
                                                        <img src={ item } alt='image upload' />
                                                        <button className="image-delete" onClick={ () => { handleDeleteImage( index ); } } >X</button>
                                                    </div>
                                                );
                                            } )
                                            : null
                                    }
                                </div>
                            </div>
                            <div className="flex-item">
                                <h5 className="postnews-form-heading">Hình ảnh nhà</h5>
                                <FileUploader
                                    onCancel={ stopFakeProgress }
                                    multiple={ true }
                                    onDrop={ ( acceptedFiles, rejectedFiles ) =>
                                    {
                                        console.log( 'accepted files', acceptedFiles );
                                        acceptedFiles.map( ( item, index ) =>
                                        {
                                            console.log( 'item[', index, "]: ", item );
                                            const formData = new FormData();
                                            formData.append(
                                                "image",
                                                item,
                                                item.name
                                            );

                                            apiPostImage( token, formData, ( err, res ) =>
                                            {
                                                if ( err ) return;
                                                if ( res )
                                                {
                                                    setImages( arr => [ ...arr, res.data ] );
                                                }
                                            } );
                                        } );
                                        // handle file upload...
                                        startFakeProgress();
                                    } }

                                    // progressAmount is a number from 0 - 100 which indicates the percent of file transfer completed
                                    progressAmount={ progressAmount }
                                    progressMessage={
                                        progressAmount
                                            ? `Uploading... ${ progressAmount }% of 100%`
                                            : ''
                                    }
                                    onChange={ ( e ) =>
                                    {
                                        console.log( 'onchange event: ', e.target.event );
                                    } }
                                // getDataTransferItems={(e) => console.log(e)}
                                />
                            </div>
                        </div>
                        <div className="flex-2">
                            <div className="flex-item">
                                <h5 className="postnews-form-heading">Mô tả</h5>
                                <Textarea
                                    value={ description }
                                    onChange={ e => setDescription( e.currentTarget.value ) }
                                    placeholder="Nhập mô tả"
                                />
                            </div>
                            <div className="flex-item">
                                {/* <h5 className="postnews-form-heading">Nhu cầu khác</h5>
                                <Checkbox
                                    checked={ checkbox1 }
                                    onChange={ () => setCheckbox1( !checkbox1 ) }
                                >
                                    Thẩm định giá bất động sản
                                </Checkbox>
                                <Checkbox
                                    checked={ checkbox2 }
                                    onChange={ () => setCheckbox2( !checkbox2 ) }
                                >
                                    Cung cấp thông tin quy hoạch
                                </Checkbox>
                                <Checkbox
                                    checked={ checkbox3 }
                                    onChange={ () => setCheckbox3( !checkbox3 ) }
                                >
                                    Hoàn thiện hồ sơ pháp lý rao bán (Quyền sở hữu, tách thửa, kê khai thừa kế,...)
                                </Checkbox> */}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="postnews-post marginTop-l">
                    <div>
                        <Checkbox
                            checked={ checkbox4 }
                            onChange={ () => setCheckbox4( !checkbox4 ) }
                        >
                            Hoàn thiện hồ sơ pháp lý rao bán (Quyền sở hữu, tách thửa, kê khai thừa kế,...)
                        </Checkbox>
                        <Button
                            overrides={ {
                                BaseButton: {
                                    style: {
                                        outline: `#FFAC12`,
                                        backgroundColor: `#FFAC12`,
                                        whiteSpace: `nowrap`,
                                        padding: `16px`,
                                        marginTop: `10px`,
                                        width: `100%`,
                                        ':hover': {
                                            outline: `#ed9a00`,
                                            backgroundColor: `#ed9a00`
                                        }
                                    }
                                }
                            } }
                            onClick={ handlePostProduct }
                        >
                            Gửi thông tin
                        </Button>
                    </div>
                </div>
            </div>
            <Notification
                isOpen={ isOpenModal }
                onClose={ () => { setIsOpenModal( false ); } }
                styles={ stylesModal }
                content={ contentModal }
                header={ headingModal }
                onClickFeatureButton={ () => { setIsOpenModal( false ); } }
            />
        </Layout>
    );
};

export default PostPage;
