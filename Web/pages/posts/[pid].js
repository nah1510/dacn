import React, { Component, useState, useEffect } from 'react';
import { FormControl } from 'baseui/form-control';
import { Input } from 'baseui/input';
import { Button } from "baseui/button";
import { useFakeProgress, formatPrice, formatAddress } from "../../reusefunc/reuse";
import Layout from '../../layout/layout';
import Scrollbar from 'smooth-scrollbar';
import { DatePicker } from "baseui/datepicker";
import { Textarea } from 'baseui/textarea';
import Card, { HCard } from '../../components/card';
import SwiperCore, { Navigation, Pagination, Scrollbar as SpScrollbar, A11y, EffectCoverflow } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import Shared from '../../public/svgicon/upload.svg';
import Heart from '../../public/svgicon/heart.svg';
import Price from '../../public/svgicon/price.svg';
import Squarearea from '../../public/svgicon/squarearea.svg';
import CheckedIcon from '../../public/svgicon/checked.svg';
import { Avatar } from "baseui/avatar";
import Tag from '../../components/tag';
import { apiGetRE, apiSearchRealEstate, apiGetWard, apiGetDistrict, apiGetProvince, apiCheckLogin, apiUserCommentPostRE } from '../../API';
import Cookies from 'js-cookie';
import Notification from '../../components/modal';
import { ERROR_MODAL, SUCCESS_MODAL, WARNING_MODAL, PRIMARY_MODAL, BLUE_MODAL } from '../../reusefunc/reuse';
import { useRouter } from 'next/router';

const direction_Array = [ { value: 0, name: 'Bắc' }, { value: 1, name: 'T.Bắc' }, { value: 2, name: 'Đ.Bắc' }, { value: 3, name: 'Nam' },
{ value: 4, name: 'T.Nam' }, { value: 5, name: 'Đ.Nam' }, { value: 6, name: 'Bắc' }, { value: 0, name: 'Tây' } ];
function PostPage ( props )
{
    const router = useRouter();
    const { pid } = router.query;
    console.log( 'router query: ', pid );
    const carditem = [ 1, 2, 3, 4, 5, 6, 7, 8, 9 ];
    const token = Cookies.get( 'user' );


    const [ relatedPosts, setRelatedPosts ] = useState( [] );
    const [ comments, setComments ] = useState( [] );
    const [ title, setTitle ] = useState( '' );
    const [ price, setPrice ] = useState( 0 );
    const [ acreage, setAcreage ] = useState( 0 );
    const [ description, setDescription ] = useState( '' );
    const [ province, setProvince ] = useState( '' );
    const [ district, setDistrict ] = useState( '' );
    const [ ward, setWard ] = useState( '' );
    const [ street, setStreet ] = useState( '' );
    const [ address, setAddress ] = useState( '' );
    const [ mainImage, setMainImage ] = useState( '/images/default-image.jpg' );
    const [ arrayImage, setArrayImage ] = useState( [] );
    const [ avatarUrl, setAvatarUrl ] = useState( '' );

    // handle modal event
    const [ contentModal, setContentModal ] = useState( <p>đây là nội dung 2</p> );
    const [ headingModal, setHeadingModal ] = useState( "đây là tiêu đề" );
    const [ stylesModal, setStylesModal ] = useState( ERROR_MODAL );
    const [ isOpenModal, setIsOpenModal ] = useState( false );
    useEffect( () =>
    {
        apiCheckLogin( token, ( err, res ) =>
        {
            if ( err ) console.log( err );
            else
            {
                if ( res.success )
                {
                    console.log( 'res login pid:', res );
                    setAvatarUrl( res.data.avatar );
                }
            }
        } );
        apiGetRE( pid, ( err, res ) =>
        {
            if ( err ) return;
            else
            {
                console.log( 'pid: ', res );
                setProvince( res.province.name );
                setWard( res.ward.name );
                setDistrict( res.district.name );
                setComments( res.comments );

                setInfo( [ { name: "Phòng ngủ", value: res.bedroom }, { name: "Phòng tắm", value: res.bathroom }, { name: "Diện tích", value: res.acreage }, { name: "Diện tích đất", value: res.land_acreage }, { name: "Chiều dài", value: res.length }, { name: "Chiều rộng", value: res.width }, { name: "Năm xây dựng", value: res.year_built }, { name: "Hướng", value: direction_Array[ res.direction ].name } ] );
                apiSearchRealEstate( `limit=10&post_type=${ res.post_type }`, ( err, res ) =>
                {
                    if ( err ) return;
                    else
                    {
                        setRelatedPosts( res.data.filter( ( item ) =>
                        {
                            return item.id != pid;
                        } ) );
                    }
                } );
                setAcreage( res.acreage );
                setPrice( res.price );
                setTitle( res.tittle );
                setDescription( res.description );
                setMainImage( res.image );
                setArrayImage( res.images );
                setAddress( res.adress );
                setStreet( res.street );

            }



        } );
        Scrollbar.init( document.getElementById( 'my-scrollbar' ),
            {
                damping: 0.05,
                thumbMinSize: 20,
                renderByPixels: true,
                alwaysShowTracks: false,
                continuousScrolling: true
            } );
        return () =>
        {
            Scrollbar.destroyAll();
        };
    }, [ pid ] );
    const [ info, setInfo ] = useState( [] );
    const [ comment, setComment ] = useState( '' );
    const [ userNameValue, setUserNameValue ] = useState( '' );
    const [ nameValue, setNameValue ] = useState( null );
    const [ mailValue, setMailValue ] = useState( null );
    const [ phoneNumber, setPhoneNumber ] = useState( null );
    const [ date, setDate ] = useState( [ new Date() ] );
    SwiperCore.use( [ Navigation, Pagination, SpScrollbar, A11y, EffectCoverflow ] );
    const handleCommentPost = e =>
    {
        apiUserCommentPostRE( token, pid, { comment: comment }, ( err, res ) =>
        {
            if ( err ) return;
            else
            {
                if ( res.success )
                {
                    setHeadingModal( 'Thông báo' );
                    setStylesModal( SUCCESS_MODAL );
                    setContentModal(
                        <p className="success-text">
                            Bình luận thành công !
                        </p>
                    );
                    setIsOpenModal( true );

                    apiGetRE( pid, ( err, res ) =>
                    {
                        if ( err ) return;
                        else
                        {
                            setComments( res.comments );
                        }
                    } );
                } else
                {
                    setHeadingModal( 'Thông báo' );
                    setStylesModal( ERROR_MODAL );
                    setContentModal(
                        <p className="danger-text">
                            Bạn cần đăng nhập để bình luận
                        </p>
                    );
                    setIsOpenModal( true );
                }
            }
        } );
    };
    return (
        <Layout >
            <section className="detailed-header">
                <div className="detailed-header-info">
                    <h3>{ title }</h3>
                    <p>
                        { formatAddress( province, district, ward, street, address ) }
                    </p>
                </div>

            </section>
            <div className="box-container">
                <section className="detailed-wrapper">
                    <div className="detailed__wrapper">
                        <div className="detailed-left">
                            <div className="detailed-subblock">
                                <div className="detailed-left-heading">
                                    <h3>Đăng ký tư vấn</h3>
                                </div>
                                <div className="detailed-subcribe">
                                    <FormControl
                                        label="Email"
                                    >
                                        <Input
                                            onChange={ e => setMailValue( e.target.value ) }
                                            value={ mailValue }
                                            id={ props.id }
                                            placeholder={ "Nhập email của bạn" }
                                        />
                                    </FormControl>
                                    <FormControl
                                        label="Họ tên"
                                    >
                                        <Input
                                            onChange={ e => setNameValue( e.target.value ) }
                                            value={ nameValue }
                                            id={ props.id }
                                            placeholder={ "Nhập họ tên của bạn" }
                                        />
                                    </FormControl>
                                    <FormControl
                                        label="Số điện thoại"
                                    >
                                        <Input
                                            onChange={ e => setPhoneNumber( e.target.value ) }
                                            value={ phoneNumber }
                                            id={ props.id }
                                            placeholder={ "Nhập số điện thoại" }
                                            type="number"
                                        />
                                    </FormControl>
                                    <FormControl
                                        label="Lịch hẹn"
                                    >
                                        <DatePicker
                                            value={ date }
                                            onChange={ ( { date } ) =>
                                                setDate( Array.isArray( date ) ? date : [ date ] )
                                            }
                                        />
                                    </FormControl>
                                    <a className="detailed-button" href="/">
                                        Đặt lịch xem
                                    </a>
                                </div>
                            </div>


                        </div>
                        <div className="detailed-right">
                            <div className="detailed-images marginBottom-l">
                                <img src={ mainImage } onError={
                                    e =>
                                    {
                                        setMainImage( '/images/default-image.jpg' );
                                    }
                                } />
                            </div>
                            <div className="detailed-gallery marginBottom-l">
                                <Swiper
                                    navigation
                                    pagination={ { clickable: true } }
                                    scrollbar={ { draggable: true } }
                                    // onSwiper={ ( swiper ) => console.log( swiper ) }
                                    // onSlideChange={ () => console.log( 'slide change' ) }
                                    breakpoints={ {
                                        0: {
                                            slidesPerView: 4,
                                            spaceBetweenSlides: 20
                                        },
                                        767: {
                                            slidesPerView: 4,
                                        },
                                        991: {
                                            slidesPerView: 5,
                                            spaceBetweenSlides: 30
                                        },
                                        1200: {
                                            slidesPerView: 6,
                                            spaceBetween: 40
                                        }
                                    } }
                                >
                                    { arrayImage.map( ( e, index ) =>
                                    {
                                        return (
                                            <SwiperSlide>
                                                <div className="detailed-gallery-image">
                                                    <img src={ e.url } alt="image"
                                                        onError={
                                                            e =>
                                                            {
                                                                e.target.src = "/images/default-image.jpg";
                                                            }
                                                        } />
                                                </div>
                                            </SwiperSlide>
                                        );
                                    } ) }
                                </Swiper>
                            </div>
                            <div className="detailed-block marginBottom-l detailed-detail">
                                <div className="detailed-detail-col">
                                    <div><span className="icon"><Price /></span><p>{ formatPrice( price ) }</p></div>
                                    <div><span className="icon"><Squarearea /></span><p>{ acreage } m<sup>2</sup></p></div>
                                </div>
                                <div className="detailed-detail-col">
                                    <button><span className="icon"><Heart /></span><p>Lưu tin</p></button>
                                    <button><span className="icon"><Shared /></span><p>Chia sẻ</p></button>
                                </div>
                            </div>
                            <div className="detailed-block marginBottom-l">
                                <div className="detailed-block-heading">
                                    <h3>Thông tin</h3>
                                </div>
                                <div className="detailed-block-description flex-2 features">
                                    { info.map( ( e, index ) =>
                                    {
                                        console.log( e );
                                        return (
                                            <div className="flex-item detailed-checkbox">
                                                <CheckedIcon />
                                                <p>{ e.name } : { e.value }</p>
                                            </div>
                                        );
                                    } ) }
                                </div>
                            </div>
                            <div className="detailed-block marginBottom-l">
                                <div className="detailed-block-heading">
                                    <h3>Mô tả</h3>
                                </div>
                                <div className="detailed-block-description">
                                    <p>{ description }</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="detailed-block marginTop-l">
                        <div className="detailed-block-heading">
                            <h3>Bình luận   <Tag content={ comments ? comments.length : 0 } /></h3>
                        </div>
                        <div className="detailed-comment">
                            <div className="detailed-comment_card marginBottom-m">
                                <ul className="detailed-comment_list">
                                    { comments.map( ( e, index ) =>
                                    {
                                        return (
                                            <li>
                                                <HCard info={ e } />
                                            </li>
                                        );
                                    } ) }
                                </ul>
                            </div>
                            <div className="detailed-comment_wrap">
                                <div className="detailed-comment-content">
                                    <Avatar
                                        name={ userNameValue }
                                        size="45px"
                                        src={ avatarUrl }
                                    />
                                    <div className="comment-section">
                                        <Textarea
                                            value={ comment }
                                            onChange={ e => setComment( e.currentTarget.value ) }
                                            placeholder="nhập bình luận"
                                        />
                                    </div>
                                </div>
                                <div className="detailed-comment-button marginTop-m">
                                    <Button
                                        onClick={
                                            handleCommentPost
                                        }
                                    >
                                        Bình luận
                                </Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="marginTop-l">
                    <h3>
                        <span className="list-heading">
                            Danh sách liên quan
                        </span>
                    </h3>
                    <Swiper
                        navigation
                        pagination={ { clickable: true } }
                        scrollbar={ { draggable: true } }
                        // onSwiper={ ( swiper ) => console.log( swiper ) }
                        // onSlideChange={ () => console.log( 'slide change' ) }
                        breakpoints={ {
                            0: {
                                slidesPerView: 1,
                                spaceBetweenSlides: 20
                            },
                            767: {
                                slidesPerView: 2,
                            },
                            991: {
                                slidesPerView: 3,
                                spaceBetweenSlides: 30
                            },
                            1200: {
                                slidesPerView: 4,
                                spaceBetween: 40
                            }
                        } }
                    >
                        { relatedPosts.map( ( e, index ) =>
                        {
                            return (
                                <SwiperSlide>
                                    <Card info={ e } />
                                </SwiperSlide>
                            );
                        } ) }
                    </Swiper>
                </section>
                <Notification
                    isOpen={ isOpenModal }
                    onClose={ () => { setIsOpenModal( false ); } }
                    styles={ stylesModal }
                    content={ contentModal }
                    header={ headingModal }
                    onClickFeatureButton={ () => { setIsOpenModal( false ); } }
                />
            </div>
        </Layout >
    );
};

export default PostPage;
