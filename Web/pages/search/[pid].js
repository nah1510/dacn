import Head from 'next/head';
import Layout from '../../layout/layout';
import utilStyles from '../../styles/utils.module.css';
import Link from 'next/link';
import { Pagination } from "baseui/pagination";
import { motion } from "framer-motion";
import Scrollbar from 'smooth-scrollbar';
import { useEffect, useState } from 'react';
import Card from '../../components/card';
import SwiperCore, { Navigation, Pagination as SPagination, Scrollbar as SpScrollbar, A11y, Autoplay } from 'swiper';
import { Button } from 'baseui/button';
import { Swiper, SwiperSlide } from 'swiper/react';
import Filter from '../../components/filter';
import { useRouter } from 'next/router';
import { getParameterByName } from '../../reusefunc/reuse';
import { apiSearchRealEstate, formatAddressShort } from '../../API';

const dev_url = "http://gondarpham.myddns.rocks";
export default function Search ( { match } )
{
  const router = useRouter();
  const { pid } = router.query;
  console.log( 'match: ', match );
  console.log( 'pid', pid );
  const [ resultCards, setResultCars ] = useState( [] );
  const [ totalPage, setTotalPage ] = useState( 0 );
  const [ results, setResults ] = useState( 0 );

  SwiperCore.use( [ Navigation, SPagination, SpScrollbar, A11y, Autoplay ] );
  useEffect( () =>
  {
    apiSearchRealEstate( pid, ( err, res ) =>
    {
      if ( err ) return;
      else
      {
        console.log( 'result', res );
        setResults( res.total );
        setResultCars( res.data );
        setTotalPage( res.last_page );
        // setTotalPage()
      }
    } );
    Scrollbar.init( document.getElementById( 'my-scrollbar' ),
      {
        damping: 0.05,
        thumbMinSize: 20,
        renderByPixels: true,
        alwaysShowTracks: false,
        continuousScrolling: true
      } );
    return () =>
    {
      Scrollbar.destroyAll();
    };
  }, [ pid ] );
  const [ currentPage, setCurrentPage ] = React.useState( 1 );
  const backgroundSlider = [
    { src: '/images/background1.jpg', alt: 'background image' },
    { src: '/images/background2.jpeg', alt: 'background image' },
    { src: '/images/background3.jpg', alt: 'background image' },
    { src: '/images/background4.jpg', alt: 'background image' },
    { src: '/images/background5.jpg', alt: 'background image' },
  ];
  return (
    <Layout>
      <Head>
        <title>Trang chủ</title>
      </Head>
      <div className="home-slider">
        <div className="home-search">
          <Filter />
        </div>
        <div className="home-sliders">
          {
            <Swiper
              autoplay={ {
                delay: 5000,
                disableOnInteraction: true
              } }
              loop={ true }
              navigation
              pagination={ { clickable: true } }
              scrollbar={ { draggable: true } }
              speed={ 800 }
            >
              {
                backgroundSlider.map( ( e, index ) =>
                {
                  return (
                    <SwiperSlide>
                      <img src={ e.src } alt={ e.alt } className="home-image" />
                    </SwiperSlide>
                  );
                } ) }
            </Swiper>
          }
        </div>
      </div>
      <div className="box-container">
        <section className="home-content">
          <h3>
            <span className="list-heading">
              Có { results } kết quả tìm kiếm:
              </span>
          </h3>
          <div className="search-results marginTop-l">
            {
              resultCards ?
                resultCards.map( ( e, index ) =>
                {
                  return (
                    <div className="result-item">
                      <Card info={ e } />
                    </div>
                  );
                } )
                : null
            }
          </div>
          <div className="search-pagination marginTop-m">
            <Pagination
              numPages={ totalPage }
              currentPage={ currentPage }
              onPageChange={ ( { nextPage } ) =>
              {
                console.log( 'nextpage', nextPage );
                apiSearchRealEstate( `page=${ Math.min( Math.max( nextPage, 1 ), totalPage ) }`, ( err, res ) =>
                {
                  if ( err ) return;
                  else
                  {
                    setResultCars( res.data );
                    // setCurrentPage( res.current_page );
                    setCurrentPage(
                      Math.min( Math.max( nextPage, 1 ), totalPage )
                    );
                  }
                } );
              } }
            />
          </div>
        </section>
      </div>
    </Layout>
  );
}
