import "../styles/layout.scss";
import { AnimateSharedLayout } from "framer-motion";
import { motion, AnimatePresence } from "framer-motion";
import { Provider as StyletronProvider } from 'styletron-react';
import { styletron, debug } from '../styletron';
import Head from 'next/head';
import { useEffect } from 'react';
import { CookiesProvider } from "react-cookie";

export default function App ( { Component, pageProps, router } )
{
    const spring = {
        type: "spring",
        damping: 30,
        stiffness: 100,
        when: "afterChildren"
    };

    return (
        <>
            <StyletronProvider value={ styletron } debug={ debug } debugAfterHydration>
                <AnimatePresence >
                    <motion.div
                        transition={ spring }
                        key={ router.pathname }
                        initial={ { y: 400, opacity: 0 } }
                        animate={ { y: 0, opacity: 1 } }
                        exit={ { y: -400, opacity: 0 } }
                        id="page-transition-container"
                    >
                        <CookiesProvider>
                            <Component { ...pageProps } key={ router.route } />
                        </CookiesProvider>
                    </motion.div>
                </AnimatePresence>
            </StyletronProvider>
        </>
    );
}
