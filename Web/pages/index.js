import Head from 'next/head';
import Layout from '../layout/layout';
import Scrollbar from 'smooth-scrollbar';
import { useEffect, useState } from 'react';
import Card from '../components/card';
import SwiperCore, { Lazy, Navigation, Pagination, Scrollbar as SpScrollbar, A11y, Autoplay } from 'swiper';
import { Button } from 'baseui/button';
import { Swiper, SwiperSlide } from 'swiper/react';
import Filter from '../components/filter';
import Tag from '../components/tag';
import Homesearch from '../public/svgicon/homesearch.svg';
import Homeprice from '../public/svgicon/homeprice.svg';
import ArrowWhite from '../public/svgicon/ArrowWhite.svg';
import Cookies from 'js-cookie';
import { useRouter } from 'next/router';
import { apiGetRE, apiSearchRealEstate, apiGetUserListRE, apiGetUserDetailRE } from '../API';

export default function Home ()
{
  const token = Cookies.get( 'user' );
  SwiperCore.use( [ Lazy, Navigation, Pagination, SpScrollbar, A11y, Autoplay ] );
  const router = useRouter();

  // handle pages:
  const [ REList, setREList ] = useState( [] );
  const [ rentREList, setRentREList ] = useState( [] );
  const [ buyREList, setBuyREList ] = useState( [] );
  useEffect( () =>
  {
    apiSearchRealEstate( '', ( err, res ) =>
    {
      if ( err ) return;
      setREList( res.data.sort( function () { return 0.5 - Math.random(); } ) );
    } );
    apiSearchRealEstate( 'post_type=1&limit=12', ( err, res ) =>
    {
      if ( err ) return;
      setRentREList( res.data );
    } );
    apiSearchRealEstate( 'post_type=0&limit=12', ( err, res ) =>
    {
      if ( err ) return;
      setBuyREList( res.data );
    } );
    Scrollbar.init( document.getElementById( 'my-scrollbar' ),
      {
        damping: 0.05,
        thumbMinSize: 20,
        renderByPixels: true,
        alwaysShowTracks: false,
        continuousScrolling: true
      } );
    return () =>
    {
      Scrollbar.destroyAll();
    };
  }, [] );
  const carditem = [ 1, 2, 3, 4, 5, 6, 3, 8, 9, 0, 12, 32, 3 ];
  const backgroundSlider = [
    { src: '/images/background1.jpg', alt: 'background image' },
    { src: '/images/background2.jpeg', alt: 'background image' },
    { src: '/images/background3.jpg', alt: 'background image' },
    { src: '/images/background4.jpg', alt: 'background image' },
    { src: '/images/background5.jpg', alt: 'background image' },
  ];
  return (
    <Layout home>
      <Head>
        <title>Trang chủ</title>
      </Head>
      <div className="home-slider">
        <div className="home-sliders">
          {
            <Swiper
              autoplay={ {
                delay: 5000,
                disableOnInteraction: true
              } }
              loop={ true }
              navigation
              pagination={ { clickable: true } }
              scrollbar={ { draggable: true } }
              speed={ 800 }
            >
              {
                backgroundSlider.map( ( e, index ) =>
                {
                  return (
                    <SwiperSlide>
                      <img src={ e.src } alt={ e.alt } className="home-image" />
                    </SwiperSlide>
                  );
                } ) }
            </Swiper>
          }
        </div>
        <div className="home-search">
          <Filter />
        </div>
      </div>
      <div className="box-container">
        <section className="home-content">
          <h3>
            <span className="list-heading">
              Bất động sản nổi bật
              </span>
          </h3>
          {/* <Swiper
            navigation
            scrollbar={ { draggable: true, hide: true } }
            // onSwiper={ ( swiper ) => console.log( swiper ) }
            // onSlideChange={ () => console.log( 'slide change' ) }
            breakpoints={ {
              0: {
                slidesPerView: 3,
                spaceBetweenSlides: 10
              },
              576: {
                slidesPerView: 4,
                spaceBetweenSlides: 20
              },
              767: {
                slidesPerView: 5,
                spaceBetweenSlides: 20
              },
              991: {
                slidesPerView: 7,
                spaceBetweenSlides: 30
              },
              1200: {
                slidesPerView: 9,
                spaceBetween: 20
              }
            } }
          >
            { carditem.map( ( e, index ) =>
            {
              return (
                <SwiperSlide>
                  <Tag />
                </SwiperSlide>
              );
            } ) }
          </Swiper> */}
          <Swiper
            navigation
            pagination={ { clickable: true } }
            scrollbar={ { draggable: true, hide: true } }
            // onSwiper={ ( swiper ) => console.log( swiper ) }
            // onSlideChange={ () => console.log( 'slide change' ) }
            breakpoints={ {
              0: {
                slidesPerView: 1,
                spaceBetweenSlides: 20
              },
              767: {
                slidesPerView: 2,
              },
              991: {
                slidesPerView: 3,
                spaceBetweenSlides: 30
              },
              1200: {
                slidesPerView: 4,
                spaceBetween: 40
              }
            } }
          >
            { REList.map( ( e, index ) =>
            {
              // console.log( "card index: ", e );
              return (
                <SwiperSlide>
                  <Card info={ e } />
                </SwiperSlide>
              );
            } ) }
          </Swiper>
        </section>
        <section className="home-content marginTop-l">
          <h3>
            <span className="list-heading">
              Cho thuê bất động sản
              </span>
          </h3>
          {/* <Swiper
            navigation
            scrollbar={ { draggable: true, hide: true } }
            // onSwiper={ ( swiper ) => console.log( swiper ) }
            // onSlideChange={ () => console.log( 'slide change' ) }
            breakpoints={ {
              0: {
                slidesPerView: 3,
                spaceBetweenSlides: 10
              },
              576: {
                slidesPerView: 4,
                spaceBetweenSlides: 20
              },
              767: {
                slidesPerView: 5,
                spaceBetweenSlides: 20
              },
              991: {
                slidesPerView: 7,
                spaceBetweenSlides: 30
              },
              1200: {
                slidesPerView: 9,
                spaceBetween: 20
              }
            } }
          >
            { carditem.map( ( e, index ) =>
            {
              return (
                <SwiperSlide>
                  <Tag />
                </SwiperSlide>
              );
            } ) }
          </Swiper> */}
          <Swiper
            navigation
            pagination={ { clickable: true } }
            scrollbar={ { draggable: true, hide: true } }
            // onSwiper={ ( swiper ) => console.log( swiper ) }
            // onSlideChange={ () => console.log( 'slide change' ) }
            breakpoints={ {
              0: {
                slidesPerView: 1,
                spaceBetweenSlides: 20
              },
              767: {
                slidesPerView: 2,
              },
              991: {
                slidesPerView: 3,
                spaceBetweenSlides: 30
              },
              1200: {
                slidesPerView: 4,
                spaceBetween: 40
              }
            } }
          >
            { rentREList.map( ( e, index ) =>
            {
              return (
                <SwiperSlide>
                  <Card info={ e } />
                </SwiperSlide>
              );
            } ) }
          </Swiper>
        </section>
        <section className="home-content marginTop-l">
          <h3>
            <span className="list-heading">
              Mua bán bất động sản
              </span>
          </h3>
          {/* <Swiper
            navigation
            scrollbar={ { draggable: true, hide: true } }
            // onSwiper={ ( swiper ) => console.log( swiper ) }
            // onSlideChange={ () => console.log( 'slide change' ) }
            breakpoints={ {
              0: {
                slidesPerView: 3,
                spaceBetweenSlides: 10
              },
              576: {
                slidesPerView: 4,
                spaceBetweenSlides: 20
              },
              767: {
                slidesPerView: 5,
                spaceBetweenSlides: 20
              },
              991: {
                slidesPerView: 7,
                spaceBetweenSlides: 30
              },
              1200: {
                slidesPerView: 9,
                spaceBetween: 20
              }
            } }
          >
            { carditem.map( ( e, index ) =>
            {
              return (
                <SwiperSlide>
                  <Tag />
                </SwiperSlide>
              );
            } ) }
          </Swiper> */}
          <Swiper
            navigation
            pagination={ { clickable: true } }
            scrollbar={ { draggable: true, hide: true } }
            // onSwiper={ ( swiper ) => console.log( swiper ) }
            // onSlideChange={ () => console.log( 'slide change' ) }
            breakpoints={ {
              0: {
                slidesPerView: 1,
                spaceBetweenSlides: 20
              },
              767: {
                slidesPerView: 2,
              },
              991: {
                slidesPerView: 3,
                spaceBetweenSlides: 30
              },
              1200: {
                slidesPerView: 4,
                spaceBetween: 40
              }
            } }
          >
            { buyREList.map( ( e, index ) =>
            {
              return (
                <SwiperSlide>
                  <Card info={ e } />
                </SwiperSlide>
              );
            } ) }
          </Swiper>
        </section>
      </div>
      <div className="block-content">

      </div>
      <div className="block-parallax">
        <div className="block-parallax_wrapper">
          <h3 className="block-parallax-heading">Dịch vụ nổi bật của batdongsanhd</h3>
          <div className="block-parallax-content">
            <div className="col">
              <div className="image">
                <Homesearch />
              </div>
              <h4>Đăng Bán và Cho Thuê</h4>
              <p>Tiếp cận tập khách hàng sẵn có, quảng cáo tin đăng</p>
              <Button
                overrides={ {
                  BaseButton: {
                    style: {
                      outline: `#FFAC12`,
                      backgroundColor: `#FFAC12`,
                      borderRadius: `0 20px 0 0`,
                      whiteSpace: `nowrap`,
                      padding: `16px`,
                      ':hover': {
                        outline: `#ed9a00`,
                        backgroundColor: `#ed9a00`
                      }
                    }
                  }
                } }
                onClick={ () =>
                {
                  router.push( '/postpage' );
                } }
              >
                Đăng tin <span className="marginLeft-xxs"><ArrowWhite /></span>
              </Button>
            </div>
            <div className="col">
              <div className="image">
                <Homeprice />
              </div>
              <h4>Tìm Mua và Thuê Như Ý</h4>
              <p>Tìm kiếm theo tiêu chí, đảm bảo an toàn pháp lý</p>
              <Button
                overrides={ {
                  BaseButton: {
                    style: {
                      outline: `#08AD36`,
                      backgroundColor: `#08AD36`,
                      borderRadius: `0 20px 0 0`,
                      whiteSpace: `nowrap`,
                      padding: `16px`,
                      ':hover': {
                        outline: `#07922e`,
                        backgroundColor: `#07922e`
                      }
                    }
                  }
                } }
                onClick={ () =>
                {
                  router.push( '/postpage' );
                } }
              >
                Liên hệ<span className="marginLeft-xxs"><ArrowWhite /></span>
              </Button>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

