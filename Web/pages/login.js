import { useEffect } from 'react';
import Login from "../components/login";
import Cookies from 'js-cookie';
import { apiCheckLogin } from '../API';
import { useRouter } from 'next/router';
import Head from 'next/head';
const LoginPage = ( props ) =>
{
  const token = Cookies.get( 'user' );
  const router = useRouter();
  useEffect( () =>
  {
    apiCheckLogin( token, ( err, res ) =>
    {
      if ( err ) console.log( err );
      else
      {
        if ( res.success )
        {
          router.push( '/' );
        }
      }
    } );
  }, [] );
  return (
    <>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <meta
          name="description"
          content="Learn how to build a personal website using Next.js"
        />
        <meta
          property="og:image"
          content={ `https://og-image.now.sh/${ encodeURI(
            'Đăng nhập'
          ) }.png?theme=light&md=0&fontSize=75px&images=https%3A%2F%2Fassets.vercel.com%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg` }
        />
        <meta name="og:title" content={ 'Đăng nhập' } />
        <meta name="twitter:card" content="summary_large_image" />
        <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests" />
      </Head>
      <div className="section">
        <Login />
      </div>
    </>
  );
};

export default LoginPage;
