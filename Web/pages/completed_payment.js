import React, { Component, useState, useEffect } from 'react';
import Link from 'next/link';
import Checked from '../public/svgicon/checked.svg';
import { useRouter } from 'next/router';
import { apiCreateTransaction, apiPutTransaction } from '../API';
import Cookies from 'js-cookie';
import { getParameterByName } from "../reusefunc/reuse";
import Head from 'next/head';

function CompletedPayment ( props )
{
    const router = useRouter();
    useEffect( () =>
    {
        const { asPath } = router;
        const tokenPaypal = getParameterByName( 'token', asPath );

        const formData = new FormData();
        formData.append( 'payment_id', tokenPaypal.toString() );
        apiPutTransaction( { payment_id: tokenPaypal.toString() }, ( err, res ) =>
        {
            if ( err ) return;
            if ( res )
            {
                setTimeout( () =>
                {
                    router.push( '/user/payment' );
                }, 3000 );
            }
        } );
    }, [] );
    return (
        <>
            <Head>
                <link rel="icon" href="/favicon.ico" />
                <meta
                    name="description"
                    content="Learn how to build a personal website using Next.js"
                />
                <meta
                    property="og:image"
                    content={ `https://og-image.now.sh/${ encodeURI(
                        'Thanh toán thành công'
                    ) }.png?theme=light&md=0&fontSize=75px&images=https%3A%2F%2Fassets.vercel.com%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg` }
                />
                <meta name="og:title" content={ 'Thanh toán thành công' } />
                <meta name="twitter:card" content="summary_large_image" />
                <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests" />
            </Head>
            <section className="congrate">
                <Link href="/">
                    <a className="login-logo">
                        <img src="/images/Logo.png" />
                    </a>
                </Link>
                <div className="congrate-box">
                    <h3 className="congrate-heading">
                        Chúc mừng bạn đã giao dịch thành công
                </h3>
                    <p className="congrate-redirect">
                        Bạn sẽ chuyển hướng về trang thanh toan
                </p>
                </div>

            </section>
        </>
    );
};

export default CompletedPayment;
