import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import { Pagination } from "baseui/pagination";
import Card from '../../components/card';
import { FileUploader } from 'baseui/file-uploader';
import { apiPostImage, apiCheckLogin, apiLogout, updateOwnUser } from '../../API';
import { Button } from "baseui/button";
import { useFakeProgress } from "../../reusefunc/reuse";
import Layout from '../../layout/layout';
import Scrollbar from 'smooth-scrollbar';
import SwiperCore, { Navigation, Pagination as SPagination, Scrollbar as SpScrollbar, A11y, EffectCoverflow } from 'swiper';
import Cookies from 'js-cookie';
import { FormControl } from 'baseui/form-control';
import { Select } from 'baseui/select';
import { Avatar } from "baseui/avatar";
import { formatDate } from '../../reusefunc/reuse';
import { ERROR_MODAL, SUCCESS_MODAL, WARNING_MODAL, PRIMARY_MODAL, BLUE_MODAL } from '../../reusefunc/reuse';
import Notification from '../../components/modal';
import { useRouter } from 'next/router';
import Camera from '../../public/svgicon/camera.svg';
const profilelinks = [ { title: 'Thông tin cá nhân', href: '/user/profile' }, { title: 'Bất động sản của tôi', href: '/user/myproducts', status: 'active' },
{ title: 'Đổi mật khẩu', href: '/user/changepassword' }, { title: 'Thanh toán', href: '/user/payment' } ];
import { apiGetUserListRE } from '../../API';

const orderBy_Array = [ { value: 'created_at', name: 'Ngày đăng' }, { value: 'price', name: 'Giá' } ];
const orderType_Array = [ { value: 'asc', name: 'Tăng dần' }, { value: 'desc', name: 'Giảm dần' } ];
function PostPage ( props )
{
    const token = Cookies.get( 'user' );
    const [ avatar_url, setAvatar ] = useState( '' );
    const carditem = [ 1, 2, 3, 4, 5, 6, 3, 8, 9, 0, 12, 32 ];
    const [
        progressAmount,
        startFakeProgress,
        stopFakeProgress,
    ] = useFakeProgress();
    const [ fileImage, setFileImage ] = useState( null );
    const [ fileDigitalDraw, setFileDigitalDraw ] = useState( null );

    const router = useRouter();

    // handle modal event
    const [ contentModal, setContentModal ] = useState( <p>đây là nội dung 2</p> );
    const [ headingModal, setHeadingModal ] = useState( "đây là tiêu đề" );
    const [ stylesModal, setStylesModal ] = useState( ERROR_MODAL );
    const [ isOpenModal, setIsOpenModal ] = useState( false );

    const [ myProducts, setMyProducts ] = useState( [] );
    const [ totalPage, setTotalPage ] = useState( 0 );
    const [ orderValue, setOrderValue ] = useState( [ { value: 'price', name: 'Giá' } ] );
    const [ orderTypeValue, setOrderTypeValue ] = useState( [ { value: 'asc', name: 'Tăng dần' } ] );
    const [ currentPage, setCurrentPage ] = React.useState( 1 );
    const [ nameValue, setNameValue ] = useState( null );
    const [ mailValue, setMailValue ] = useState( null );
    const [ phoneNumber, setPhoneNumber ] = useState( null );
    const [ date, setDate ] = useState( [ new Date() ] );
    const [ accountMoney, setAccountMoney ] = useState( 0 );
    const [ createdAt, setCreateAt ] = useState( '' );
    // handle avatar
    const handleUpdateAvatar = ( e ) =>
    {
        e.preventDefault();
        const object = {};
        const avatar = localStorage.getItem( 'avatar_url' );
        object[ 'avatar' ] = avatar;
        updateOwnUser( token, object, ( err, res ) =>
        {
            if ( err ) return;
            if ( res )
            {
                localStorage.clear();
                setHeadingModal( 'Thông báo' );
                setStylesModal( SUCCESS_MODAL );
                setContentModal( <p>
                    Cập nhật ảnh đại diện thành công !
                </p> );
                setIsOpenModal( true );
            }
        } );
    };
    const handleUploadCamera = ( e ) =>
    {
        setContentModal(
            <>
                <FileUploader
                    over
                    onCancel={ stopFakeProgress }
                    onDrop={ ( acceptedFiles, rejectedFiles ) =>
                    {
                        const formData = new FormData();
                        formData.append(
                            "image",
                            acceptedFiles[ 0 ],
                            acceptedFiles[ 0 ].name
                        );
                        // handle file upload...
                        startFakeProgress();

                        setFileImage( acceptedFiles );
                        apiPostImage( token, formData, ( err, res ) =>
                        {
                            if ( err ) return console.log( err );
                            if ( res )
                            {
                                setAvatar( res.data );
                                localStorage.setItem( 'avatar_url', res.data );
                            }

                        } );
                    } }
                    progressAmount={ progressAmount }
                    progressMessage={
                        progressAmount
                            ? `Đang tải hình ảnh sản phẩm... ${ progressAmount }% of 100%`
                            : ''
                    }
                />
                <Button
                    overrides={ {
                        BaseButton: {
                            style: {
                                outline: `#1565D8`,
                                backgroundColor: `#1565D8`,
                                width: '100%',
                                ':hover': {
                                    outline: `#1053B5`,
                                    backgroundColor: `#1053B5`
                                }
                            }
                        }
                    } }
                    onClick={ handleUpdateAvatar }
                >
                    Đăng ảnh
                </Button>
            </>
        );
        setHeadingModal( 'Tải ảnh đại diện cá nhân' );
        setStylesModal( BLUE_MODAL );
        setIsOpenModal( true );
    };
    useEffect( () =>
    {
        apiCheckLogin( token, ( err, res ) =>
        {
            if ( err ) console.log( err );
            else
            {
                if ( res.success )
                {
                    setAvatar( res.data.avatar );
                    setNameValue( res.data.name );
                    setCreateAt( formatDate( res.data.created_at ) );
                    setAccountMoney( res.data.ewallet.account_balance );
                } else
                {
                    router.push( '/' );
                }
            }
        } );
        apiGetUserListRE( token, 'limit=6', ( err, res ) =>
        {
            if ( err ) return;
            setMyProducts( res.data );
            setTotalPage( res.last_page );
        } );

        Scrollbar.init( document.getElementById( 'my-scrollbar' ),
            {
                damping: 0.05,
                thumbMinSize: 20,
                renderByPixels: true,
                alwaysShowTracks: false,
                continuousScrolling: true
            } );
        return () =>
        {
            Scrollbar.destroyAll();
        };
    }, [] );

    const handleOnClick = ( e ) =>
    {
        e.preventDefault();
        apiLogout( token, ( err, res ) =>
        {
            if ( err ) console.log( err );
            else
            {
                console.log( res );
                setTimeout( () =>
                {
                    router.push( '/login' );
                }, 2000 );
            }
        } );

    };

    SwiperCore.use( [ Navigation, SPagination, SpScrollbar, A11y, EffectCoverflow ] );
    return (
        <Layout >
            <div className="box-container">
                <section className="profile-wrapper">
                    <div className="profile__wrapper marginTop-l">
                        <div className="profile-left">
                            <div className="profile-left-info">
                                <div className="avatar">
                                    <button className="profile-avatar" onClick={ handleUploadCamera }>
                                        <Camera />
                                    </button>
                                    <Avatar
                                        name={ nameValue ? nameValue : 'No name' }
                                        size="90px"
                                        src={ avatar_url }
                                        id="avatar"
                                    />
                                </div>
                                <p className="name">{ nameValue }</p>
                                <p className="joindate">Thành viên từ { createdAt }</p>
                                <p className="money">Số dư trong tài khoản: { accountMoney }đ</p>
                            </div>
                            <ul className="profile-links">
                                {
                                    profilelinks.map( ( e, index ) =>
                                    {
                                        return (
                                            <li className={ e.status == `active` ? `item active` : `item` } key={ "linkinfo-" + index }>
                                                <Link href={ e.href }>
                                                    <a>
                                                        <span>
                                                            { e.title }
                                                        </span>
                                                    </a>
                                                </Link>
                                            </li>
                                        );
                                    } )
                                }
                                <li className={ `item` }>
                                    <a>
                                        <button onClick={ handleOnClick }>
                                            Đăng xuất
                                    </button>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div className="profile-right">
                            <div className="profile-block">
                                <div className="profile-block-heading f-space-between">
                                    <h3>Bất động sản của tôi</h3>
                                    <div style={ { display: 'flex', alignItems: 'center' } }>
                                        <div style={ { width: '150px' } }>
                                            <Select
                                                id="select-id"
                                                value={ orderValue }
                                                onChange={ ( { value } ) =>
                                                {
                                                    apiGetUserListRE( token, `limit=6&page=${ currentPage }&order_type=${ orderTypeValue[ 0 ].value }&order_by=` + value[ 0 ].value, ( err, res ) =>
                                                    {
                                                        if ( err ) return;
                                                        setMyProducts( res.data );
                                                        setTotalPage( res.last_page );
                                                    } );
                                                    setOrderValue( value );
                                                }
                                                }
                                                options={
                                                    orderBy_Array
                                                }
                                                labelKey="name"
                                                valueKey="value"
                                                placeholder="Sắp xếp theo"
                                            />
                                        </div>
                                        <div style={ { width: '150px' } }>
                                            <Select
                                                id="select-id"
                                                value={ orderTypeValue }
                                                onChange={ ( { value } ) =>
                                                {
                                                    apiGetUserListRE( token, `limit=6&page=${ currentPage }&order_by=${ orderValue[ 0 ].value }&order_type=` + value[ 0 ].value, ( err, res ) =>
                                                    {
                                                        if ( err ) return;
                                                        setMyProducts( res.data );
                                                        setTotalPage( res.last_page );
                                                    } );
                                                    setOrderTypeValue( value );
                                                } }
                                                options={
                                                    orderType_Array
                                                }
                                                labelKey="name"
                                                valueKey="value"
                                                placeholder="Sắp xếp theo"
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="profile-block-description">
                                    <div className="flex-3">
                                        { myProducts.map( ( e, index ) =>
                                        {
                                            return (
                                                <div className="flex-item">
                                                    <Card key={ "favorite card -" + index } info={ e } />
                                                </div>
                                            );
                                        } ) }
                                    </div>
                                    <div className="pagination marginTop-m">
                                        <Pagination
                                            numPages={ totalPage }
                                            currentPage={ currentPage }
                                            onPageChange={ ( { nextPage } ) =>
                                            {
                                                apiGetUserListRE( token, `order_by=${ orderValue[ 0 ].value }&order_type=${ orderTypeValue[ 0 ].value }&&limit=6&page=` + Math.min( Math.max( nextPage, 1 ), totalPage ), ( err, res ) =>
                                                {
                                                    if ( err ) return;
                                                    setMyProducts( res.data );
                                                    setCurrentPage(
                                                        Math.min( Math.max( nextPage, 1 ), totalPage )
                                                    );
                                                } );
                                            } }
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <Notification
                isOpen={ isOpenModal }
                onClose={ () => { setIsOpenModal( false ); } }
                styles={ stylesModal }
                content={ contentModal }
                header={ headingModal }
                onClickFeatureButton={ () => { setIsOpenModal( false ); } }
            />
        </Layout>
    );
};

export default PostPage;
