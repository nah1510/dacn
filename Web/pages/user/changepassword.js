import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import { FileUploader } from 'baseui/file-uploader';
import { FormControl } from 'baseui/form-control';
import { ERROR_MODAL, SUCCESS_MODAL, WARNING_MODAL, PRIMARY_MODAL, BLUE_MODAL } from '../../reusefunc/reuse';
import { Input } from 'baseui/input';
import { Button } from "baseui/button";
import Layout from '../../layout/layout';
import Scrollbar from 'smooth-scrollbar';
import { Avatar } from "baseui/avatar";
import { useFakeProgress } from "../../reusefunc/reuse";
import Cookies from 'js-cookie';
import { formatDate } from '../../reusefunc/reuse';
import { useRouter } from 'next/router';
import { apiChangePassword, apiCheckLogin, apiLogout, apiPostImage, updateOwnUser } from '../../API';
import Notification from '../../components/modal';
import Camera from '../../public/svgicon/camera.svg';

const profilelinks = [ { title: 'Thông tin cá nhân', href: '/user/profile' }, { title: 'Bất động sản của tôi', href: '/user/myproducts' },
{ title: 'Đổi mật khẩu', href: '/user/changepassword', status: 'active' }, { title: 'Thanh toán', href: '/user/payment' } ];

function PostPage ( props )
{
    const [ avatar_url, setAvatar ] = useState( '' );
    const [ passwordValue, setPasswordValue ] = useState( null );
    const [ passwordOldValue, setOldPasswordValue ] = useState( null );
    const [ passwordConfirm, setPasswordConfirm ] = useState( null );
    const [ nameValue, setNameValue ] = useState( null );
    const [ createdAt, setCreateAt ] = useState( '' );
    const [ accountMoney, setAccountMoney ] = useState( 0 );
    const [ fileImage, setFileImage ] = useState( null );
    const token = Cookies.get( 'user' );
    const router = useRouter();
    const carditem = [ 1, 2, 3, 4, 5, 6, 3, 8, 9, 0, 12, 32, 3 ];
    const [
        progressAmount,
        startFakeProgress,
        stopFakeProgress,
    ] = useFakeProgress();


    // handle modal event
    const [ contentModal, setContentModal ] = useState( <p>đây là nội dung 2</p> );
    const [ headingModal, setHeadingModal ] = useState( "đây là tiêu đề" );
    const [ stylesModal, setStylesModal ] = useState( ERROR_MODAL );
    const [ isOpenModal, setIsOpenModal ] = useState( false );
    useEffect( () =>
    {
        apiCheckLogin( token, ( err, res ) =>
        {
            if ( err ) console.log( err );
            else
            {
                if ( res.success )
                {
                    setAvatar( res.data.avatar );
                    setNameValue( res.data.name );
                    setCreateAt( formatDate( res.data.created_at ) );
                    setAccountMoney( res.data.ewallet.account_balance );
                } else
                {
                    router.push( '/' );
                }

            }
        } );
        Scrollbar.init( document.getElementById( 'my-scrollbar' ),
            {
                damping: 0.05,
                thumbMinSize: 20,
                renderByPixels: true,
                alwaysShowTracks: false,
                continuousScrolling: true
            } );
        return () =>
        {
            Scrollbar.destroyAll();
        };
    }, [] );


    const handleOnClick = ( e ) =>
    {
        e.preventDefault();
        apiLogout( token, ( err, res ) =>
        {
            if ( err ) console.log( err );
            else
            {
                console.log( res );
                setTimeout( () =>
                {
                    router.push( '/' );
                }, 2000 );
            }
        } );

    };
    const handleChangePassword = ( e ) =>
    {
        e.preventDefault();
        const formData = new FormData();
        formData.append( "new_password", passwordValue );
        formData.append( "recheck_password", passwordConfirm );
        apiChangePassword( token, formData, ( err, res ) =>
        {
            if ( err ) console.log( err );
            else
            {
                if(res.success) {
                    setHeadingModal( 'Thông báo' );
                    setStylesModal( SUCCESS_MODAL );
                    setContentModal( <p>
                        Cập nhật mật khẩu thành công !
                    </p> );
                    setIsOpenModal( true );
                }
            }
        } );

    };
    // handle avatar
    const handleUpdateAvatar = ( e ) =>
    {
        e.preventDefault();
        const object = {};
        const avatar = localStorage.getItem( 'avatar_url' );
        object[ 'avatar' ] = avatar;
        updateOwnUser( token, object, ( err, res ) =>
        {
            if ( err ) return;
            if ( res )
            {
                localStorage.clear();
                setHeadingModal( 'Thông báo' );
                setStylesModal( SUCCESS_MODAL );
                setContentModal( <p>
                    Cập nhật ảnh đại diện thành công !
                </p> );
                setIsOpenModal( true );
            }
        } );
    };
    const handleUploadCamera = ( e ) =>
    {
        setContentModal(
            <>
                <FileUploader
                    over
                    onCancel={ stopFakeProgress }
                    onDrop={ ( acceptedFiles, rejectedFiles ) =>
                    {
                        const formData = new FormData();
                        formData.append(
                            "image",
                            acceptedFiles[ 0 ],
                            acceptedFiles[ 0 ].name
                        );
                        // handle file upload...
                        startFakeProgress();

                        setFileImage( acceptedFiles );
                        apiPostImage( token, formData, ( err, res ) =>
                        {
                            if ( err ) return console.log( err );
                            if ( res )
                            {
                                console.log( 'res data', res.data );
                                setAvatar( res.data );
                                localStorage.setItem( 'avatar_url', res.data );
                            }

                        } );
                    } }
                    progressAmount={ progressAmount }
                    progressMessage={
                        progressAmount
                            ? `Đang tải hình ảnh sản phẩm... ${ progressAmount }% of 100%`
                            : ''
                    }
                    onChange={ ( e ) =>
                    {
                        console.log( 'onchange event: ', e.target.event );
                    } }
                />
                <Button
                    overrides={ {
                        BaseButton: {
                            style: {
                                outline: `#1565D8`,
                                backgroundColor: `#1565D8`,
                                width: '100%',
                                ':hover': {
                                    outline: `#1053B5`,
                                    backgroundColor: `#1053B5`
                                }
                            }
                        }
                    } }
                    onClick={ handleUpdateAvatar }
                >
                    Đăng ảnh
                 </Button>
            </>
        );
        setHeadingModal( 'Tải ảnh đại diện cá nhân' );
        setStylesModal( BLUE_MODAL );
        setIsOpenModal( true );
    };
    return (
        <Layout >
            <div className="box-container">
                <section className="profile-wrapper">
                    <div className="profile__wrapper marginTop-l">
                        <div className="profile-left">
                            <div className="profile-left-info">
                                <div className="avatar">
                                    <button className="profile-avatar" onClick={ handleUploadCamera }>
                                        <Camera />
                                    </button>
                                    <Avatar
                                        name={ nameValue ? nameValue : 'No name' }
                                        size="90px"
                                        src={ avatar_url }
                                        id="avatar"
                                    />
                                </div>
                                <p className="name">{ nameValue }</p>
                                <p className="joindate">Thành viên từ { createdAt }</p>
                                <p className="money">Số dư trong tài khoản: { accountMoney }đ</p>
                            </div>
                            <ul className="profile-links">
                                {
                                    profilelinks.map( ( e, index ) =>
                                    {
                                        return (
                                            <li className={ e.status == `active` ? `item active` : `item` } key={ "linkinfo-" + index }>
                                                <Link href={ e.href }>
                                                    <a>
                                                        <span>
                                                            { e.title }
                                                        </span>
                                                    </a>
                                                </Link>
                                            </li>
                                        );
                                    } )
                                }
                                <li className={ `item` }>
                                    <a>
                                        <button onClick={ handleOnClick }>
                                            Đăng xuất
                                    </button>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div className="profile-right">
                            <div className="profile-block">
                                <div className="profile-block-heading">
                                    <h3>Thông tin cá nhân</h3>
                                </div>
                                <div className="profile-block-description flex-3">
                                    <FormControl
                                        label="Mật khẩu mới"
                                    >
                                        <Input
                                            onChange={ e => setPasswordValue( e.target.value ) }
                                            value={ passwordValue }
                                            id={ props.id }
                                            placeholder={ "Nhập mật khẩu mới" }
                                            type="password"
                                        />
                                    </FormControl>
                                    <FormControl
                                        label="xác nhận mật khẩu"
                                    >
                                        <Input
                                            onChange={ e => setPasswordConfirm( e.target.value ) }
                                            value={ passwordConfirm }
                                            id={ props.id }
                                            placeholder={ "Nhập mật khẩu xác nhận" }
                                            type="password"
                                        />
                                    </FormControl>
                                    <Button
                                        overrides={ {
                                            BaseButton: {
                                                style: {
                                                    outline: `#FFAC12`,
                                                    backgroundColor: `#FFAC12`,
                                                    marginTop: `16px`,
                                                    width: '100%',
                                                    ':hover': {
                                                        outline: `#e69500`,
                                                        backgroundColor: ` #e69500`
                                                    }
                                                }
                                            }
                                        } }
                                        onClick={ handleChangePassword }
                                    >
                                        Lưu
                                </Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <Notification
                isOpen={ isOpenModal }
                onClose={ () => { setIsOpenModal( false ); } }
                styles={ stylesModal }
                content={ contentModal }
                header={ headingModal }
                onClickFeatureButton={ () => { setIsOpenModal( false ); } }
            />
        </Layout>
    );
};

export default PostPage;
