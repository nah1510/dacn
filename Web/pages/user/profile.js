import React, { Component, useState, useEffect } from 'react';
import Link from 'next/link';
import { FileUploader } from 'baseui/file-uploader';
import { FormControl } from 'baseui/form-control';
import { Input } from 'baseui/input';
import { Button } from "baseui/button";
import { Select } from 'baseui/select';
import { useFakeProgress } from "../../reusefunc/reuse";
import Layout from '../../layout/layout';
import Scrollbar from 'smooth-scrollbar';
import { DatePicker } from "baseui/datepicker";
import { apiPostImage, updateOwnUser, apiCheckLogin, apiLogout } from '../../API';
import Card from '../../components/card';
import SwiperCore, { Navigation, Pagination, Scrollbar as SpScrollbar, A11y, EffectCoverflow } from 'swiper';
import Camera from '../../public/svgicon/camera.svg';
import { Avatar } from "baseui/avatar";
import Cookies from 'js-cookie';
import { useRouter } from 'next/router';
import { formatDate, formatDate1 } from '../../reusefunc/reuse';
import Notification from '../../components/modal';
import { ERROR_MODAL, SUCCESS_MODAL, WARNING_MODAL, PRIMARY_MODAL, BLUE_MODAL } from '../../reusefunc/reuse';

const profilelinks = [ { title: 'Thông tin cá nhân', href: '/user/profile', status: 'active' }, { title: 'Bất động sản của tôi', href: '/user/myproducts' },
{ title: 'Đổi mật khẩu', href: '/user/changepassword' }, { title: 'Thanh toán', href: '/user/payment' } ];



const GENDER = [
    { id: 'Nam', sexual: '0' },
    { id: 'Nữ', sexual: '1' },
    { id: 'Khác', sexual: '2' },
];
function PostPage ( props )
{
    const [ avatar_url, setAvatar ] = useState( '' );
    const [ nameValue, setNameValue ] = useState( null );
    const [ nameEdit, setNameEditValue ] = useState( null );
    const [ mailValue, setMailValue ] = useState( null );
    const [ gender, setGender ] = useState( [ { id: 'Khác', sexual: '2' }] );
    const [ phoneNumber, setPhoneNumber ] = useState( null );
    const [ birthday, setBirthDay ] = useState( [ new Date() ] );
    const [ accountMoney, setAccountMoney ] = useState( 0 );
    const [ createdAt, setCreateAt ] = useState( '' );
    const token = Cookies.get( 'user' );
    const carditem = [ 1, 2, 3, 4, 5, 6, 3, 8, 9, 0, 12, 32, 3 ];
    const [
        progressAmount,
        startFakeProgress,
        stopFakeProgress,
    ] = useFakeProgress();
    const [ fileImage, setFileImage ] = useState( null );

    const handleEditUser = ( e ) =>
    {
        e.preventDefault();
        const object = {};
        if ( nameValue ) object[ 'name' ] = nameEdit;
        if ( phoneNumber ) object[ 'phone_number' ] = phoneNumber;
        // if ( gender[0] ) object[ 'gender' ] = gender[ 0 ].sexual;
        if ( birthday ) object[ 'birthday' ] = formatDate1( birthday[ 0 ] );
        updateOwnUser( token, object, ( err, res ) =>
        {
            if ( err ) return;
            if ( res )
            {
                apiCheckLogin( token, ( err, res ) =>
                {
                    if ( err )
                    {
                        setHeadingModal( 'Thông báo' );
                        setStylesModal( SUCCESS_ERROR );
                        setContentModal( <p>
                            Cập nhật thông tin thất bại, vui lòng kiẻm tra lại đường truyền.
                        </p> );
                        setIsOpenModal( true );
                    }
                    else
                    {
                        setMailValue( res.data.email );
                        setNameValue( res.data.name );
                        setCreateAt( formatDate( res.data.created_at ) );
                        setBirthDay( [ new Date( res.data.birthday ) ] );
                        setNameEditValue( res.data.name );
                        setPhoneNumber( res.data.phone_number );
                        setHeadingModal( 'Thông báo' );
                        setStylesModal( SUCCESS_MODAL );
                        setContentModal( <p>
                            Cập nhật thông tin thành công !
                        </p> );
                        setIsOpenModal( true );
                        setAccountMoney( res.data.ewallet.account_balance );
                    }

                } );
            }
        } );
    };
    const handleUpdateAvatar = ( e ) =>
    {
        e.preventDefault();
        const object = {};
        const avatar = localStorage.getItem( 'avatar_url' );
        object[ 'avatar' ] = avatar;
        updateOwnUser( token, object, ( err, res ) =>
        {
            if ( err ) return;
            if ( res )
            {
                localStorage.clear();
                setHeadingModal( 'Thông báo' );
                setStylesModal( SUCCESS_MODAL );
                setContentModal( <p>
                    Cập nhật ảnh đại diện thành công !
                </p> );
                setIsOpenModal( true );
            }
        } );
    };
    const handleUploadCamera = ( e ) =>
    {
        setContentModal(
            <>
                <FileUploader
                    over
                    onCancel={ stopFakeProgress }
                    onDrop={ ( acceptedFiles, rejectedFiles ) =>
                    {
                        const formData = new FormData();
                        formData.append(
                            "image",
                            acceptedFiles[ 0 ],
                            acceptedFiles[ 0 ].name
                        );
                        // handle file upload...
                        startFakeProgress();

                        setFileImage( acceptedFiles );
                        apiPostImage( token, formData, ( err, res ) =>
                        {
                            if ( err ) return console.log( err );
                            if ( res )
                            {
                                setAvatar( res.data );
                                localStorage.setItem( 'avatar_url', res.data );
                            }

                        } );
                    } }
                    progressAmount={ progressAmount }
                    progressMessage={
                        progressAmount
                            ? `Đang tải hình ảnh sản phẩm... ${ progressAmount }% of 100%`
                            : ''
                    }
                />
                <Button
                    overrides={ {
                        BaseButton: {
                            style: {
                                outline: `#1565D8`,
                                backgroundColor: `#1565D8`,
                                width: '100%',
                                ':hover': {
                                    outline: `#1053B5`,
                                    backgroundColor: `#1053B5`
                                }
                            }
                        }
                    } }
                    onClick={ handleUpdateAvatar }
                >
                    Đăng ảnh
                </Button>
            </>
        );
        setHeadingModal( 'Tải ảnh đại diện cá nhân' );
        setStylesModal( BLUE_MODAL );
        setIsOpenModal( true );
    };
    const router = useRouter();
    const [ isOpenModal, setIsOpenModal ] = useState( false );
    const [ contentModal, setContentModal ] = useState( <p>đây là nội dung 2</p> );
    const [ headingModal, setHeadingModal ] = useState( "đây là tiêu đề" );
    const [ stylesModal, setStylesModal ] = useState( ERROR_MODAL );
    useEffect( () =>
    {
        apiCheckLogin( token, ( err, res ) =>
        {
            if ( err ) console.log( err );
            else
            {
                if ( res.success )
                {
                    setAvatar( res.data.avatar );
                    setMailValue( res.data.email );
                    setNameValue( res.data.name );
                    setNameEditValue( res.data.name );
                    setCreateAt( formatDate( res.data.created_at ) );
                    setBirthDay( [ new Date( res.data.birthday ) ] );
                    if(res.data.gender) setGender( [ GENDER[ res.data.gender ] ] );
                    setPhoneNumber( res.data.phone_number );
                    setAccountMoney( res.data.ewallet.account_balance );
                } else
                {
                    router.push( '/' );
                }
            }
        } );
        Scrollbar.init( document.getElementById( 'my-scrollbar' ),
            {
                damping: 0.05,
                thumbMinSize: 20,
                renderByPixels: true,
                alwaysShowTracks: false,
                continuousScrolling: true
            } );
        return () =>
        {
            Scrollbar.destroyAll();
        };
    }, [] );

    const handleOnClick = ( e ) =>
    {
        e.preventDefault();
        apiLogout( token, ( err, res ) =>
        {
            if ( err ) console.log( err );
            else
            {
                console.log( res );
            }
        } );
        setTimeout( () =>
        {
            router.push( '/' );
        }, 3000 );
    };

    SwiperCore.use( [ Navigation, Pagination, SpScrollbar, A11y, EffectCoverflow ] );
    return (
        <Layout >
            <div className="box-container">
                <section className="profile-wrapper">
                    <div className="profile__wrapper marginTop-l">
                        <div className="profile-left">
                            <div className="profile-left-info">
                                <div className="avatar">
                                    <button className="profile-avatar" onClick={ handleUploadCamera }>
                                        <Camera />
                                    </button>
                                    <Avatar
                                        name={ nameValue ? nameValue : 'No name' }
                                        size="90px"
                                        src={ avatar_url }
                                        id="avatar"
                                    />
                                </div>
                                <p className="name">{ nameValue }</p>
                                <p className="joindate">Thành viên từ { createdAt }</p>
                                <p className="money">Số dư trong tài khoản: { accountMoney }đ</p>
                            </div>
                            <ul className="profile-links">
                                {
                                    profilelinks.map( ( e, index ) =>
                                    {
                                        return (
                                            <li className={ e.status == `active` ? `item active` : `item` } key={ "linkinfo-" + index }>
                                                <Link href={ e.href }>
                                                    <a>
                                                        <span>
                                                            { e.title }
                                                        </span>
                                                    </a>
                                                </Link>
                                            </li>
                                        );
                                    } )
                                }
                                <li className={ `item` }>
                                    <a>
                                        <button onClick={ handleOnClick }>
                                            Đăng xuất
                            </button>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div className="profile-right">
                            <div className="profile-block">
                                <div className="profile-block-heading">
                                    <h3>Thông tin cá nhân</h3>
                                </div>
                                <div className="profile-block-description flex-3">
                                    <FormControl
                                        label="Họ tên*"
                                    >
                                        <Input
                                            onChange={ e => setNameEditValue( e.target.value ) }
                                            value={ nameEdit }
                                            id={ props.id }
                                            placeholder={ "Nhập họ tên của bạn" }
                                        />
                                    </FormControl>
                                    <FormControl
                                        label="Số điện thoại"
                                    >
                                        <Input
                                            onChange={ e => setPhoneNumber( e.target.value ) }
                                            value={ phoneNumber }
                                            id={ props.id }
                                            placeholder={ phoneNumber }
                                            type="number"
                                        />
                                    </FormControl>
                                    <FormControl
                                        label="Giới tính"
                                    >
                                        <Select
                                            id="sexual"
                                            value={ gender ? gender :  [{ id: 'Khác', sexual: '2' }] }
                                            onChange={ ( { value } ) => setGender( value ) }
                                            options={ [
                                                { id: 'Nam', sexual: '0' },
                                                { id: 'Nữ', sexual: '1' },
                                                { id: 'Khác', sexual: '2' },
                                            ] }
                                            labelKey="id"
                                            valueKey="sexual"
                                            placeholder=""
                                        />
                                    </FormControl>
                                    <FormControl
                                        label="Ngày sinh"
                                    >
                                        <DatePicker
                                            value={ birthday }
                                            onChange={ ( { date } ) =>
                                                setBirthDay( Array.isArray( date ) ? date : [ date ] )
                                            }
                                        />
                                    </FormControl>
                                    <FormControl
                                        label="Email*"
                                    >
                                        <Input
                                            onChange={ e => setMailValue( e.target.value ) }
                                            value={ mailValue }
                                            id={ props.id }
                                            disabled
                                        />
                                    </FormControl>
                                    <Button
                                        overrides={ {
                                            BaseButton: {
                                                style: {
                                                    outline: `#FFAC12`,
                                                    backgroundColor: `#FFAC12`,
                                                    marginTop: `16px`,
                                                    width: '100%',
                                                    ':hover': {
                                                        outline: `#e69500`,
                                                        backgroundColor: ` #e69500`
                                                    }
                                                }
                                            }
                                        } }
                                        onClick={ handleEditUser }
                                    >
                                        Lưu
                        </Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <Notification
                isOpen={ isOpenModal }
                onClose={ () => { setIsOpenModal( false ); } }
                styles={ stylesModal }
                content={ contentModal }
                header={ headingModal }
                onClickFeatureButton={ () => { setIsOpenModal( false ); } }
            />
        </Layout>
    );
};

export default PostPage;
