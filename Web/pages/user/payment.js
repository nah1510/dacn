import React, { Component, useState, useEffect } from 'react';
import Link from 'next/link';
import Paypal from '../../public/svgicon/paypal.svg';
import { FileUploader } from 'baseui/file-uploader';
import { RadioGroup, Radio } from "baseui/radio";
import { FormControl } from 'baseui/form-control';
import { Input } from 'baseui/input';
import { Button } from "baseui/button";
import Layout from '../../layout/layout';
import Scrollbar from 'smooth-scrollbar';
import { Avatar } from "baseui/avatar";
import { useFakeProgress } from "../../reusefunc/reuse";
import Cookies from 'js-cookie';
import { apiCheckLogin, apiLogout, apiCreateTransaction, apiPutTransaction, apiPutTransactionVNPay, apiPostImage, updateOwnUser } from '../../API';
import { formatDate } from '../../reusefunc/reuse';
import { useRouter } from 'next/router';
import { ERROR_MODAL, SUCCESS_MODAL, WARNING_MODAL, PRIMARY_MODAL, BLUE_MODAL } from '../../reusefunc/reuse';
import Notification from '../../components/modal';
import Camera from '../../public/svgicon/camera.svg';

const profilelinks = [ { title: 'Thông tin cá nhân', href: '/user/profile' }, { title: 'Bất động sản của tôi', href: '/user/myproducts' },
{ title: 'Đổi mật khẩu', href: '/user/changepassword' }, { title: 'Thanh toán', href: '/user/payment', status: 'active' } ];

function PostPage ( props )
{
    const [ avatar_url, setAvatar ] = useState( '' );
    const [ selectPayMent_radio, setSelectedPayment ] = useState( '1' );
    const token = Cookies.get( 'user' );
    const router = useRouter();
    const [
        progressAmount,
        startFakeProgress,
        stopFakeProgress,
    ] = useFakeProgress();
    const [ fileImage, setFileImage ] = useState( null );
    const [ money, setMoney ] = useState( 0 );
    const [ accountMoney, setAccountMoney ] = useState( 0 );
    const [ checkErrInput, setCheckErrInput ] = useState( false );

    // handle modal event
    const [ contentModal, setContentModal ] = useState( <p>đây là nội dung 2</p> );
    const [ headingModal, setHeadingModal ] = useState( "đây là tiêu đề" );
    const [ stylesModal, setStylesModal ] = useState( ERROR_MODAL );
    const [ isOpenModal, setIsOpenModal ] = useState( false );


    // handle payment url
    const [ paymentURL, setPaymentURL ] = useState( null );

    // handle avatar
    const handleUpdateAvatar = ( e ) =>
    {
        e.preventDefault();
        const object = {};
        const avatar = localStorage.getItem( 'avatar_url' );
        object[ 'avatar' ] = avatar;
        updateOwnUser( token, object, ( err, res ) =>
        {
            if ( err ) return;
            if ( res )
            {
                localStorage.clear();
                setHeadingModal( 'Thông báo' );
                setStylesModal( SUCCESS_MODAL );
                setContentModal( <p>
                    Cập nhật ảnh đại diện thành công !
                </p> );
                setIsOpenModal( true );
            }
        } );
    };
    const handleUploadCamera = ( e ) =>
    {
        setContentModal(
            <>
                <FileUploader
                    over
                    onCancel={ stopFakeProgress }
                    onDrop={ ( acceptedFiles, rejectedFiles ) =>
                    {
                        const formData = new FormData();
                        formData.append(
                            "image",
                            acceptedFiles[ 0 ],
                            acceptedFiles[ 0 ].name
                        );
                        // handle file upload...
                        startFakeProgress();

                        setFileImage( acceptedFiles );
                        apiPostImage( token, formData, ( err, res ) =>
                        {
                            if ( err ) return console.log( err );
                            if ( res )
                            {
                                setAvatar( res.data );
                            }

                        } );
                    } }
                    progressAmount={ progressAmount }
                    progressMessage={
                        progressAmount
                            ? `Đang tải hình ảnh sản phẩm... ${ progressAmount }% of 100%`
                            : ''
                    }
                    onChange={ ( e ) =>
                    {
                        console.log( 'onchange event: ', e.target.event );
                    } }
                />
                <Button
                    overrides={ {
                        BaseButton: {
                            style: {
                                outline: `#1565D8`,
                                backgroundColor: `#1565D8`,
                                width: '100%',
                                ':hover': {
                                    outline: `#1053B5`,
                                    backgroundColor: `#1053B5`
                                }
                            }
                        }
                    } }
                    onClick={ handleUpdateAvatar }
                >
                    Đăng ảnh
                </Button>
            </>
        );
        setHeadingModal( 'Tải ảnh đại diện cá nhân' );
        setStylesModal( BLUE_MODAL );
        setIsOpenModal( true );
    };

    useEffect( () =>
    {
        apiCheckLogin( token, ( err, res ) =>
        {
            if ( err ) console.log( err );
            else
            {
                if ( res.success )
                {
                    setAvatar( res.data.avatar );
                    setNameValue( res.data.name );
                    setCreateAt( formatDate( res.data.created_at ) );
                    setAccountMoney( res.data.ewallet.account_balance );
                } else
                {
                    router.push( '/' );
                }
            }
        } );
        Scrollbar.init( document.getElementById( 'my-scrollbar' ),
            {
                damping: 0.05,
                thumbMinSize: 20,
                renderByPixels: true,
                alwaysShowTracks: false,
                continuousScrolling: true
            } );
        return () =>
        {
            Scrollbar.destroyAll();
        };
    }, [] );
    const [ passwordValue, setPasswordValue ] = useState( null );
    const [ passwordOldValue, setOldPasswordValue ] = useState( null );
    const [ nameValue, setNameValue ] = useState( null );
    const [ mailValue, setMailValue ] = useState( null );
    const [ city, setCity ] = useState( null );
    const [ phoneNumber, setPhoneNumber ] = useState( null );
    const [ date, setDate ] = useState( [ new Date() ] );
    const [ createdAt, setCreateAt ] = useState( '' );

    const handleOnClick = ( e ) =>
    {
        e.preventDefault();
        apiLogout( token, ( err, res ) =>
        {
            if ( err ) console.log( err );
            else
            {
                console.log( res );
                setTimeout( () =>
                {
                    router.push( '/' );
                }, 2000 );
            }
        } );
    };
    const handleTransactions = ( e ) =>
    {
        let url;
        e.preventDefault();
        const formData = new FormData();
        console.log( 'radio: ', selectPayMent_radio );
        formData.append( "return_url", process.env.URL + 'completed_payment' );
        if ( money >= 20000 )
        {
            if ( selectPayMent_radio == "1" )
            {
                url = 'paypal';
                formData.append( "price", parseFloat( money ).toFixed( 2 ) );
            }
            if ( selectPayMent_radio == "2" )
            {
                url = 'vnpay';
                formData.append( "price", parseFloat( money ).toFixed( 2 ) );
            }
            apiCreateTransaction( token, url, formData, ( err, res ) =>
            {

                if ( err ) return console.log( err );
                if ( res )
                {
                    // console.log( 'payment res: ', res );
                    router.push( res.data );
                }
            } );

        } else
        {
            setIsOpenModal( true );
            setStylesModal( ERROR_MODAL );
            setContentModal(
                <p className="danger-text">
                    Vui lòng nạp số tiền lớn hơn mệnh giá 20.000 vnđ !
                    </p>
            );
        }
    };
    return (
        <Layout >
            <div className="box-container">
                <section className="profile-wrapper">
                    <div className="profile__wrapper marginTop-l">
                        <div className="profile-left">
                            <div className="profile-left-info">
                                <div className="avatar">
                                    <button className="profile-avatar" onClick={ handleUploadCamera }>
                                        <Camera />
                                    </button>
                                    <Avatar
                                        name={ nameValue ? nameValue : 'No name' }
                                        size="90px"
                                        src={ avatar_url }
                                        id="avatar"
                                    />
                                </div>
                                <p className="name">{ nameValue }</p>
                                <p className="joindate">Thành viên từ { createdAt }</p>
                                <p className="money">Số dư trong tài khoản: { accountMoney }đ</p>
                            </div>
                            <ul className="profile-links">
                                {
                                    profilelinks.map( ( e, index ) =>
                                    {
                                        return (
                                            <li className={ e.status == `active` ? `item active` : `item` } key={ "linkinfo-" + index }>
                                                <Link href={ e.href }>
                                                    <a>
                                                        <span>
                                                            { e.title }
                                                        </span>
                                                    </a>
                                                </Link>
                                            </li>
                                        );
                                    } )
                                }
                                <li className={ `item` }>
                                    <a>
                                        <button onClick={ handleOnClick }>
                                            Đăng xuất
                                        </button>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div className="profile-right">
                            <div className="profile-block">
                                <div className="profile-block-heading">
                                    <h3>Thông tin thanh toán</h3>
                                </div>
                                <div className="profile-block-description flex-3">
                                    <FormControl
                                        label="Chọn phương thức thanh toán"
                                    >
                                        <RadioGroup
                                            value={ selectPayMent_radio }
                                            onChange={ e => setSelectedPayment( e.target.value ) }
                                            name="number"
                                            label={ `Chọn phương thức thanh toán` }
                                            align="horizontal"
                                        >
                                            <Radio value="1">
                                                <Paypal className="paypal-svg" />
                                            </Radio>
                                            <Radio value="2">
                                                <img alt="vnpay" src="/images/Logo-VNPAYpng.png" className="vnpay" />
                                            </Radio>
                                        </RadioGroup>
                                    </FormControl>
                                    <FormControl
                                        label="Số tiền cần nạp"
                                        error={ checkErrInput ? "Số tiền cần nạp cần lớn hơn hoặc bằng 20000" : null }
                                    >
                                        <Input
                                            onChange={ e =>
                                            {
                                                if ( e.target.value < 10000000 ) setMoney( e.target.value );
                                                if ( e.target.value < 20000 ) setCheckErrInput( true );
                                                else setCheckErrInput( false );
                                            } }
                                            value={ money }
                                            id={ props.id }
                                            placeholder={ "Nhập số tiền cần nạp" }
                                            type="number"
                                            error={ checkErrInput }
                                        />
                                    </FormControl>
                                    <Button
                                        overrides={ {
                                            BaseButton: {
                                                style: {
                                                    outline: `#FFAC12`,
                                                    backgroundColor: `#FFAC12`,
                                                    marginTop: `16px`,
                                                    width: '100%',
                                                    ':hover': {
                                                        outline: `#e69500`,
                                                        backgroundColor: ` #e69500`
                                                    }
                                                }
                                            }
                                        } }
                                        onClick={ handleTransactions }
                                    >
                                        Nạp tiền vào ví
                                    </Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <Notification
                isOpen={ isOpenModal }
                onClose={ () => { setIsOpenModal( false ); } }
                styles={ stylesModal }
                content={ contentModal }
                header={ headingModal }
            // onClickFeatureButton={ () => { setIsOpenModal( false ); } }
            />
        </Layout>
    );
};

export default PostPage;
