import axios from 'axios';

const dev_url = process.env.API_URL;
export function apiCreateTransaction ( token, urlTransaction, data, cb )
{
  axios( {
    method: 'post',
    url: 'api/' + urlTransaction,
    baseURL: dev_url,
    data: data,
    headers: { "Content-type": "application/json", 'Authorization': token }
  } )
    .then( function ( response )
    {
      const data = response.data;
      cb( null, data );
    } )
    .catch( function ( error )
    {
      cb( error, null );
    } );
}

export function apiPutTransaction ( data, cb )
{
  axios( {
    method: 'put',
    url: 'api/paypal',
    baseURL: dev_url,
    data: data,
    headers: { "Content-type": "application/json" }
  } )
    .then( function ( response )
    {
      const data = response.data;
      cb( null, data );
    } )
    .catch( function ( error )
    {
      cb( error, null );
    } );
}

export function apiPutTransactionVNPay ( data, cb )
{
  axios( {
    method: 'put',
    url: 'api/vnpay',
    baseURL: dev_url,
    data: data,
    headers: { "Content-type": "application/json" }
  } )
    .then( function ( response )
    {
      const data = response.data;
      cb( null, data );
    } )
    .catch( function ( error )
    {
      cb( error, null );
    } );
}

