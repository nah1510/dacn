import axios from 'axios';

const dev_url = process.env.API_URL;
const base_url = "http://gondarpham.xyz";

export function apiChangePassword ( token, data, cb )
{
    axios( {
        method: 'post',
        url: 'api/auth/change-password',
        baseURL: dev_url,
        data: data,
        headers: { "Content-type": "application/json", 'Authorization': token }
    } ).then( function ( response )
    {
        const data = response.data;
        cb( null, data );
    } ).catch( function ( error )
    {
        cb( error, null );
    } );
}
export function updateUser ( token, id, formData, cb )
{
    axios( {
        method: 'put',
        url: `api/admin/user/${ id }`,
        baseURL: dev_url,
        data: formData,
        headers: { "Content-type": "application/json", 'Authorization': token }
    } ).then( function ( response )
    {
        const data = response.data.data;
        cb( null, data );
    } ).catch( function ( error )
    {
        cb( error, null );
    } );
}


export function updateOwnUser ( token, formData, cb )
{
    axios( {
        method: 'put',
        url: `api/user`,
        baseURL: dev_url,
        data: formData,
        headers: { "Content-type": "application/json", 'Authorization': token }
    } ).then( function ( response )
    {
        const data = response.data.data;
        cb( null, data );
    } ).catch( function ( error )
    {
        cb( error, null );
    } );
}
