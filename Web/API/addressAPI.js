import axios from 'axios';

const dev_url = process.env.API_URL;
const base_url = "http://gondarpham.xyz";


export function apiGetListProvinces ( token, cb )
{
    axios( {
        method: 'get',
        url: 'api/address/province',
        baseURL: dev_url,
        headers: { "Content-type": "application/json", "Authorization": token }
    } )
        .then( function ( response )
        {
            const data = response.data.data;
            cb( null, data );
        } )
        .catch( function ( error )
        {
            cb( error, null );
        } );
}

export function apiGetListWards ( token, district_id, cb )
{
    axios( {
        method: 'get',
        url: 'api/address/ward?district_id=' + district_id,
        baseURL: dev_url,
        headers: { "Content-type": "application/json", "Authorization": token }
    } )
        .then( function ( response )
        {
            const data = response.data.data;
            cb( null, data );
        } )
        .catch( function ( error )
        {
            cb( error, null );
        } );
}

export function apiGetListDistricts ( token, province_id, cb )
{
    axios( {
        method: 'get',
        url: 'api/address/district?province_id=' + province_id,
        baseURL: dev_url,
        headers: { "Content-type": "application/json", "Authorization": token }
    } )
        .then( function ( response )
        {
            const data = response.data.data;
            cb( null, data );
        } )
        .catch( function ( error )
        {
            cb( error, null );
        } );
}

export function apiGetDistrict ( id, cb )
{
    axios( {
        method: 'get',
        url: 'api/address/district/' + id,
        baseURL: dev_url,
        headers: { "Content-type": "application/json" }
    } )
        .then( function ( response )
        {
            const data = response.data.data;
            cb( null, data );
        } )
        .catch( function ( error )
        {
            cb( error, null );
        } );
}

export function apiGetProvince ( id, cb )
{
    axios( {
        method: 'get',
        url: 'api/address/province/' + id,
        baseURL: dev_url,
        headers: { "Content-type": "application/json" }
    } )
        .then( function ( response )
        {
            const data = response.data.data;
            cb( null, data );
        } )
        .catch( function ( error )
        {
            cb( error, null );
        } );
}

export function apiGetWard ( id, cb )
{
    axios( {
        method: 'get',
        url: 'api/address/ward/' + id,
        baseURL: dev_url,
        headers: { "Content-type": "application/json" }
    } )
        .then( function ( response )
        {
            const data = response.data.data;
            cb( null, data );
        } )
        .catch( function ( error )
        {
            cb( error, null );
        } );
}
