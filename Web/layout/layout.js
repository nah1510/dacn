import Head from 'next/head';
import styles from './layout.module.css';
import utilStyles from '../styles/utils.module.css';
import Link from 'next/link';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Header from '../components/header';
import { Button } from "baseui/button";
import Footer from "../components/footer";
import ArrowWhite from "../public/svgicon/ArrowWhite.svg";
// import {getCookie} from '../reusefunc/reuse';
import Cookies from 'js-cookie';
const name = 'Duy Nguyen';
import { apiCheckLogin } from '../API/loginAPI';
export const siteTitle = 'Next.js Sample Website';

export default function Layout ( { children, home } )
{
    const router = useRouter();
    const [ token, setToken ] = useState( Cookies.get( 'user' ) );
    const [ isLogin, setIsLogin ] = useState( false );
    const [ userProfile, setUserProfile ] = useState( {} );
    useEffect( () =>
    {
        apiCheckLogin( token, ( err, res ) =>
        {
            if ( err ) console.log( err );
            else
            {
                if ( res.success == true )
                {
                    setIsLogin( true );
                    setUserProfile( res.data );
                } else
                {
                    setIsLogin( false );
                }
            }
        } );
    }, [ isLogin ] );
    return (
        <div className="page-transition-wrapper" id="my-scrollbar">
            <Head>
                <link rel="icon" href="/favicon.ico" />
                <meta
                    name="description"
                    content="Learn how to build a personal website using Next.js"
                />
                <meta
                    property="og:image"
                    content={ `https://og-image.now.sh/${ encodeURI(
                        siteTitle
                    ) }.png?theme=light&md=0&fontSize=75px&images=https%3A%2F%2Fassets.vercel.com%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg` }
                />
                <meta name="og:title" content={ siteTitle } />
                <meta name="twitter:card" content="summary_large_image" />
                <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests" />
            </Head>
            <header className="header">
                <Header isLogin={ isLogin } data={ userProfile } token={ token } />
                <div className="header-post">
                    <Button
                        overrides={ {
                            BaseButton: {
                                style: {
                                    outline: `#FFAC12`,
                                    backgroundColor: `#FFAC12`,
                                    borderRadius: `0 20px 0 0`,
                                    whiteSpace: `nowrap`,
                                    padding: `16px`,
                                    ':hover': {
                                        outline: `#ed9a00`,
                                        backgroundColor: `#ed9a00`
                                    }
                                }
                            }
                        } }
                        onClick={ () =>
                        {
                            router.push( '/postpage' );
                        } }
                    >
                        Đăng tin <span className="marginLeft-xxs"><ArrowWhite /></span>
                    </Button>
                </div>
            </header>
            <main>{ children }</main>
            <Footer />
        </div>
    );
}
