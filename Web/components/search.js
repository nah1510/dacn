import React, { Component, useState, useEffect } from 'react';
import Link from 'next/link';
import Heart from "../public/images/card/heart.svg";
import Checked from "../public/svgicon/checked.svg";
import Bed from "../public/images/card/hotel-single-bed-1.svg";
import Bathroom from "../public/images/card/bathroom-tub-towel.svg";
import Artboard from "../public/images/card/grid-artboard.svg";
import { Tabs, Tab } from "baseui/tabs";
import { FormControl } from 'baseui/form-control';
import { Select } from 'baseui/select';
import { Input } from "baseui/input";
import { Button } from "baseui/button";
import { useRouter } from 'next/router';
import Search from "baseui/icon/search";
import { apiGetListProvinces, apiGetListDistricts, apiGetListWards } from '../API';
import Cookies from 'js-cookie';
import Scrollbar from 'smooth-scrollbar';


const type_Array = [ { value: 0, name: 'Nhà riêng' }, { value: 1, name: 'Chung cư' }, { value: 2, name: 'Đất nền' } ];
const direction_Array = [ { value: 0, name: 'Bắc' }, { value: 1, name: 'T.Bắc' }, { value: 2, name: 'Đ.Bắc' }, { value: 3, name: 'Nam' },
{ value: 4, name: 'T.Nam' }, { value: 5, name: 'Đ.Nam' }, { value: 6, name: 'Bắc' }, { value: 0, name: 'Tây' } ];

const orderBy_Array = [ { value: 'created_at', name: 'Ngày đăng' }, { value: 'price', name: 'Giá' } ];
const price_Array = [ { value: 200000000, name: 'trên 200 triệu' }, { value: 400000000, name: 'trên 400 triệu' }, { value: 600000000, name: 'trên 600 triệu' }, { value: 1000000000, name: 'trên 1 tỷ' }, { value: 2000000000, name: 'trên 2 tỷ' }, { value: 4000000000, name: 'trên 4 tỷ' } ];

function SearchComponents ( props )
{
    const router = useRouter();
    const token = Cookies.get( 'user' );
    const { post_type } = props;
    const [ activeKey, setActiveKey ] = useState( "" );
    const [ orderValue, setOrderValue ] = useState( null );
    const [ valueTypeProperties, setTypePropertyies ] = useState( null );
    const [ directionValue, setdirectionValue ] = useState( null );
    const [ valueArea, setArea ] = useState( null );
    const [ valuePrice, setPrice ] = useState( null );
    const [ project, setProject ] = useState( [] );
    const [ inputValue, setInputValue ] = useState();

    // address: set address
    const [ city, setCity ] = useState( null );
    const [ district, setDistrict ] = useState( null );
    const [ ward, setWard ] = useState( null );

    const [ cities, setCities ] = useState( [] );
    const [ districts, setDistricts ] = useState( [] );
    const [ wards, setWards ] = useState( [] );

    const handleSearch = e =>
    {
        let query = `post_type=${ post_type }&limit=12${ city && city != [] ? "&province_id=" + city[ 0 ].id : '' }
        ${ district && district != [] ? "&district_id=" + district[ 0 ].id : '' }${ ward && ward != [] ? "&ward_id=" + ward[ 0 ].id : '' }${ directionValue && directionValue != [] ? "&direction=" + directionValue[ 0 ].value : '' }${ valuePrice && valuePrice != [] ? "&mt=" + valuePrice[ 0 ].value : '' }${ orderValue && orderValue != [] ? "&order_by=" + orderValue[ 0 ].value : '' } ${ valueTypeProperties && valueTypeProperties != [] ? "&type=" + valueTypeProperties[ 0 ].value : '' }${ inputValue ? "&keyword=" + inputValue : '' }
        `;
        router.push( `/search/${ query }` );
    };

    useEffect( () =>
    {
        apiGetListProvinces( token, ( err, res ) =>
        {
            if ( err ) return;
            else
            {
                setCities( res );
            }
        } );
        Scrollbar.init( document.getElementById( 'my-scrollbar' ),
            {
                damping: 0.05,
                thumbMinSize: 20,
                renderByPixels: true,
                alwaysShowTracks: false,
                continuousScrolling: true
            } );
        return () =>
        {
            Scrollbar.destroyAll();
        };
    }, [] );

    return (
        <div className="search">
            <div className="search-section">
                <FormControl
                    overrides={ {
                        ControlContainer: {
                            style: () => ( {
                                flex: `6 0 0`
                            } )
                        }
                    } }
                >
                    <Select
                        id="select-id"
                        value={ valueTypeProperties }
                        onChange={ ( { value } ) => setTypePropertyies( value ) }
                        options={ type_Array }
                        labelKey="name"
                        valueKey="value"
                        placeholder="Chọn loại BĐS"

                    />
                </FormControl>
                <FormControl
                    overrides={ {
                        ControlContainer: {
                            style: () => ( {
                                flex: `20 0 0`
                            } )
                        }
                    } }
                >
                    <Input
                        value={ inputValue }
                        onChange={ e => setInputValue( e.target.value ) }
                        placeholder="Tìm kiếm tên bài đăng"
                        clearOnEscape
                    />
                </FormControl>
                <FormControl
                    overrides={ {
                        ControlContainer: {
                            style: () => ( {
                                flex: `3 0 0`
                            } )
                        }
                    } }
                >
                    <Button
                        onClick={ handleSearch }
                        startEnhancer={ () => <Search size={ 24 } /> }
                        overrides={ {
                            BaseButton: {
                                style: {
                                    outline: `#FFAC12`,
                                    backgroundColor: `#FFAC12`,
                                    whiteSpace: `nowrap`,
                                    padding: `12px`,
                                    ':hover': {
                                        outline: `#ed9a00`,
                                        backgroundColor: `#ed9a00`
                                    }
                                }
                            }
                        } }
                    >
                        Tìm kiếm
                    </Button>
                </FormControl>
            </div>
            <div className="search-section">
                <FormControl>
                    <Select
                        id="select-city"
                        value={ city }
                        onChange={ ( { value } ) =>
                        {
                            setCity( value );
                            if ( value.length != 0 )
                            {
                                apiGetListDistricts( token, value[ 0 ].id, ( err, res ) =>
                                {
                                    if ( err ) return;
                                    else
                                    {
                                        setDistricts( res );
                                    }
                                } );
                            }
                        } }
                        options={ cities }
                        labelKey="name"
                        valueKey="id"
                        placeholder="Chọn tỉnh/thành phố"
                    />
                </FormControl>
                <FormControl>
                    <Select
                        id="select-district"
                        value={ district }
                        onChange={ ( { value } ) =>
                        {
                            setDistrict( value );
                            if ( value.length != 0 )
                            {
                                apiGetListWards( token, value[ 0 ].id, ( err, res ) =>
                                {
                                    if ( err ) return;
                                    else
                                    {
                                        setWards( res );
                                    }
                                } );
                            }
                        } }
                        options={ districts }
                        labelKey="name"
                        valueKey="id"
                        placeholder="Chọn Quận / huyện"
                    />
                </FormControl>
                <FormControl>
                    <Select
                        id="select-ward"
                        value={ ward }
                        onChange={ ( { value } ) => setWard( value ) }
                        options={ wards }
                        labelKey="name"
                        valueKey="id"
                        placeholder="Chọn phường / xã"
                    />
                </FormControl>
                <FormControl>
                    <Select
                        id="select-id"
                        value={ directionValue }
                        onChange={ ( { value } ) => setdirectionValue( value ) }
                        options={ direction_Array }
                        labelKey="name"
                        valueKey="id"
                        placeholder="Chọn hướng"
                    />
                </FormControl>
                <FormControl>
                    <Select
                        id="select-id"
                        value={ orderValue }
                        onChange={ ( { value } ) => setOrderValue( value ) }
                        options={
                            orderBy_Array
                        }
                        labelKey="name"
                        valueKey="value"
                        placeholder="Sắp xếp theo"
                    />
                </FormControl>
                <FormControl>
                    <Select
                        id="select-id"
                        value={ valuePrice }
                        onChange={ ( { value } ) => setPrice( value ) }
                        options={
                            price_Array }
                        labelKey="name"
                        valueKey="value"
                        placeholder="chọn mức giá"
                    />
                </FormControl>
            </div>
        </div>
    );
};

export default SearchComponents;
