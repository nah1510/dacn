import React, { useState, useEffect } from "react";
import Link from 'next/link';
import { useRouter } from 'next/router';
import { apiCheckLogin, apiLogout } from '../API/loginAPI';
import
{
  AppNavBar,
  setItemActive
} from "baseui/app-nav-bar";
import
{
  ChevronDown,
  Delete,
  Overflow,
  Upload
} from "baseui/icon";
import Cookies from 'js-cookie';





function Header ( props )
{
  const MAIN_NAV_UNLOGGED = [ { id: "1", icon: Upload, label: "Cần mua - cần thuê", href: '/search/limit=12' },
  { id: "2", icon: Upload, label: "Nhà cho thuê", href: '/search/post_type=1&limit=12' },
  { id: "3", icon: Upload, label: "Nhà đất bán", href: '/search/post_type=0&limit=12' },
  { id: "4", icon: Upload, label: "Đăng ký", href: '/signup' },
  {
    id: "5",
    active: true,
    icon: ChevronDown,
    label: "Đăng nhập",
    href: '/login',
  },
  ];
  const MAIN_NAV_LOGGED = [
    { id: "1", icon: Upload, label: "Cần mua - cần thuê", href: '/search/limit=12' },
    // { id: "2", icon: Upload, label: "Dự án", href: '/' },
    { id: "2", icon: Upload, label: "Nhà cho thuê", href: '/search/post_type=1&limit=12' },
    { id: "3", icon: Upload, label: "Nhà đất bán", href: '/search/post_type=0&limit=12' },
    // { id: "5", icon: Upload, label: "Tin tức", href: '/' },
  ];
  const router = useRouter();
  const token = Cookies.get( 'user' );
  const handleClickMainItem = ( href ) =>
  {
    router.push( href );
  };
  const { isLogin, data } = props;
  const [ mainItems, setMainItems ] = useState( MAIN_NAV_UNLOGGED );
  useEffect( () =>
  {
    apiCheckLogin( token, ( err, res ) =>
    {
      if ( err ) return;
      else
      {
        if ( res.success )
        {
          setMainItems( MAIN_NAV_LOGGED );
        } else
        {
          setMainItems( MAIN_NAV_UNLOGGED );
        }
      }
    } );
  }, [ token ] );
  const handleOnUserSelect = ( item ) =>
  {
    if ( item.href )
    {
      router.push( item.href );
    }
    if ( item.label == 'Đăng xuất' )
    {
      apiLogout( token, ( err, res ) =>
      {
        if ( err ) console.log( err );
        else
        {
          if ( res.success == true )
          {
            router.push( '/' );
          }
        }
      } );
    }
    return;
  };
  return (
    <AppNavBar
      title={
        <Link href="/">
          <a>
            <img className="header-logo" alt="logo" src="/images/Logo.png" />
          </a>
        </Link>
      }
      mainItems={ mainItems }
      onMainItemSelect={ item =>
      {
        setMainItems( prev => setItemActive( prev, item ) );
      } }
      username={ isLogin ? data.name : "default" }
      usernameSubtitle={ isLogin ? data.email : "default" }
      userImgUrl={ isLogin ? data.avatar : "default" }
      userItems={ isLogin ?
        [
          { icon: Overflow, label: "Thông tin cá nhân", href: '/user/profile' },
          { icon: Overflow, label: "Bất động sản của tôi", href: '/user/myproducts' },
          { icon: Overflow, label: "Đổi mật khẩu", href: '/user/changepassword' },
          { icon: Overflow, label: "Thanh toán", href: '/user/payment' },
          { icon: Overflow, label: "Đăng xuất" },
        ] : [] }
      onUserItemSelect={ item => handleOnUserSelect( item ) }
      onMainItemSelect={ ( item ) =>
      {
        handleClickMainItem( item.href );
      } }
      overrides={ {
        MainMenuItem: {
          style: () => ( {
            whiteSpace: `nowrap`,
            color: `#ffffff`,
            transition: `all 0.2s ease-in`,
            cursor: `pointer`,
            ':hover': {
              color: `#FFAC12`
            }
          } )
        },
        Root: {
          style: () => ( {
            background: `transparent`
          } )
        }
      } }
    />
  );
};

export default Header;
