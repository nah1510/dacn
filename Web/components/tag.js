import React, { Component, useState, useEffect } from 'react'; 

function Tag(props) {
    const { content, onClick } = props;
    return (
        <span className="tag" onClick={onClick}>
            {content ? content : 0}
        </span>
    );
};

export default Tag;