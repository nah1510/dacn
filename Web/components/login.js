import React, { Component, useState, useEffect } from 'react';
import { FormControl } from 'baseui/form-control';
import { Input } from 'baseui/input';
import { Checkbox } from 'baseui/checkbox';
import { Button } from "baseui/button";
import Link from 'next/link';
import Checked from '../public/svgicon/checked.svg';
import { apiLogin } from '../API/loginAPI';
import { useRouter } from 'next/router';
import { useCookies } from "react-cookie";
import Notification from './modal';
import { ERROR_MODAL, SUCCESS_MODAL, WARNING_MODAL, PRIMARY_MODAL, BLUE_MODAL } from '../reusefunc/reuse';

function Login ( props )
{
    // router
    const router = useRouter();
    const [ checked, setChecked ] = useState( true );
    // value
    const [ mailValue, setMailValue ] = useState( '' );
    const [ passwordValue, setPasswordValue ] = useState( '' );
    const [ cookie, setCookie ] = useCookies( [ "user" ] );
    const { fullpageApi } = props;
    const handleOnClick = ( e ) =>
    {
        e.preventDefault();
        const formData = new FormData();
        formData.append( "email", mailValue );
        formData.append( "password", passwordValue );
        apiLogin( formData, ( err, res ) =>
        {
            if ( err ) console.log( 'err: ', err );
            else
            {
                console.log( 'res: ', res );
                if ( res.success == true )
                {
                    setCookie( "user", res.data.token, {
                        path: "/",
                        maxAge: 3600, // Expires after 1hr
                        sameSite: true,
                    } );
                    router.push( '/' );
                }
                if ( res.success == false )
                {
                    setHeadingModal( 'Lỗi đăng nhập' );
                    setStylesModal( ERROR_MODAL );
                    setContentModal(
                        <p className="danger-text">
                            { res.msg }
                        </p>
                    );
                    setIsOpenModal( true );
                }
            }
        } );
    };
    const [ forgetPasswordValue, SetForgetPasswordValue ] = useState( null );

    // handle modal event
    const [ contentModal, setContentModal ] = useState( <p>đây là nội dung 2</p> );
    const [ headingModal, setHeadingModal ] = useState( "đây là tiêu đề" );
    const [ stylesModal, setStylesModal ] = useState( ERROR_MODAL );
    const [ isOpenModal, setIsOpenModal ] = useState( false );

    const testModal = ( e ) =>
    {
        e.preventDefault();
        setIsOpenModal( true );
    };
    const handleForgetPassword = ( e ) =>
    {
        e.preventDefault();
        setContentModal(
            <Input
                onChange={ ( e ) =>
                {
                    SetForgetPasswordValue( e.target.value );
                } }
                value={ forgetPasswordValue }
                id={ props.id }
                placeholder={ "Nhập email hoặc số điện thoại" }
            />
        );
        setHeadingModal( 'Lấy lại mật khẩu' );
        setStylesModal( BLUE_MODAL );
        setIsOpenModal( true );
    };

    const handleKeyDown = ( e ) =>
    {
        // e.preventDefault();
        if ( e.keyCode == 13 )
        {
            const formData = new FormData();
            formData.append( "email", mailValue );
            formData.append( "password", passwordValue );
            apiLogin( formData, ( err, res ) =>
            {
                if ( err ) console.log( 'err: ', err );
                else
                {
                    console.log( 'res: ', res );
                    if ( res.success == true )
                    {
                        setCookie( "user", res.data.token, {
                            path: "/",
                            maxAge: 3600, // Expires after 1hr
                            sameSite: true,
                        } );
                        router.push( '/' );
                    }
                    if ( res.success == false )
                    {
                        setHeadingModal( 'Lỗi đăng nhập' );
                        setStylesModal( ERROR_MODAL );
                        setContentModal(
                            <p className="danger-text">
                                { res.msg }
                            </p>
                        );
                        setIsOpenModal( true );
                    }
                }
            } );
        }
    };

    return (
        <section className="login">
            <div className="login-left">
                <Link href="/">
                    <a className="login-logo">
                        <img src="/images/Logo.png" />
                    </a>
                </Link>
                <div className="login-text-box text-white">
                    <p>Bất động sản không thể mất đi hoặc bị đánh cắp và cũng như không thể tự nó có thể di chuyển được.
                    Với những đặc điểm của nó, thì với việc chi trả đầy đủ để có thể sở hữu nó.
                    Được quản lý nó cẩn thận, thì đó sẽ là kênh đầu tư an toàn nhất trên thế giới hiện nay.</p>
                    <br></br>
                    <p><strong>Franklin D.Roosevelt</strong></p>
                </div>
            </div>
            <div className="login-right">
                <div className="login-box">
                    <h3 className="login-heading">Đăng nhập</h3>
                    <FormControl
                        label="Email*"
                    >
                        <Input
                            onChange={ ( e ) =>
                            {
                                setMailValue( e.target.value );
                            } }
                            value={ mailValue }
                            id={ props.id }
                            placeholder={ "Nhập email của bạn" }
                            onKeyDown={ handleKeyDown }
                        />
                    </FormControl>
                    <FormControl
                        label="Mật khẩu*"
                    >
                        <Input
                            onChange={ e => setPasswordValue( e.target.value ) }
                            onKeyDown={ handleKeyDown }
                            value={ passwordValue }
                            id={ props.id }
                            placeholder={ "Nhập mật khẩu của bạn" }
                            type="password"
                        />
                    </FormControl>
                    <Button
                        overrides={ {
                            BaseButton: {
                                style: {
                                    outline: `#1565D8`,
                                    backgroundColor: `#1565D8`,
                                    width: '100%',
                                    ':hover': {
                                        outline: `#1053B5`,
                                        backgroundColor: `#1053B5`
                                    }
                                }
                            }
                        } }
                        onClick={ handleOnClick }
                    >
                        Đăng nhập
                </Button>
                    <p className="login-link">Bạn đã có tài khoản ?
                    <Link href="/signup">
                            <a >
                                Đăng ký tại đây
                        </a>
                        </Link>
                    hoặc
                    <button onClick={ handleForgetPassword }>
                            Quên mật khẩu
                    </button>
                    </p>
                </div>
            </div>
            <Notification
                isOpen={ isOpenModal }
                onClose={ () => { setIsOpenModal( false ); } }
                styles={ stylesModal }
                content={ contentModal }
                header={ headingModal }
                onClickFeatureButton={ () => { setIsOpenModal( false ); } }
            />
        </section>
    );
};

export default Login;
