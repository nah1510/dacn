import * as React from 'react';
import { Button } from 'baseui/button';
import
{
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    ModalButton,
} from 'baseui/modal';

export default function Notification ( props )
{
    const { isOpen, onClose, header, content, styles, onClickFeatureButton } = props;
    return (
        <React.Fragment>
            {/* <Button onClick={() => setIsOpen(true)}>Open Modal</Button> */ }
            <Modal
                onClose={ onClose }
                isOpen={ isOpen }
                overrides={ {
                    Root: {
                        style: ( { $theme } ) => ( {
                            // outline: `${$theme.colors.warning200} solid`,
                            // backgroundColor: $theme.colors.warning200
                        } )
                    },
                    Dialog: {
                        style: ( { $theme } ) => ( {
                            // outline: `${$theme.colors.warning200} solid`,
                            // backgroundColor: $theme.colors.warning200
                        } )
                    },
                    Backdrop: {
                        style: () => ( {
                            // outline: `${$theme.colors.warning200} solid`,
                            backgroundColor: `#000`,
                            opacity: `0.6`
                        } )
                    },
                    DialogContainer: {
                        style: ( { $theme } ) => ( {
                            // outline: `${$theme.colors.warning200} solid`,
                            // backgroundColor: $theme.colors.warning200
                        } )
                    },
                }
                }
            >
                <ModalHeader
                    style={ {
                        borderBottom: `1px solid ${ styles.main ? styles.main : '' }`,
                        paddingBottom: `10px`,
                        color: `${ styles.main ? styles.main : '' }`
                    } }
                >
                    { header ? header : 'default' }
                </ModalHeader>
                <ModalBody
                    style={ {
                        paddingLeft: `20px`
                    } }>
                    { content ? content : '' }
                </ModalBody>
                <ModalFooter>
                    <ModalButton kind="tertiary" onClick={ onClose }>
                        Cancel
                    </ModalButton>
                    {
                        onClickFeatureButton ?
                            <ModalButton onClick={ onClickFeatureButton }
                                overrides={ {
                                    BaseButton: {
                                        style: {
                                            outline: `${ styles.main ? styles.main : '' }`,
                                            backgroundColor: `${ styles.main ? styles.main : '' }`,
                                            ':hover': {
                                                outline: `${ styles.hover ? styles.hover : '' }`,
                                                backgroundColor: `${ styles.hover ? styles.hover : '' }`
                                            }
                                        }
                                    }
                                } }
                            >
                                Okay
                        </ModalButton> : null
                    }
                </ModalFooter>
            </Modal>
        </React.Fragment>
    );
}
