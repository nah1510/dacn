import React, { Component, useState, useEffect } from 'react';
import { Tabs, Tab } from "baseui/tabs";
import SearchComponents from "./search";

function Filter ( props )
{
    const { info } = props;
    const [ activeKey, setActiveKey ] = useState( "0" );
    return (
        <div className="search__wrapper">
            <Tabs
                onChange={ ( { activeKey } ) =>
                {
                    setActiveKey( activeKey );
                } }
                activeKey={ activeKey }
                overrides={ {
                    Tab: {
                        style: () => ( {
                            width: `fit-content`,
                        } )
                    },
                    TabBar: {
                        style: () => ( {
                            background: `#ffffff`,
                        } )
                    }
                } }
            >
                <Tab title="NHÀ ĐẤT BÁN">
                    <SearchComponents post_type={ 0 } />
                </Tab>
                <Tab title="NHÀ ĐẤT CHO THUÊ">
                    <SearchComponents post_type={ 1 } />
                </Tab>
            </Tabs>
        </div>
    );
};

export default Filter;
