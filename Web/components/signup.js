import React, { useState } from 'react';
import { FormControl } from 'baseui/form-control';
import { Input } from 'baseui/input';
import { Checkbox } from 'baseui/checkbox';
import { Button } from "baseui/button";
import Link from 'next/link';
import { apiRegister } from '../API/loginAPI';
import Notification from './modal';
import { ERROR_MODAL, SUCCESS_MODAL, WARNING_MODAL, PRIMARY_MODAL, BLUE_MODAL } from '../reusefunc/reuse';
import { useRouter } from 'next/router';
function Signup ( props )
{
    const router = useRouter();
    const [ checked, setChecked ] = useState( false );
    const [ nameValue, setNameValue ] = useState( null );
    const [ mailValue, setMailValue ] = useState( null );
    const [ passwordValue, setPasswordValue ] = useState( null );
    const [ contentModal, setContentModal ] = useState( <p>đây là nội dung 2</p> );
    const [ headingModal, setHeadingModal ] = useState( "đây là tiêu đề" );
    const [ stylesModal, setStylesModal ] = useState( ERROR_MODAL );
    const [ isOpenModal, setIsOpenModal ] = useState( false );
    const handleOnClick = ( e ) =>
    {
        e.preventDefault();
        const formData = new FormData();
        formData.append( "email", mailValue );
        formData.append( "password", passwordValue );
        formData.append( "name", nameValue );
        apiRegister( formData, ( err, res ) =>
        {
            if ( err ) console.log( 'err: ', err );
            else
            {
                if ( res.success == true )
                {

                    setHeadingModal( 'Thông báo' );
                    setStylesModal( SUCCESS_MODAL );
                    setContentModal(
                        <p className="success-text">
                            Chúc mừng, bạn đã đăng ký thành công !
                        </p>
                    );
                    setIsOpenModal( true );
                    setTimeout(()=> {
                        router.push( '/completed' );
                    },2000)
                } else
                {

                    setHeadingModal( 'Lỗi đăng nhập' );
                    setStylesModal( ERROR_MODAL );
                    setContentModal(
                        <p className="danger-text">
                            { res.msg }
                        </p>
                    );
                    setIsOpenModal( true );
                }
            }
        } );
    };


    return (
        <section className="login">
            <div className="login-left">
                <Link href="/">
                    <a className="login-logo">
                        <img src="/images/Logo.png" />
                    </a>
                </Link>
                <div className="login-text-box text-white">
                    <p>The passage experienced a surge in popularity during the 1960s when Letraset used it
                    on their dry-transfer sheets,
                    and again during the 90s as desktop publishers bundled
                    the text with their software.</p>
                    <br></br>
                    <p>Vincent Obi</p>
                </div>
            </div>
            <div className="login-right">
                <div className="login-box">
                    <h3 className="login-heading">Đăng ký tài khoản cá nhân</h3>
                    <p className="login-note">Xin vui lòng nhập thông tin cần thiết để đăng ký.</p>
                    <FormControl
                        label="Họ tên*"
                    >
                        <Input
                            onChange={ e => setNameValue( e.target.value ) }
                            value={ nameValue }
                            id={ props.id }
                            placeholder={ "Nhập họ tên của bạn" }
                        />
                    </FormControl>
                    <FormControl
                        label="Email*"
                    >
                        <Input
                            onChange={ e => setMailValue( e.target.value ) }
                            value={ mailValue }
                            id={ props.id }
                            placeholder={ "Nhập email của bạn" }
                        />
                    </FormControl>
                    <FormControl
                        label="Mật khẩu*"
                    >
                        <Input
                            onChange={ e => setPasswordValue( e.target.value ) }
                            value={ passwordValue }
                            id={ props.id }
                            placeholder={ "Nhập mật khẩu của bạn" }
                            type="password"
                        />
                    </FormControl>
                    <Button
                        overrides={ {
                            BaseButton: {
                                style: {
                                    outline: `#1565D8`,
                                    backgroundColor: `#1565D8`,
                                    width: '100%',
                                    ':hover': {
                                        outline: `#1053B5`,
                                        backgroundColor: `#1053B5`
                                    }
                                }
                            }
                        } }
                        onClick={ handleOnClick }
                    >
                        Đăng ký
                </Button>
                    <div className="marginTop-xs">
                        <Checkbox
                            checked={ checked }
                            onChange={ () => setChecked( !checked ) }
                            overrides={ {
                                Checkmark: {
                                    style: ( {
                                        border: `#1565D8 solid`,
                                    } )
                                }
                            } }
                        >
                            Tôi đồng ý với
                        <Link href="/">
                                <a className="text-link">
                                    &nbsp;chính sách
                            </a>
                            </Link> và
                        <Link href="/">
                                <a className="text-link">
                                    &nbsp;điều khoản
                            </a>
                            </Link> của trang web
                    </Checkbox>
                    </div>
                    <p className="login-link">Bạn đã có tài khoản ?
                    <Link href="/login">
                            <a >
                                Đăng nhập
                        </a>
                        </Link>
                    </p>
                </div>
            </div>
            <Notification
                isOpen={ isOpenModal }
                onClose={ () => { setIsOpenModal( false ); } }
                styles={ stylesModal }
                content={ contentModal }
                header={ headingModal }
                onClickFeatureButton={ () => { setIsOpenModal( false ); } }
            />
        </section>
    );
};

export default Signup;
