import React, { Component, useState, useEffect } from 'react';
import { Button } from "baseui/button";
import Link from 'next/link';
import ArrowWhite from "../public/svgicon/ArrowWhite.svg";
import Facebook from "../public/svgicon/facebook.svg";
import Twitter from "../public/svgicon/twitter.svg";
import Instagram from "../public/svgicon/instagram.svg";
function Footer ( props )
{
    return (
        <footer className="footer">
            <div className="footer-main">
                <div className="footer-logo">
                    <Link href="/">
                        <a>
                            <img alt="logo" src="/images/Logo.png" />
                        </a>
                    </Link>
                </div>
                <div className="footer-contact">
                    <Link href="/">
                        <a>
                            <Facebook />
                        </a>
                    </Link>
                    <Link href="/">
                        <a>
                            <Twitter />
                        </a>
                    </Link>
                    <Link href="/">
                        <a>
                            <Instagram />
                        </a>
                    </Link>
                </div>
            </div>
            <div className="footer-link">
                <div className="footer-info">
                    <ul className="footer-list">
                        <li className="footer-list-item heading">
                            <Link href="/search/limit=12&post_type=1">
                                <a>
                                    Bất động sản cho thuê
                                </a>
                            </Link></li>
                        <li className="footer-list-item">
                            <Link href="/search/limit=12&post_type=1&type=0">
                                <a>
                                    Nhà riêng
                                </a>
                            </Link>
                        </li>
                        <li className="footer-list-item">

                            <Link href="/search/limit=12&post_type=1&type=11">
                                <a>
                                    Chung cư / căn hộ
                                </a>
                            </Link></li>
                        <li className="footer-list-item"><Link href="/search/limit=12&post_type=22">
                            <a>
                                Đất nền
                                </a>
                        </Link> </li>
                    </ul>
                </div>
                <div className="footer-info">
                    <ul className="footer-list">
                        <li className="footer-list-item heading">
                            <Link href="/search/limit=12&post_type=0">
                                <a>
                                    Bất động sản bán
                                </a>
                            </Link>
                        </li>
                        <li className="footer-list-item">
                            <Link href="/search/limit=12&post_type=1&type=0">
                                <a>
                                    Nhà riêng
                                </a>
                            </Link>
                        </li>
                        <li className="footer-list-item">

                            <Link href="/search/limit=12&post_type=1&type=11">
                                <a>
                                    Chung cư / căn hộ
                                </a>
                            </Link>
                        </li>
                        <li className="footer-list-item">
                            <Link href="/search/limit=12&post_type=1&type=2">
                                <a>
                                    Đất nền
                                </a>
                            </Link>
                        </li>
                    </ul>
                </div>
                <div className="footer-info">
                    <ul className="footer-list">
                        <li className="footer-list-item heading">
                            <Link href="/search/limit=12">
                                <a>
                                    Bất động sản cho thuê - mua bán
                                </a>
                            </Link>
                        </li>
                        {/* <li className="footer-list-item">content </li>
                        <li className="footer-list-item">content </li>
                        <li className="footer-list-item">content </li>
                        <li className="footer-list-item">content </li> */}
                    </ul>
                </div>
            </div>
        </footer>
    );
};

export default Footer;
