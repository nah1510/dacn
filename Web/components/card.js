import React, { Component, useState, useEffect } from 'react';
import Link from 'next/link';
import Heart from "../public/images/card/heart.svg";
import Bed from "../public/images/card/hotel-single-bed-1.svg";
import Bathroom from "../public/images/card/bathroom-tub-towel.svg";
import Floors from "../public/images/card/floors.svg";
import { Avatar } from "baseui/avatar";
import Tilt from 'react-tilt';
import { formatDateShort, formatDate, formatPrice, formatAddressShort } from '../reusefunc/reuse';
import { apiGetListProvinces, apiGetListDistricts, apiGetListWards } from '../API';
import { Skeleton } from 'baseui/skeleton';

const OPTION_TILT = {
    reverse: false,  // reverse the tilt direction
    max: 15,     // max tilt rotation (degrees)
    perspective: 1000,   // Transform perspective, the lower the more extreme the tilt gets.
    scale: 1,      // 2 = 200%, 1.5 = 150%, etc..
    speed: 500,    // Speed of the enter/exit transition
    transition: true,   // Set a transition on enter/exit.
    axis: null,   // What axis should be disabled. Can be X or Y.
    reset: true,   // If the tilt effect has to be reset on exit.
    easing: "cubic-bezier(.03,.98,.52,.99)",    // Easing on enter/exit.
};

const Array_Post_Type = [ 'Bán', 'Cho thuê' ];
function Card ( props )
{
    const { info, className } = props;
    if ( info )
    {
        const { id, image, post_type, tittle, created_at, price, acreage, floor, bedroom, bathroom, province, ward, district } = info;
        const [ image_render, setImage ] = useState( image );
        return (
            <Tilt className="Tilt" options={ OPTION_TILT } >
                <article className={ `card${ className ? " " + className : '' }` }>
                    {/* <div className="card-favorite">
                        <Heart />
                    </div> */}
                    <Link href={ `/posts/${ id }` }>
                        <a >
                            <div className="card-image">

                                <img src={ image_render }
                                    onError={
                                        e =>
                                        {
                                            setImage( '/images/default-image.jpg' );
                                        }
                                    }
                                />

                                <span className="card-label">
                                    { Array_Post_Type[ post_type ] }
                                </span>
                            </div>
                        </a>
                    </Link>
                    <div className="card-info">
                        <div className="card-heading">
                            <Link href={ `/posts/${ id }` }>
                                <a>
                                    <h3>
                                        { tittle }
                                    </h3>
                                    <p>
                                        { formatAddressShort( province.name, district.name, ward.name ) }
                                    </p>
                                </a>
                            </Link>
                        </div>
                        <div className="card-content">
                            <p>
                                <span>
                                    { formatPrice( price ) }
                                </span>
                                <span className="minor">
                                    -
                            </span>
                                <span>
                                    { acreage }m<sup> 2</sup>
                                </span>
                            </p>
                            <p className="card-datetime marginTop-m">{ formatDate( created_at ) }</p>
                        </div>
                        <div className="card-features">
                            <div className="card-features-item">
                                <Bed />
                                <span className="marginLeft-xs">{ bedroom }</span>
                            </div>
                            <div className="card-features-item center">
                                <Bathroom />
                                <span className="marginLeft-xs">{ bathroom }</span>
                            </div>
                            <div className="card-features-item">
                                <Floors />
                                <span className="marginLeft-xs">{ floor }</span>
                            </div>
                        </div>
                    </div>
                </article>
            </Tilt>
        );
    }
    return (
        <Tilt className="Tilt" options={ OPTION_TILT } >
            <Skeleton width="275px" height="433.5px" animation overrides={ {
                Root: {
                    style: ( {
                        borderRadius: `25px`
                    } )
                }
            } } />
        </Tilt>
    );
};

export function HCard ( props )
{
    const { info, className } = props;
    if ( info )
    {
        const { comment, created_at, user } = info;
        return (
            <article className={ `hcard${ className ? " " + className : "" }` }>
                <div className="hcard-avatar">
                    <Avatar
                        name={ user.name }
                        size="45px"
                        src={ user.avatar }
                    />
                </div>
                <div className="hcard-content">
                    <h3 className="marginBottom-xs">{ user.name }<span className="time">{ formatDate( created_at ) }</span></h3>
                    <div className="hcard-content">
                        <p>
                            { comment }
                        </p>
                    </div>
                </div>
            </article>
        );
    };
    return null;
};

export default Card;
